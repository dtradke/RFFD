# RFire
# 11/16/19
#
# Visualizes 2D clustering for RFire


from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
from matplotlib import pyplot as plt



def scatterplot(results, testX, testY):
        x_ax = [] #list(range(0, len(results)))
        y_ax = results
        color = []

        for a in testX:
            x_ax.append(a[0][0])

        for a in testY:
            if a == 0:
                color.append('red')
            else:
                color.append('green')

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1,axisbg='1.0')
        #
        # for d, color, group in zip(d, colors, groups):
        #     x, y = d
        #     ax.scatter(x, y, alpha=0.8, c = color, edgecolors='none', s=30, label=group)


        x = x_ax
        y = y_ax

        # print(x)
        # print(y)


        ax.scatter(x, y, alpha=0.7, c=color, edgecolors = 'none', s=30, label='Days')
        # plt.plot(x, np.poly1d(np.polyfit(x, y, 1))(x), polymin=0)

        # plt.title(name, fontsize=18)
        # plt.legend(loc=2)


        ax.set_ylim(ymin=0)
        ax.set_xlim(xmin=0)
        plt.yticks(fontsize=15)
        plt.xticks(fontsize=15)
        plt.ylabel('Regression value', fontsize=15)
        plt.xlabel('Reflection Power', fontsize=15)

        plt.show()

def pca_scatterplot(x, y, testY):
        color = []

        for a in testY:
            if a == 0:
                color.append('red')
            else:
                color.append('green')

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1,axisbg='1.0')


        ax.scatter(x, y, alpha=0.7, c=color, edgecolors = 'none', s=30, label='Days')

        # ax.set_ylim(ymin=0)
        # ax.set_xlim(xmin=0)
        plt.yticks(fontsize=15)
        plt.xticks(fontsize=15)
        plt.ylabel('Regression value', fontsize=15)
        plt.xlabel('Reflection Power', fontsize=15)

        plt.show()
