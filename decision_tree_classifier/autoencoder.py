# RFire
# 11/16/19
# 
# Autoencoder to try and cluser fire events in RFire.


# cnn model
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
# from matplotlib import pyplot
from keras import backend as K
from keras.models import Sequential, Model
from keras.layers import Dense, Input
from keras.layers import Flatten, Reshape
from keras.layers import Dropout
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D, UpSampling1D
from keras.utils import to_categorical
from keras.models import load_model

from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.decomposition import PCA


from matplotlib import pyplot as plt
import time
import preprocess
import sys
import decision_tree_numpy



def AutoEncodeModel(trainX, testX, epochs=1, batch_size=500):
    samples, sample_size, step_size = trainX.shape

    input_window = Input(shape=(sample_size,6))
    x = Conv1D(16, 30, activation="relu", padding="same")(input_window) # 10 dims
    #x = BatchNormalization()(x)
    x = MaxPooling1D(5, padding="same")(x) # 5 dims
    x = Conv1D(8, 1, activation="relu", padding="same")(x) # 5 dims
    #x = BatchNormalization()(x)
    encoded = MaxPooling1D(5, padding="same")(x) # 3 dims
    flat = Flatten()(encoded)
    encoder_output = Dense(32, activation='relu')(flat)

    encoder = Model(input_window, encoder_output)
    # print(encoder.summary())
    t_shape = encoded.get_shape().as_list()

    decoder_input = Dense(32, activation='relu')(encoder_output)
    reshaped = Reshape(target_shape=(t_shape[1], t_shape[2]))(decoder_input)
    x = Conv1D(8, 30, activation="relu", padding="same")(reshaped)#(encoded) # 3 dims
    #x = BatchNormalization()(x)
    x = UpSampling1D(5)(x) # 6 dims
    x = Conv1D(16, 1, activation='relu')(x) # 5 dims
    #x = BatchNormalization()(x)
    x = UpSampling1D(5)(x) # 10 dims
    decoded = Conv1D(6, 100, activation='sigmoid', padding='same')(x)


    autoencoder = Model(input_window, decoded)
    # print(autoencoder.summary())
    autoencoder.compile(optimizer='adam', loss='mse', metrics=['accuracy'])
    autoencoder.fit(trainX, trainX, epochs=epochs, batch_size=batch_size, shuffle=True, validation_data=(testX, testX), verbose=1)
    return autoencoder, encoder



    # verbose, epochs, batch_size = 0, epochs, 100#32
    # n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], 50
    # model = Sequential()
    # model.add(Conv1D(filters=64, kernel_size=2, activation='relu', input_shape=(n_timesteps,n_features)))
    # model.add(Conv1D(filters=64, kernel_size=2, activation='relu'))
    # model.add(Conv1D(filters=128, kernel_size=2, activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(MaxPooling1D(pool_size=2))
    # model.add(Flatten())
    # model.add(Dense(100, activation='relu'))
    # model.add(Dense(n_outputs, kernel_initializer='normal'))
    # model.compile(loss='mean_squared_error', optimizer='adam') #loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    # model.fit(trainX, trainX, epochs=epochs, batch_size=batch_size, shuffle=True, validation_data=(testX, testX), verbose=1) #verbose
    # return model
