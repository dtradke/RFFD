# RFire
# 11/16/19
#
# This code reads in data from the csv files and generates scatterplots from defined clusering and
# dimensionality reduction, for RFire data.


# cnn model
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
# from matplotlib import pyplot
from keras.models import Sequential, Model
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.utils import to_categorical
from keras.models import load_model

from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.decomposition import PCA


from matplotlib import pyplot as plt
import time
import preprocess
import sys
import decision_tree_numpy
import autoencoder
import data_viz



def train_model(trainX, trainy, epochs=1, batch_size=500):
    verbose, epochs, batch_size = 0, epochs, 100#32
    n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], 1
    model = Sequential()
    model.add(Conv1D(filters=64, kernel_size=2, activation='relu', input_shape=(n_timesteps,n_features)))
    model.add(Conv1D(filters=64, kernel_size=2, activation='relu'))
    model.add(Conv1D(filters=128, kernel_size=2, activation='relu'))
    model.add(Dropout(0.5))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(100, activation='relu'))
    model.add(Dense(n_outputs, kernel_initializer='normal'))
    model.compile(loss='mean_squared_error', optimizer='adam') #loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=1) #verbose
    return model

def flatten(pca_data):
    arr = []
    for i in pca_data:
        arr.append(i.flatten())
    return np.array(arr)

def PCABuild(X):
    pca_data = []
    pca = PCA(n_components=10)
    X_transpose = X.transpose()
    pca.fit(X_transpose)
    pca_mapping = pca.components_.transpose()
    return pca_mapping

# def autoEncoderBuild(X):

def viz_data(X_flattened, testY):
    x = [i[0] for i in X_flattened]
    y = [i[1] for i in X_flattened]

    print(len(x))
    print(len(y))

    data_viz.pca_scatterplot(x, y, testY)

def unsupervisedCluster(X, y, ignition_sample=None, cluster_type='kmeans', autoencoder_mode=False):
    PCA_mode = True
    if autoencoder_mode:
        PCA_mode = False
        X_flattened = X

    no_burn_clusters = {}
    burn_clusters = {}

    if PCA_mode:
        X_flattened = decision_tree_numpy.flattenData(X)
        X_flattened = PCABuild(X_flattened)
    elif not autoencoder_mode:
        X_flattened = decision_tree_numpy.flattenData(X)

    # viz_data(X_flattened, y)

    if cluster_type == 'kmeans':
        kmeans = KMeans(n_clusters=2, random_state=0).fit(X_flattened)
        labels = kmeans.labels_
        for idx, val in enumerate(labels):
            print(y[idx], " ", val)
        exit()
    elif cluster_type == 'dbscan':
        dbscan = DBSCAN(eps=0.05, min_samples=20).fit(X_flattened)
        labels = dbscan.labels_
        for idx, val in enumerate(labels):
            # print(y[idx], " ", val)
            if y[idx] == 1:
                if val not in burn_clusters:
                    burn_clusters[val] = 1
                else:
                    burn_clusters[val] += 1
            else:
                if val not in no_burn_clusters:
                    no_burn_clusters[val] = 1
                else:
                    no_burn_clusters[val] += 1

        print("Burn Clusters:    ", burn_clusters)
        print("No Burn Clusters: ", no_burn_clusters)

    exit()

# summarize scores
def summarize_results(scores):
	print(scores)
	m, s = mean(scores), std(scores)
	print('Accuracy: %.3f%% (+/-%.3f)' % (m, s))

def timeSeriesTest(testY, y, leave_one_out, ignition_sample):
    correct = 0
    incorrect = 0
    FN = 0
    FP = 0
    TP = 0
    TN = 0
    FN_before_fire = 0
    fire_flag = False
    early_classification = 0
    false_alarm = 0
    for i, val in enumerate(testY):
        if y[i][0] > y[i][1]: #predicted no fire
            if val[0] > val[1]: #actual no fire
                correct += 1
                TN += 1
            else: #actual fire
                incorrect += 1
                FN += 1
                if not fire_flag:
                    FN_before_fire += 1
        else: # predicted fire
            if leave_one_out and not fire_flag:
                if val[0] < val[1]:
                    fire_flag = True
                    print(" \U0001f525 \U0001f525 \U0001f525 Detected fire at time ", i, " \U0001f525 \U0001f525 \U0001f525")
                    latency = i - ignition_sample
                    print("Latency: ", latency, " second(s)")
                else:
                    future = testY[i:i+5]
                    #checking if a fire is close due to the labeling format
                    for f in future:
                        if f[0] < f[1]:
                            early_classification += 1
                            break
                    else:
                        print("\U0000274C \U0000274C \U0000274C FALSE ALARM at time ", i, " \U0000274C \U0000274C \U0000274C")
                        false_alarm += 1
            if val[0] < val[1]: #actual fire
                correct += 1
                TP += 1
            else: # actual no fire
                incorrect += 1
                FP += 1

    print("Correct: ", correct)
    print("incorrect: ", incorrect)
    print("correct perc: ", correct/len(testY))
    print("TP: ", TP)
    print("TN: ", TN)
    print("FP (annoying) - no fire but predicted fire:     ", FP)
    print("FN (bad) - actual fire but not predicted fire:  ", FN)
    print("Predicted no fire before time when fire starts: ", FN_before_fire)
    print('Early Classification: ', early_classification)
    print('False Alarms: ', false_alarm)

# Run unsupervised clustering
def cluster(leave_one_out, cluster, cluster_type):
    X, y, ignition_sample = preprocess.loadDataset(leave_one_out=False, cluster=cluster)
    unsupervisedCluster(X, y, ignition_sample, cluster_type)

#run the autoencoder
def autoencode(autoencode=None):
    trainX, trainY, testX, testY, ignition_sample = preprocess.loadDataset(leave_one_out=False)
    if autoencode is None:
        epochs = 100
        autoencode, encoder = autoencoder.AutoEncodeModel(trainX, testX, epochs=epochs)
        time_string = time.strftime("%Y%m%d-%H%M%S")
        fname = 'autoencoder_models/' + time_string + '_' + str(epochs) + 'epochs_mapsto32.h5'
        autoencode.save(fname)
    encoder_trained = Model(autoencode.input, autoencode.layers[-8].output)
    fullX = np.concatenate((trainX, testX), axis=0)
    fullY = np.concatenate((trainY, testY), axis=0)
    encoded = encoder_trained.predict(fullX)
    unsupervisedCluster(encoded, fullY, cluster_type='dbscan', autoencoder_mode=True)

#run the regression model
def regression_cluster(model=None):
    trainX, trainY, testX, testY, ignition_sample = preprocess.loadDataset(leave_one_out=False)

    print("Training Shape: ", trainX.shape, "   ", trainY.shape)
    print("Testing Shape:  ", testX.shape, "    ", testY.shape)
    # print(testY)

    if model is None:
        epochs = 100
        model = train_model(trainX, trainY, epochs=epochs)
        time_string = time.strftime("%Y%m%d-%H%M%S")
        if leave_one_out:
            fname = 'regression_models/' + time_string + '_' + str(epochs) + 'epochs_leaveoneout.h5'
        else:
            fname = 'regression_models/' + time_string + '_' + str(epochs) + 'epochs_allrelu.h5'
        model.save(fname)

    y = model.predict(testX)
    arr = []
    for a in y:
        arr.append(a[0])
    data_viz.scatterplot(arr, testX, testY)
    # timeSeriesTest(testY, y, leave_one_out, ignition_sample)

# run an experiment
def run_experiment(model=None, repeats=10, cluster=False, cluster_type='kmeans', autoencode_mode=False):

    if cluster:
        cluster(leave_one_out, cluster, cluster_type)
    elif autoencode_mode:
        autoencode(model)
    else:
        regression_cluster(model)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        if sys.argv[len(sys.argv) - 1] == 'kmeans':
            print("===== K-Means Clustering =====")
            run_experiment(cluster=True, cluster_type='kmeans')
        elif sys.argv[len(sys.argv) - 1] == 'dbscan':
            print("===== DBSCAN Clustering =====")
            run_experiment(cluster=True, cluster_type='dbscan')
        elif sys.argv[len(sys.argv) - 1] == 'autoencode':
            print("===== USING AUTOENCODER =====")
            run_experiment(autoencode_mode=True)
        else:
            mod_str = sys.argv[len(sys.argv) - 1]
            mod = load_model('regression_models/' + mod_str)
            run_experiment(mod)
    elif len(sys.argv) == 3:
        if sys.argv[len(sys.argv) - 2] == 'autoencode':
            print("===== USING AUTOENCODER =====")
            mod_str = sys.argv[len(sys.argv) - 1]
            mod = load_model('autoencoder_models/' + mod_str)
            run_experiment(mod, autoencode_mode=True)
    else:
        run_experiment()
