# RFire
# 11/16/19
#
# Loads and processes all of the temporal samples for RFire.
# NOTE: Might need to change the indices depending on the folder the data files are in.


import pandas as pd
import numpy as np
from numpy import genfromtxt
from matplotlib import pyplot as plt
import collections
import math
from scipy.signal import argrelmin, argrelmax
import formatdata
import augmentFunctions

FLUCT_RANGE = .11
SEARCH_SPACE_MAX = 4
SEARCH_SPACE_MIN = 0.5
ENDBASELINE = 600
POINTTHRESH = 0.8
RECENT_TIME_BUFFER = 30

# NOTE: MAKE SURE THAT WHEN YOU USE DIFFERENT CSVS, THE INDICES ARE CORRECT.

# for first_experiments/beta_index and midul_apartment/ (add in the index_col=False)
# # TIMESTEP_IDX = 0
# # OBJAMT = 1
# # X_IDX = 3
# # Y_IDX = 4
# # POWER_IDX = 7
#
# # for first_experiments/midul_index
TIMESTEP_IDX = 1
OBJAMT = 0
X_IDX = 4
Y_IDX = 5
POWER_IDX = 8

directory = '../data/'

class SigObject(object):

    def __init__(self, x=None, y=None, id=None, power=0, timestep=None):
        self.x = x
        self.y = y
        self.id = id
        self.power = collections.deque(40*[power], 40)
        self.distance = [math.sqrt((x ** 2) + (y ** 2))]
        self.movement = [0]
        self.avg_movement_x = 0
        self.avg_movement_y = 0
        self.cache_movement_x = 0 #cache movement calculates how much an object has moved in recent memory
        self.cache_movement_y = 0
        self.flag = False
        self.avgrelmax = power
        self.avgrelmin = power
        self.returning_obj = True
        self.fluc_flag = False
        self.fluc_zone = []
        self.timesteps = [timestep]
        # self.occluded = collections.deque(10*[1], 10) #1 means not occluded, 0 means it is
        self.occlusions = [] #collections.deque(30*[0], 30)
        self.ram = [power]
        self.fire_flag = False
        self.has_baseline = False
        self.normal_reflection = sum(self.power) / len(self.power)
        self.time_fluctuating = 0
        try:
            self.direction = [math.atan2(y,x)/math.pi*180]
        except:
            self.direction = [90]

        self.range = self.normal_reflection * FLUCT_RANGE

    def updateMetrics(self, cur_x, cur_y, cur_power, timestep):
        old_x = self.x
        old_y = self.y

        self.x = cur_x
        self.y = cur_y
        self.power.popleft()
        self.power.append(cur_power)
        self.distance.append(math.sqrt((self.x ** 2) + (self.y ** 2)))
        self.timesteps.append(timestep)
        self.ram.append(cur_power)


        if timestep > ENDBASELINE:
            self.cache_movement_x = self.cache_movement_x + (old_x - cur_x)
            self.cache_movement_y = self.cache_movement_y + (old_y - cur_y)

        self.avg_movement_x = self.avg_movement_x + (old_x - cur_x)
        self.avg_movement_y = self.avg_movement_y + (old_y - cur_y)

        if abs(self.cache_movement_x) > abs(self.avg_movement_x) or abs(self.cache_movement_y) > abs(self.avg_movement_y):
            self.movement.append(1)# = True
        else:
            self.movement.append(0)# = False

        try:
            self.direction.append(math.atan2(cur_y,cur_x)/math.pi*180)
        except:
            self.direction.append(90)

    def graphItNoFire(self):
        data = self.ram

        # data2 = []
        # s = 0
        # e = 30
        # for i in range(0, (len(data) // 30)):
        #     new_arr = data[s:e]
        #     data2.append(min(new_arr))
        #     s = s + 30
        #     e = e + 30



        # data = np.array(data2)

        t = np.arange(0,len(data))
        data1 = data

        # fig, ax1 = plt.subplots()

        color = 'tab:blue'
        # ax1.set_title('Fire on Drywall at 1 Meter', fontsize=15)
        plt.xlabel('Time (Seconds)', fontsize=15)
        plt.ylabel('Reflection Power', color=color, fontsize=15)
        # ax1.set_ylim(0,4800)
        line1, = plt.plot(t, data1, color=color, label='Reflection Power')
        plt.tick_params(axis='y', labelcolor=color, labelsize=15)
        plt.tick_params(axis='x', labelsize=15)
        # plt.set_xticklabels(('0', '0', '50', '100', '150', '200', '250'))

        plt.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.legend((line1,), ('Reflection Power', ))
        plt.show()

    def getAvgFluctuation(self):
        np_power = np.array(self.power)
        self.avgrelmax = ((self.avgrelmax + len(argrelmax(np_power)[0])) / 2) / len(self.power)
        self.avgrelmin = ((self.avgrelmin + len(argrelmin(np_power)[0])) / 2) / len(self.power)
        return

    def checkFluctuation(self, timestep):
        # np_power = np.array(self.power)
        np_power = np.array(self.fluc_zone)
        np_recent = np.array(self.power)
        np_occlusions = np.array(self.occlusions)

        cur_rel_max = argrelmax(np_power)
        cur_rel_min = argrelmin(np_power)
        rel_max_amt = argrelmax(np_recent)
        rel_min_amt = argrelmin(np_recent)
        cur_occlusion_rel_max = argrelmax(np_occlusions)
        cur_occlusion_rel_min = argrelmin(np_occlusions)

        if (len(cur_rel_max[0]) + len(cur_rel_min[0])) >= 4:
            distances = self.getFluctuationDistances(cur_rel_max[0], cur_rel_min[0], np_power)
            avg_fluc_dist = sum(distances) / len(distances)
            max_fluc_dist = max(distances)
            rel_max_metric = len(cur_rel_max[0]) / len(np_power)
            rel_min_metric = len(cur_rel_min[0]) / len(np_power)
            avg_extrema = (rel_max_metric + rel_min_metric) / 2


            # if avg_fluc_dist > self.range or len(rel_max_amt[0]) > self.avgrelmax and len(rel_min_amt[0]) > self.avgrelmin:
            if avg_fluc_dist > self.range and rel_max_metric > self.avgrelmax and rel_min_metric > self.avgrelmin and self.fire_flag == False and np.mean(np_power) < (self.normal_reflection - self.range):
                if self.has_baseline == True and self.distance < SEARCH_SPACE_MAX and self.movement[len(self.movement) - 1] == 0 and len(self.ram) > 600:
                    occ_diff = self.occlusions[len(self.occlusions) - 1] - self.occlusions[0]
                    if len(set(self.occlusions)) ==  1 or len(set(self.occlusions)) == 2 and occ_diff < 0:
                        if len(self.timesteps) > (POINTTHRESH * timestep):
                            self.fire_flag = True
                            print('CONCLUSION: OBJECT {} IS ON FIRE! Occuring at time {} \U0001f525 \U0001f525 \U0001f525'.format(self, timestep))
        return

    def getFluctuationDistances(self, cur_rel_max, cur_rel_min, arr):
        idx = 0
        important_idx = []
        for x in cur_rel_max:
            important_idx.append(x)
        for x in cur_rel_min:
            important_idx.append(x)
        important_idx = sorted(important_idx)
        fluctuation_diff = []
        for i in range(0, (len(important_idx) - 1)):
            diff = arr[important_idx[i]] - arr[important_idx[i+1]]
            fluctuation_diff.append(abs(diff))
        return fluctuation_diff

    def getCurAvgPower(self):
        numerator = list(self.power)[(-1 * RECENT_TIME_BUFFER):]
        min_recent = min(numerator)
        max_recent = max(numerator)
        return min_recent, max_recent

    def __repr__(self):
        return "SigObject({},{} meters,x: {}, y: {}, {} degrees, Norm Ref: {})".format(self.id, self.distance[len(self.distance) - 1], self.x, self.y, self.direction[len(self.direction) - 1], self.normal_reflection)


def makeInitialObj(data_df):
    cols = data_df.columns
    first = data_df.loc[data_df[cols[OBJAMT]] == 1]
    id = 0
    objs = []
    old_obj_amt = 0

    for index, row in first.iterrows():
        old_obj_amt = row[cols[OBJAMT]]
        objs.append(SigObject(float(row[cols[X_IDX]]), float(row[cols[Y_IDX]]), id, int(row[cols[POWER_IDX]]), timestep=1))
        id = id + 1

    objs = getOccluded(objs, 1)

    return objs, old_obj_amt, data_df

def getOccluded(sig_objects, timestep):
    for obj in sig_objects:
        occlusions = 0
        if timestep not in obj.timesteps:
            continue
        else:
            for a in sig_objects:
                if a.id != obj.id:
                    if a.direction[len(a.direction) - 1] <= (obj.direction[len(obj.direction) - 1] + 10) and a.direction[len(a.direction) - 1] >= (obj.direction[len(obj.direction) - 1] - 10):
                        if a.distance[len(a.distance) - 1] < obj.distance[len(obj.distance) - 1]:
                            if timestep in a.timesteps:
                                if obj.distance[len(obj.distance) - 1] < SEARCH_SPACE_MAX:
                                    occlusions = occlusions + 1
            # obj.occlusions.popleft()
            obj.occlusions.append(occlusions)
    return sig_objects

def checkForFire(sig_objects, new_sig_objects, timestep):
    sig_objects = getOccluded(sig_objects, timestep)

    for obj in sig_objects:
        # current_state = obj.power[0] - obj.power[len(obj.power) - 1]
        comp_to_avg = obj.normal_reflection - obj.power[len(obj.power) - 1]
        current_max_min = max(obj.power) - min(obj.power)
        min_recent, max_recent = obj.getCurAvgPower()
        recent_diff = max_recent - min_recent
        within_10_perc = obj.normal_reflection * .1

        #still decreasing
        if abs((obj.power[len(obj.power) - 1] - obj.power[len(obj.power) - 2])) < obj.range and comp_to_avg > obj.range: #current_state
            obj.fluc_flag = True

        #back to normal level
        # if obj.flag == True and recent_diff <= (obj.normal_reflection + within_10_perc) and recent_diff >= (obj.normal_reflection - within_10_perc):
        if obj.flag == True and max_recent <= (obj.normal_reflection + within_10_perc) and min_recent >= (obj.normal_reflection - within_10_perc) and recent_diff < within_10_perc:
            obj.flag = False
            obj.fluc_flag = False
            obj.time_fluctuating = 0

        if obj.fluc_flag:
            obj.fluc_zone.append(obj.power[len(obj.power) - 1])
        else:
            obj.fluc_zone = []

        #dropped and not returned and stabilized yet
        if obj.flag and obj.has_baseline:
            obj.time_fluctuating += 1
            if obj.time_fluctuating > (RECENT_TIME_BUFFER*2):
                obj.checkFluctuation(timestep)

        #drops
        if comp_to_avg > obj.range and obj.has_baseline:
            obj.flag = True
            if obj.time_fluctuating > (RECENT_TIME_BUFFER*2):
                obj.checkFluctuation(timestep)

    return


def getBaselineRange(sig_objects):
    for obj in sig_objects:
        avg_power = (sum(obj.power) / len(obj.power))
        if avg_power > 50:
            cur_range = avg_power * FLUCT_RANGE
        else:
            cur_range = 15
        obj.range = cur_range
    return sig_objects


def graphX(sample):
    data = []

    for i in sample:
        data.append(i[0])

    data = np.array(data)

    t = np.arange(0,len(data))
    data1 = data
    color = 'tab:blue'
    # ax1.set_title('Fire on Drywall at 1 Meter', fontsize=15)
    plt.xlabel('Time (Seconds)', fontsize=15)
    plt.ylabel('Reflection Power', color=color, fontsize=15)
    # ax1.set_ylim(0,4800)
    line1, = plt.plot(t, data1, color=color, label='Reflection Power')
    plt.tick_params(axis='y', labelcolor=color, labelsize=15)
    plt.tick_params(axis='x', labelsize=15)
    # plt.set_xticklabels(('0', '0', '50', '100', '150', '200', '250'))

    plt.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.legend((line1,), ('Reflection Power', ))
    plt.show()







def runSim(sig_objects, data_df, old_obj_amt, fire, experiment_name, augment, leave_one_out):
    global SEARCH_SPACE_MIN
    cols = data_df.columns

    idx = 2
    length = data_df[cols[TIMESTEP_IDX]][data_df.shape[0] - 1]


    for z in range(0, int(length)):
        new_sig_objects = []
        cur_df = data_df.loc[data_df[cols[TIMESTEP_IDX]] == idx]


        for index, row in cur_df.iterrows():
            cur_x = float(row[cols[X_IDX]])
            cur_y = float(row[cols[Y_IDX]])
            cur_power = int(row[cols[POWER_IDX]])
            cur_obj_amt = int(row[cols[OBJAMT]])
            obj_amt = 0

            for a in sig_objects:
                obj_amt+=1
                #updates
                if cur_x >= (a.x - 0.05) and cur_x <= (a.x + 0.05) and idx not in a.timesteps: #ADD THIS CHECK IN OTHER SCRIPT
                    if  cur_y >= (a.y - 0.05) and cur_y <= (a.y + 0.05): #was .2
                        # if a.id == 1:
                        #     print('obj: ', idx)
                        a.updateMetrics(cur_x, cur_y, cur_power, idx)
                        a.returning_obj = True
                        break
            else:
                #new object
                new_id = (len(sig_objects) + len(new_sig_objects))
                new_obj = SigObject(cur_x, cur_y, new_id, cur_power, timestep=idx)
                new_obj.power.popleft()
                new_obj.power.append(cur_power)
                new_obj.returning_obj = False
                new_sig_objects.append(new_obj)

        sig_objects = sig_objects + new_sig_objects
        sig_objects = getOccluded(sig_objects, idx)
        idx+=1

    flags = 0
    dataset_obj = []

    if experiment_name == "heatgun_corrected_frame.csv":
        SEARCH_SPACE_MIN = 0.1
    else:
        SEARCH_SPACE_MIN = 0.5

    for a in sig_objects:
        # print(a)

        if a.distance[len(a.distance) - 1] < SEARCH_SPACE_MAX and a.distance[len(a.distance) - 1] > SEARCH_SPACE_MIN:
            if len(a.ram) >= (.5 * idx) and a.movement[len(a.movement) - 1] == 0:
                if a.normal_reflection > 500:
                    dataset_obj.append(a)


    split_experiment_name = experiment_name.split('_')
    if 'fire' in split_experiment_name:
        first_fire_objs = []
        for d in dataset_obj:
            first_fire_objs.append(d)

        for f in first_fire_objs:
            for a in sig_objects:
                if abs(f.x - a.x) < .2 and abs(f.y - a.y) < .2 and len(a.ram) > 100 and f.id is not a.id and abs(f.normal_reflection - a.normal_reflection) < (f.normal_reflection * .1):
                    dataset_obj.append(a)


    for i in dataset_obj:
        print(i)
        # i.graphItNoFire()

    samples, fires = formatdata.makeSamples(dataset_obj, fire)

    flag = False
    flagX = False

    if augment and fire and not leave_one_out:
        loop_amt = 5
        samples = augmentFunctions.augmentIncrease(samples, fires, loop_amt)
    return samples

def formatData(dirname, fnames, leave_one_out=False):
    global TIMESTEP_IDX
    global OBJAMT
    global X_IDX
    global Y_IDX
    global POWER_IDX

    returned_x, returned_y = [], []
    fireX, fireY = [], []

    for i in fnames:
        print('================ ', dirname + i, '================')
        if dirname == 'midul_apartment/':
            data_df = pd.read_csv(directory + dirname + i, index_col=False)
            TIMESTEP_IDX = 0
            OBJAMT = 1
            X_IDX = 3
            Y_IDX = 4
            POWER_IDX = 7
        else:
            data_df = pd.read_csv(directory + dirname + i) # , index_col=False (for midul_apartment)
            if dirname.split('/')[1] == 'midul_index':
                TIMESTEP_IDX = 1
                OBJAMT = 0
                X_IDX = 4
                Y_IDX = 5
                POWER_IDX = 8
            else:
                TIMESTEP_IDX = 0
                OBJAMT = 1
                X_IDX = 3
                Y_IDX = 4
                POWER_IDX = 7

        print('Read data...')

        split_i = i.split('_')
        if 'fire' in split_i:
            fire = True
            augment = True #True   SET AUGMENT TO FALSE
            print("Fire is true")
        else:
            fire = False
            augment = False
            print("Fire is false")

        initial_objects, old_obj_amt, data_df = makeInitialObj(data_df)
        samples = runSim(initial_objects, data_df, old_obj_amt, fire, i, augment, leave_one_out)

        returned_x.append(samples)

    data = returned_x[0]

    for x in range(1, len(returned_x)):
        data = np.concatenate((data, returned_x[x]), axis=0)

    return data


# if __name__ == '__main__':
def loadDataset(leave_one_out, cluster=False, autoencode_mode=True):
    # dirnames = ['first_experiments/midul_index/', 'first_experiments/beta_index/', 'midul_apartment/']
    # midul_fnames = ['new_fire_1m.csv', 'new_fire_2m.csv', 'new_occlusion_1m.csv', 'new_occlusion_2m.csv', 'heatgun_corrected_frame.csv']
    # beta_index_fnames = ['carpet_fire_1m.csv', 'iron_1m.csv', 'cloth_2m_fan.csv', 'cloth_1m_fan.csv', 'painting_1m_on_long.csv', 'painting_2m_on_long.csv', 'painting_1m_off.csv', 'painting_2m_off.csv']
    # midul_apartment_fnames = ['hotplate_metal_all.csv','boiling_water.csv', 'living_room_midul_pacing_sitting.csv', 'living_room_pacing.csv', 'living_room_dave_movement.csv', 'shower.csv', 'cooldown_stove.csv', 'living_room_midul_movement.csv', 'balcony_shades_fan.csv', 'balcony_shades_nofan.csv', 'balcony_noshades_nofan.csv', 'balcony_noshades_nofan.csv', 'living_room_fan.csv', 'living_room_no_movement.csv']

    print("Leave one out: ", leave_one_out)

    dirnames = ['first_experiments/midul_index/', 'first_experiments/beta_index/', 'midul_apartment/']
    if leave_one_out:
        fire_dirname = ['midul_apartment/', 'first_experiments/midul_index/']
        fire_fname = ['new_fire_1m.csv']
        non_fire_fname = ['living_room_fan.csv']
        midul_fnames = ['new_fire_2m.csv', 'new_occlusion_1m.csv', 'new_occlusion_2m.csv', 'heatgun_corrected_frame.csv']
        midul_apartment_fnames = ['hotplate_metal_all.csv','boiling_water.csv', 'living_room_dave_movement.csv', 'living_room_midul_pacing_sitting.csv', 'living_room_pacing.csv', 'shower.csv', 'cooldown_stove.csv', 'living_room_midul_movement.csv', 'balcony_shades_fan.csv', 'balcony_shades_nofan.csv', 'balcony_noshades_nofan.csv', 'balcony_noshades_nofan.csv', 'living_room_no_movement.csv']
    else:
        midul_fnames = ['new_fire_1m.csv', 'new_fire_2m.csv', 'new_occlusion_1m.csv', 'new_occlusion_2m.csv', 'heatgun_corrected_frame.csv']
        midul_apartment_fnames = ['hotplate_metal_all.csv', 'living_room_fan.csv','boiling_water.csv', 'living_room_dave_movement.csv', 'living_room_midul_pacing_sitting.csv', 'living_room_pacing.csv', 'shower.csv', 'cooldown_stove.csv', 'living_room_midul_movement.csv', 'balcony_shades_fan.csv', 'balcony_shades_nofan.csv', 'balcony_noshades_nofan.csv', 'balcony_noshades_nofan.csv', 'living_room_no_movement.csv']

    beta_index_fnames = ['carpet_fire_1m.csv', 'iron_1m.csv', 'cloth_2m_fan.csv', 'cloth_1m_fan.csv', 'painting_1m_on_long.csv', 'painting_2m_on_long.csv', 'painting_1m_off.csv', 'painting_2m_off.csv']

    data = formatData(dirnames[0], midul_fnames)
    returned_data = formatData(dirnames[1], beta_index_fnames)
    data = np.concatenate((data, returned_data), axis=0)
    returned_data = formatData(dirnames[2], midul_apartment_fnames)
    data = np.concatenate((data, returned_data), axis=0)

    if cluster:
        return formatdata.balanceData(data, leave_one_out, cluster)
    else:
        trainX, trainY, ignition_sample = formatdata.balanceData(data)

    testX, testY = [], []
    if leave_one_out:
        print("LEAVE ONE OUT")
        data = formatData(fire_dirname[0], non_fire_fname)
        returned_data = formatData(fire_dirname[1], fire_fname, leave_one_out)
        data = np.concatenate((data, returned_data), axis=0)
        testX, testY, ignition_sample = formatdata.balanceData(data, leave_one_out)
    else:
        fullX = trainX
        fullY = trainY
        testX = trainX[int((len(trainX)) * .75):]
        testY = trainY[int((len(trainY)) * .75):]
        trainX = trainX[:int((len(trainX)) * .75)]
        trainY = trainY[:int((len(trainY)) * .75)]

    return trainX, trainY, testX, testY, ignition_sample
