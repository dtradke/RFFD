# RFire
# 11/16/19
# 
# Augment functions for fire events.




import pandas as pd
import numpy as np
from numpy import genfromtxt
from matplotlib import pyplot as plt
import collections
import math





# splits up fires into baseline and fire event, reverses the lists, and creates new samples
# def augmentReverse(samples, ySamples, fires):
#     s = []
#     yS = []
#     for f in fires:
#         before_drop = []
#         after_drop = []
#         for i, val in enumerate(f.ram):
#             if val < (f.normal_reflection * (1 - FLUCT_RANGE)):
#                 l = [f.ram[i], f.occlusions[i], f.movement[i], f.normal_reflection]
#                 after_drop.append(np.array(l))
#             else:
#                 l = [f.ram[i], f.occlusions[i], f.movement[i], f.normal_reflection]
#                 before_drop.append(np.array(l))
#
#         before_drop = np.flip(before_drop, 0) #before_drop[::-1]
#         after_drop = np.flip(after_drop, 0) #after_drop[::-1]
#         #reversed_fire = before_drop + after_drop
#         reversed_fire = np.concatenate((before_drop, after_drop), axis=0)
#         window_start = 0
#         window_end = 100
#         step = 10
#         idx = 0
#         flag = False
#         while window_end < len(reversed_fire):
#             window = np.array([reversed_fire[window_start:window_end]])
#             y, flag = getLabel(window[0], True, flag)
#             yS.append(y)
#             samples = np.concatenate((samples, window), axis=0)
#             window_start += step
#             window_end += step
#             idx+=1
#
#     ySamples = np.concatenate((ySamples, np.array(yS)), axis=0)
#
#     return samples, ySamples

def augmentIncrease(samples, fires, loop_times):
    new_samples = []

    for x in range(loop_times):
        for s in samples:
            new_window = []
            for i in range(len(s)-1):
                l = [(s[i][0] * (1.2 * x)), abs((s[i][1] - (.08 * x))), s[i][2], s[i][3], s[i][4], (s[i][5] * (1.2 * x))]
                new_window.append(np.array(l))
            new_window.append(s[len(s)-1])
            new_samples.append(np.array(new_window))

    return np.concatenate((samples, np.array(new_samples)), axis=0) #samples + new_samples
