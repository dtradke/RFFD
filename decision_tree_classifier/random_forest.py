#Import scikit-learn dataset library
from sklearn import datasets
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
import time

DEPTH = 90
tree_number = 50

# Load dataset
# iris = datasets.load_iris()
# #
# # # print the label species(setosa, versicolor,virginica)
# # # print(iris.target_names)
# # #
# # # print the names of the four features
# # # print(iris.feature_names)
# # #
# # # print the iris data (top 5 records)
# # # print(iris.data[0:5])
# # # exit()
# # #
# # # print the iris labels (0:setosa, 1:versicolor, 2:virginica)
# # # print(iris.target)
# # # exit()
# #
# data=pd.DataFrame({
#     'sepal length':iris.data[:,0],
#     'sepal width':iris.data[:,1],
#     'petal length':iris.data[:,2],
#     'petal width':iris.data[:,3],
#     'species':iris.target
# })
# #
# # # print(data.head())
# #
# X=data[['sepal length', 'sepal width', 'petal length', 'petal width']]  # Features
# y=data['species']  # Labels
# #
# # # Split dataset into training set and test set
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)
#
# print(X_train)
# exit()

# print(X_train)
# exit()
#
# #Create a Gaussian Classifier
# clf=RandomForestClassifier(n_estimators=100)
#
# #Train the model using the training sets y_pred=clf.predict(X_test)
# clf.fit(X_train,y_train)
#
# y_pred=clf.predict(X_test)
#
# print("Accuracy:",metrics.accuracy_score(y_test, y_pred))



# RFire
# 11/16/19
#
# This code runs the decision tree classifier for RFire. Good latency and detencion results!

# Importing the required packages
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
import preprocess



# Function to calculate accuracy
# def cal_accuracy(y_test, y_pred):
#
#     print("Confusion Matrix: ",
#         confusion_matrix(y_test, y_pred))
#
#     print ("Accuracy : ",
#     accuracy_score(y_test,y_pred)*100)
#
#     print("Report : ",
#     classification_report(y_test, y_pred))

def flattenData(X):
    oneDimSamples = []

    for i in range(X.shape[0]):
        window = []
        for j in range(X[i].shape[0] - 1):
            for a in range(X[i][j].shape[0] - 1):
                window.append(X[i][j][a])
            normal_reflection = np.array(X[i][j][-1:])
        window = np.array(window)
        window = np.concatenate((window, normal_reflection), axis=0)
        oneDimSamples.append(window)

    return np.array(oneDimSamples)


def timeSeriesTest(y_test, y_pred, leave_one_out, ignition_sample):
    correct = 0
    incorrect = 0
    FN = 0
    FP = 0
    TP = 0
    TN = 0
    FN_before_fire = 0
    fire_flag = False
    early_classification = 0
    false_alarm = 0
    real_false_alarm = 0
    ones_in_a_row = 0
    latency = 60

    for i, val in enumerate(y_test):
        if y_pred[i] == 0: #predicted no fire
            ones_in_a_row = 0
            if val == 0: #actual no fire
                correct += 1
                TN += 1
            else: #actual fire
                incorrect += 1
                FN += 1
                if not fire_flag:
                    FN_before_fire += 1
        else: # predicted fire
            ones_in_a_row += 1
            if leave_one_out and not fire_flag: #if it is a leave_one_out test and a fire hasn't been detected
                if val == 1 and ones_in_a_row == 2: #if it truly is a fire
                    fire_flag = True
                    # print(" \U0001f525 \U0001f525 \U0001f525 Detected fire at time ", i, " \U0001f525 \U0001f525 \U0001f525")
                    latency = i - ignition_sample
                    # print("Latency: ", latency, " second(s)")
                else: #fire detected but not a ground truth classified fire sample yet
                    future = y_test[i:i+5]

                    if ones_in_a_row == 2:
                        real_false_alarm+=1

                    #checking if a fire is close due to the labeling format
                    if 1 not in future:
                        # print("\U0000274C \U0000274C \U0000274C FALSE ALARM at time ", i, " \U0000274C \U0000274C \U0000274C")
                        false_alarm += 1
                    else:
                        early_classification += 1

            if val == 1: #actual fire
                correct += 1
                TP += 1
            else: # actual no fire
                incorrect += 1
                FP += 1

    # print("Correct: ", correct)
    # print("incorrect: ", incorrect)
    # print("correct perc: ", correct/len(y_test))
    # print("TP: ", TP)
    # print("TN: ", TN)
    # print("FP (annoying) - no fire but predicted fire:     ", FP)
    # print("FN (bad) - actual fire but not predicted fire:  ", FN)
    # # print("Predicted no fire before time when fire starts: ", FN_before_fire)
    # print('Early Classification: ', early_classification)
    # print('False Alarms: ', false_alarm)
    # print('REAL False Alarms: ', real_false_alarm)

    if leave_one_out:
        results = [latency, TP, TN, real_false_alarm, FN]
    else:
        results = [latency, TP, TN, FP, FN]

    return results

def random_forest(X_train, X_test, y_train, y_test, mode):

    clf=RandomForestClassifier(n_estimators=tree_number, criterion=mode, max_depth=DEPTH)

    #Train the model using the training sets y_pred=clf.predict(X_test)
    clf.fit(X_train,y_train)

    y_pred=clf.predict(X_test)

    # print("Y_TEST")
    # print(y_test)
    #
    # print('Y PREDICT')
    # print(y_pred)

    # print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
    return y_pred


def write_df_to_csv(write_df, leave_one_out):
    if leave_one_out:
        dir = 'RRF_results/'
    else:
        dir = 'RRF_sample_wise_results/'
    save_str = 'RRF_10_times_depth_' + str(DEPTH) + '_trees_' + str(tree_number)
    today_string = time.strftime("%Y%m%d-%H%M%S")
    filename =  dir + today_string + save_str + '.csv'
    write_df.to_csv(filename, index=None, header=True)



# Driver code
def main():

    leave_one_out = False

    for j in range(10):

        # Building Phase
        X, y, X_test, Y_test, ignition_sample = preprocess.loadDataset(leave_one_out)

        # oneDimSamples = flattenData(X)
        # X, Y, X_train, X_test, y_train, y_test = splitdataset(oneDimSamples, y)

        X_train = flattenData(X)
        X_test = flattenData(X_test)
        y_train = y
        y_test = Y_test


        cols = []
        result = ["result"]
        for i in range(0, len(X_train[0])):
            cols.append("col" + str(i))

        X_train_df = pd.DataFrame(columns=cols)
        X_test_df = pd.DataFrame(columns=cols)
        y_train_df = pd.DataFrame(columns=result)
        y_test_df = pd.DataFrame(columns=result)

        for i in range(X_train.shape[0]):
            X_train_df.loc[i] = X_train[i]

        for i in range(X_test.shape[0]):
            X_test_df.loc[i] = X_test[i]

        for i in range(y_train.shape[0]):
            y_train_df.loc[i] = y_train[i]

        for i in range(y_test.shape[0]):
            y_test_df.loc[i] = y_test[i]

        y_train_df=y_train_df.astype('int')
        y_test_df=y_test_df.astype('int')

        gini_results_arr = [tree_number, DEPTH]
        entropy_results_arr = [tree_number, DEPTH]


        y_pred_gini = random_forest(X_train_df, X_test_df, y_train_df, y_test_df, 'gini')
        return_arr = timeSeriesTest(y_test, y_pred_gini, leave_one_out, ignition_sample)
        full_gini_results = np.array([gini_results_arr + return_arr])

        y_pred_entropy = random_forest(X_train_df, X_test_df, y_train_df, y_test_df, 'entropy')
        return_arr = timeSeriesTest(y_test, y_pred_entropy, leave_one_out, ignition_sample)
        full_entropy_results = np.array([entropy_results_arr + return_arr])

        if j == 0:
            np_gini_results = np.array(full_gini_results)
            np_entropy_results = np.array(full_entropy_results)
            print("Gini:    ", full_gini_results)
            print("Entropy: ", full_entropy_results)
        else:
            print("Gini:    ", full_gini_results)
            print("Entropy: ", full_entropy_results)
            #find averages
            np_gini_results = np.add(np_gini_results, np.array(full_gini_results))
            np_entropy_results = np.add(np_entropy_results, np.array(full_entropy_results))

            np_gini_results = np.divide(np_gini_results, 2)
            np_entropy_results = np.divide(np_entropy_results, 2)


    if leave_one_out:
        final_cols = ['tree_number', 'tree_depth', 'latency', 'TP', 'TN', 'false_alarm', 'failed_to_detect']
    else:
        final_cols = ['tree_number', 'tree_depth', 'latency', 'TP', 'TN', 'wrongly_classified', 'failed_to_detect']

    gini_df = pd.DataFrame.from_records(np_gini_results, columns=final_cols)
    entropy_df = pd.DataFrame.from_records(np_entropy_results, columns=final_cols)

    print(gini_df)
    print(entropy_df)

    write_df_to_csv(gini_df, leave_one_out)
    write_df_to_csv(entropy_df, leave_one_out)


    print("RANDOM FOREST DEPTH: ", DEPTH)
    print("TREES: ", tree_number)


# Calling main function
if __name__=="__main__":
    main()
