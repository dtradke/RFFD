# RFire
# 11/16/19
#
# This code runs the decision tree classifier for RFire. Good latency and detencion results!

# Importing the required packages
import numpy as np
from numpy import genfromtxt
import pandas as pd
from sklearn import tree
from sklearn.metrics import confusion_matrix
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from keras.models import Sequential, Model
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.utils import to_categorical
from keras.models import load_model
import preprocess
import sys
import autoencoder
import time
import graphviz

DEPTH = 90


# Function to split the dataset
def splitdataset(X, Y):
    # Spliting the dataset into train and test
    X_train, X_test, y_train, y_test = train_test_split(
    X, Y, test_size = 0.3, random_state = 100)

    return X, Y, X_train, X_test, y_train, y_test

# Function to perform training with giniIndex.
def train_using_gini(X_train, X_test, y_train):
    global DEPTH

    # DEPTH += 1
    # print()
    # print("Tree Depth GINI: ", DEPTH)

    # Creating the classifier object
    clf_gini = DecisionTreeClassifier(criterion = "gini",
            random_state = 50,max_depth=DEPTH, min_samples_leaf=1)

    # Performing training
    clf_gini.fit(X_train, y_train)

    return clf_gini

# Function to perform training with entropy.
def train_using_entropy(X_train, X_test, y_train):
    global DEPTH

    # print()
    # print("Tree Depth ENTROPY: ", DEPTH)

    # Decision tree with entropy
    clf_entropy = DecisionTreeClassifier(
            criterion = "entropy", random_state = 50,
            max_depth = DEPTH, min_samples_leaf = 1) #3 depth

    # Performing training
    clf_entropy.fit(X_train, y_train)
    return clf_entropy


# Function to make predictions
def prediction(X_test, clf_object):

    # Predicton on test with giniIndex
    y_pred = clf_object.predict(X_test)
    # print("Predicted values:")
    # print(y_pred)
    return y_pred

# Function to calculate accuracy
def cal_accuracy(y_test, y_pred):

    print("Confusion Matrix: ", confusion_matrix(y_test, y_pred))

    print ("Accuracy : ", accuracy_score(y_test,y_pred)*100)

    print("Report : ", classification_report(y_test, y_pred))

def flattenData(X):
    oneDimSamples = []

    for i in range(X.shape[0]):
        window = []
        for j in range(X[i].shape[0]):
            for a in range(X[i][j].shape[0] - 1):
                window.append(X[i][j][a])
            normal_reflection = np.array(X[i][j][-1:])
        window = np.array(window)
        window = np.concatenate((window, normal_reflection), axis=0)
        oneDimSamples.append(window)

    return np.array(oneDimSamples)

def timeSeriesTest(y_test, y_pred, leave_one_out, ignition_sample):
    correct = 0
    incorrect = 0
    FN = 0
    FP = 0
    TP = 0
    TN = 0
    FN_before_fire = 0
    fire_flag = False
    early_classification = 0
    false_alarm = 0
    real_false_alarm = 0
    ones_in_a_row = 0
    latency = 60

    for i, val in enumerate(y_test):
        if y_pred[i] == 0: #predicted no fire
            ones_in_a_row = 0
            if val == 0: #actual no fire
                correct += 1
                TN += 1
            else: #actual fire
                incorrect += 1
                FN += 1
                if not fire_flag:
                    FN_before_fire += 1
        else: # predicted fire
            ones_in_a_row += 1
            if leave_one_out and not fire_flag: #if it is a leave_one_out test and a fire hasn't been detected
                if val == 1 and ones_in_a_row == 2: #if it truly is a fire
                    fire_flag = True
                    # print(" \U0001f525 \U0001f525 \U0001f525 Detected fire at time ", i, " \U0001f525 \U0001f525 \U0001f525")
                    latency = i - ignition_sample
                    # print("Latency: ", latency, " second(s)")
                else: #fire detected but not a ground truth classified fire sample yet
                    future = y_test[i:i+5]

                    if ones_in_a_row == 2:
                        real_false_alarm+=1

                    #checking if a fire is close due to the labeling format
                    if 1 not in future:
                        # print("\U0000274C \U0000274C \U0000274C FALSE ALARM at time ", i, " \U0000274C \U0000274C \U0000274C")
                        false_alarm += 1
                    else:
                        early_classification += 1

            if val == 1: #actual fire
                correct += 1
                TP += 1
            else: # actual no fire
                incorrect += 1
                FP += 1

    # print("Correct: ", correct)
    # print("incorrect: ", incorrect)
    # print("correct perc: ", correct/len(y_test))
    # print("TP: ", TP)
    # print("TN: ", TN)
    # print("FP (annoying) - no fire but predicted fire:     ", FP)
    # print("FN (bad) - actual fire but not predicted fire:  ", FN)
    # # print("Predicted no fire before time when fire starts: ", FN_before_fire)
    # print('Early Classification: ', early_classification)
    # print('False Alarms: ', false_alarm)
    # print('REAL False Alarms: ', real_false_alarm)

    if leave_one_out:
        results = [latency, TP, TN, real_false_alarm, FN]
    else:
        results = [latency, TP, TN, FP, FN]

    return results

#run the autoencoder
def autoencode(leave_one_out, autoencode_str=None):
    trainX, trainY, testX, testY, ignition_sample = preprocess.loadDataset(leave_one_out)
    if autoencode_str is None:
        epochs = 100
        autoencode, encoder = autoencoder.AutoEncodeModel(trainX, testX, epochs=epochs)
        time_string = time.strftime("%Y%m%d-%H%M%S")
        fname = 'RDT_autoencoder_models/' + time_string + '_' + str(epochs) + 'epochs_RDT_leave1mout.h5'
        autoencode.save(fname)
    else:
        autoencode = load_model('RDT_autoencoder_models/' + autoencode_str)
    encoder_trained = Model(autoencode.input, autoencode.layers[-8].output)
    # fullX = np.concatenate((trainX, testX), axis=0)
    # fullY = np.concatenate((trainY, testY), axis=0)
    X_train_encoded = encoder_trained.predict(trainX)
    X_test_encoded = encoder_trained.predict(testX)
    print("Encoded")
    return X_train_encoded, X_test_encoded, trainY, testY, ignition_sample
    # unsupervisedCluster(encoded, fullY, cluster_type='dbscan', autoencoder_mode=True)

def write_df_to_csv(write_df, leave_one_out):
    if leave_one_out:
        dir = 'RDT_results/'
    else:
        dir = 'RDT_sample_wise_results/'
    save_str = 'RDT_10_times_depth_' + str(DEPTH)
    today_string = time.strftime("%Y%m%d-%H%M%S")
    filename =  dir + today_string + save_str + '.csv'
    write_df.to_csv(filename, index=None, header=True)

# Driver code
def main():
    global DEPTH

    leave_one_out = False

    for i in range(10):
        print("loop: ", i)

        #autoencoder: python3 decision_tree_numpy.py autoencode [model]
        try:
            if sys.argv[1] == 'autoencode':
                print('Autoencode')
                if len(sys.argv) == 3:
                    X_train, X_test, y_train, y_test, ignition_sample = autoencode(leave_one_out, sys.argv[2])
                else:
                    X_train, X_test, y_train, y_test, ignition_sample = autoencode(leave_one_out)

        # except:
            elif sys.argv[1][-1:] == '/':
                dir = 'leave_one_out_datasets/'
                X_train = genfromtxt(dir + 'X_train.csv', delimiter=',')
                X_test = genfromtxt(dir + 'X_test.csv', delimiter=',')
                y_train = genfromtxt(dir + 'y_train.csv', delimiter=',')
                y_test = genfromtxt(dir + 'y_test.csv', delimiter=',')
                ignition_sample = 334 #TODO: RECORD THIS
        except:
            print('not autoencodeing')

            # Building Phase
            X, y, X_test, Y_test, ignition_sample = preprocess.loadDataset(leave_one_out)

            X_train = flattenData(X)
            X_test = flattenData(X_test)
            y_train = y
            y_test = Y_test

            print("Training shape: ", X_train.shape)
            print("Testing shape:  ", X_test.shape)

            if leave_one_out:
                dir = 'leave_one_out_datasets/'
            else:
                dir = 'sample_wise_datasets/'
            time_string = time.strftime("%Y%m%d-%H%M%S")
            np.savetxt(dir + time_string + 'X_train.csv', X_train, delimiter=',')
            np.savetxt(dir + time_string + 'X_test.csv', X_test, delimiter=',')
            np.savetxt(dir + time_string + 'y_train.csv', y_train, delimiter=',')
            np.savetxt(dir + time_string + 'y_test.csv', y_test, delimiter=',')

        if leave_one_out:
            cols = ['tree_depth', 'latency', 'TP', 'TN', 'false_alarm', 'failed_to_detect']
        else:
            cols = ['tree_depth', 'latency', 'TP', 'TN', 'wrongly_classified', 'failed_to_detect']


        gini_results_arr = [DEPTH]
        entropy_results_arr = [DEPTH]

        clf_gini = train_using_gini(X_train, X_test, y_train)
        clf_entropy = train_using_entropy(X_train, X_test, y_train)

        # Operational Phase
        # print("Results Using Gini Index:")

        # Prediction using gini
        y_pred_gini = prediction(X_test, clf_gini)
        return_arr = timeSeriesTest(y_test, y_pred_gini, leave_one_out, ignition_sample)
        full_gini_results = np.array([gini_results_arr + return_arr])
        # cal_accuracy(y_test, y_pred_gini)

        # print("Results Using Entropy:")
        # Prediction using entropy
        y_pred_entropy = prediction(X_test, clf_entropy)
        return_arr = timeSeriesTest(y_test, y_pred_entropy, leave_one_out, ignition_sample)
        full_entropy_results = np.array([entropy_results_arr + return_arr])
        # cal_accuracy(y_test, y_pred_entropy)

        if i == 0:
            np_gini_results = np.array(full_gini_results)
            np_entropy_results = np.array(full_entropy_results)
            print("Gini:    ", full_gini_results)
            print("Entropy: ", full_entropy_results)
        else:
            print("Gini:    ", full_gini_results)
            print("Entropy: ", full_entropy_results)
            #find averages
            np_gini_results = np.add(np_gini_results, np.array(full_gini_results))
            np_entropy_results = np.add(np_entropy_results, np.array(full_entropy_results))

            np_gini_results = np.divide(np_gini_results, 2)
            np_entropy_results = np.divide(np_entropy_results, 2)

    gini_df = pd.DataFrame.from_records(np_gini_results, columns=cols)
    entropy_df = pd.DataFrame.from_records(np_entropy_results, columns=cols)

    print(gini_df)
    print(entropy_df)

    write_df_to_csv(gini_df, leave_one_out)
    write_df_to_csv(entropy_df, leave_one_out)



# Calling main function
if __name__=="__main__":
    main()
