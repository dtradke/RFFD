# RFire
# 11/21/19
#
# Works with the range profile code starting with process_dat.py

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
from os import listdir
import sample_factory
import math

isglobalmax_flat = []
isglobalmin_flat = []
object_count = 1
fire_count = 0
nonfire_count = 0
fire_bins = {}

num_top = -3 #was -3 for v1

class Experiment(object):
    def __init__(self, name='No Name', sig_objects=[], top=[], fire_bin_list=[], max_reflection=0, min_reflection=10000000000, leave_one_out=False):
        self.name = name
        self.objects = {}
        self.top = top
        self.fire_bin_list = fire_bin_list
        self.max_reflection = max_reflection
        self.min_reflection = min_reflection
        self.leave_one_out = leave_one_out
        self.max_perc_fluc = 0
        self.min_perc_fluc = 10000000000000
        self.max_diff = 0
        self.min_diff = 1000000000000
        self.max_degree = 0
        self.min_degree = 360
        self.max_fluc = 0
        for o in sig_objects:
            self.objects[o.distance] = o

        # [l_perc_fluc, l_diff, math.degrees(math.atan(l_linear_regression[0]/len_sample)), l_obj.distance, l_obj.normal_reflection]

        split_scen = name.split('_')
        if 'fire' in split_scen:
            self.fire = True
        else:
            self.fire = False

    def graphExperiment(self):
        all_same_plot = []
        for i in self.objects.keys():
            all_same_plot.append(self.objects[i].reflection)
        time = list(range(len(all_same_plot[0])))
        for i, a in enumerate(all_same_plot):
            plt.plot(time, a)
        plt.ylim(bottom=0) #, top=250000000000)
        plt.xlim(left=0)
        # plt.legend(loc=1, ncol=2, frameon=True)
        plt.ylabel('Regression Power', fontsize=15)
        plt.xlabel('Timesteps', fontsize=15)
        plt.title(self.name, fontsize=15)
        plt.show()

    def augmentIncrease(self, loop_times=4):
        augment_rate = 1.02 #1.05
        new_experiments = []
        for x in range(1, loop_times+1):
            augmented_objects = []
            new_max_reflection = self.max_reflection * (augment_rate * x)
            new_min_reflection = self.min_reflection * (augment_rate * x)
            for o in self.objects.keys():
                obj = self.objects[o]
                new_id = obj.id
                if obj.fire:
                    new_reflection = [val * (augment_rate * x) for i, val in enumerate(obj.reflection)]
                    if max(new_reflection) > 5000000000000 or average(new_reflection[:300]) > 400000000000:
                        return new_experiments
                    new_distance = obj.distance
                    new_max_fluc = obj.max_fluc * (augment_rate * x)
                    new_normal_reflection = obj.normal_reflection * (augment_rate * x)
                    fire_start_interpolate = obj.fire_start # - int((self.fire_start - previous_obj.fire_start) * (x/loop_times))
                else:
                    new_reflection = obj.reflection
                    new_distance = obj.distance
                    new_normal_reflection = obj.normal_reflection
                    fire_start_interpolate = obj.fire_start
                    new_max_fluc = obj.max_fluc
                augmented_objects.append(SigObject(direction=90, distance=new_distance, reflection=new_reflection, scenario=obj.scenario, fire_start=fire_start_interpolate, max_fluc=new_max_fluc))

            top_avg_reflections = []
            for i, col in enumerate(augmented_objects):
                reflections = col.reflection
                top_avg_reflections.append(average(reflections[:300]))
            top = sorted(top_avg_reflections)[num_top:]

            new_experiments.append(Experiment(name=(self.name + "_augmentinc_" + str(x)), sig_objects=augmented_objects, top=top, fire_bin_list=self.fire_bin_list, max_reflection=new_max_reflection, min_reflection=new_min_reflection))
        return new_experiments

    def augmentDecrease(self, loop_times=4):
        augment_rate = 0.98 #0.95
        new_experiments = []
        for x in range(1, loop_times+1):
            augmented_objects = []
            new_max_reflection = self.max_reflection
            new_min_reflection = self.min_reflection
            for i in range(1, x+1):
                new_max_reflection = new_max_reflection * augment_rate
                new_min_reflection = new_min_reflection * augment_rate
            for o in self.objects.keys():
                obj = self.objects[o]
                new_id = obj.id
                if obj.fire:
                    new_reflection = obj.reflection
                    new_normal_reflection = obj.normal_reflection
                    for j in range(1, x+1):
                        new_max_fluc = obj.max_fluc * augment_rate
                        for i, val in enumerate(obj.reflection):
                            new_reflection[i] = new_reflection[i] * augment_rate
                        new_normal_reflection = new_normal_reflection * augment_rate
                    if max(new_reflection) > 5000000000000 or average(new_reflection[:300]) > 4000000000:
                        return new_experiments
                    new_distance = obj.distance
                    fire_start_interpolate = obj.fire_start # - int((self.fire_start - previous_obj.fire_start) * (x/loop_times))
                else:
                    new_reflection = obj.reflection
                    new_distance = obj.distance
                    new_normal_reflection = obj.normal_reflection
                    fire_start_interpolate = obj.fire_start
                    new_max_fluc = obj.max_fluc
                augmented_objects.append(SigObject(direction=90, distance=new_distance, reflection=new_reflection, scenario=obj.scenario, fire_start=fire_start_interpolate, max_fluc=new_max_fluc))

            top_avg_reflections = []
            for i, col in enumerate(augmented_objects):
                reflections = col.reflection
                top_avg_reflections.append(average(reflections[:300]))
            top = sorted(top_avg_reflections)[num_top:]

            new_experiments.append(Experiment(name=(self.name + "_augmentdec_" + str(x)), sig_objects=augmented_objects, top=top, fire_bin_list=self.fire_bin_list, max_reflection=new_max_reflection, min_reflection=new_min_reflection))
        return new_experiments

    def augmentShift(self, loop_times=4):
        augment_rate = 0.999 #1.05
        shift_unit = 10
        new_experiments = []
        for x in range(1, loop_times + 1):
            shift = (shift_unit * x)
            augmented_objects = []
            new_max_reflection = self.max_reflection
            new_min_reflection = self.min_reflection
            for i in range(1, x+1):
                new_max_reflection = new_max_reflection * augment_rate
                new_min_reflection = new_min_reflection * augment_rate
            new_bins_list = [i - (x*10) for i in self.fire_bin_list]

            if self.fire:
                if min(new_bins_list) < 10:
                    return new_experiments
            for j, o in enumerate(self.objects.keys()):
                if o < shift:
                    continue
                obj = self.objects[o]
                new_id = obj.id - shift

                new_reflection = obj.reflection
                new_normal_reflection = obj.normal_reflection
                for a in range(1, x+1):
                    for i, val in enumerate(obj.reflection):
                        new_reflection[i] = new_reflection[i] * augment_rate
                    new_normal_reflection = new_normal_reflection * augment_rate
                    new_max_fluc = obj.max_fluc * augment_rate

                if max(new_reflection) > 5000000000000 or average(new_reflection[:300]) > 400000000000:
                    return new_experiments
                new_distance = obj.distance - shift
                augmented_objects.append(SigObject(direction=90, distance=new_distance, reflection=new_reflection, scenario=obj.scenario, stationary=obj.stationary, fire_start=obj.fire_start, max_fluc=new_max_fluc))
            top_avg_reflections = []
            for i, col in enumerate(augmented_objects):
                reflections = col.reflection
                top_avg_reflections.append(average(reflections[:300]))
            top = sorted(top_avg_reflections)[num_top:]

            if len(augmented_objects) == 0:
                return new_experiments

            new_experiments.append(Experiment(name=(self.name + "_augmentshift_" + str(x)), sig_objects=augmented_objects, top=top, fire_bin_list=new_bins_list, max_reflection=new_max_reflection, min_reflection=new_min_reflection))
        return new_experiments

    def augmentShiftBack(self, loop_times=4):
        augment_rate = 1.001 #0.95
        shift_unit = 10
        new_experiments = []
        for x in range(1, loop_times + 1):
            shift = (shift_unit * x)
            augmented_objects = []
            new_max_reflection = self.max_reflection
            new_min_reflection = self.min_reflection
            for i in range(1, x+1):
                new_max_reflection = new_max_reflection * augment_rate
                new_min_reflection = new_min_reflection * augment_rate
            new_bins_list = [i + (x*10) for i in self.fire_bin_list]
            if self.fire:
                if max(new_bins_list) > 200:
                    return new_experiments
            for j, o in enumerate(self.objects.keys()):
                if o > (255 - shift):
                    continue
                obj = self.objects[o]
                new_id = obj.id + shift

                new_reflection = obj.reflection
                new_normal_reflection = obj.normal_reflection
                for a in range(1, x+1):
                    for i, val in enumerate(obj.reflection):
                        new_reflection[i] = new_reflection[i] * augment_rate
                    new_normal_reflection = new_normal_reflection * augment_rate
                    new_max_fluc = obj.max_fluc * augment_rate
                if min(new_reflection) < 500 or average(new_reflection[:300]) < 4000:
                    return new_experiments
                new_distance = obj.distance + shift
                augmented_objects.append(SigObject(direction=90, distance=new_distance, reflection=new_reflection, scenario=obj.scenario, stationary=obj.stationary, fire_start=obj.fire_start, max_fluc=new_max_fluc))

            top_avg_reflections = []
            for i, col in enumerate(augmented_objects):
                reflections = col.reflection
                top_avg_reflections.append(average(reflections[:300]))
            top = sorted(top_avg_reflections)[num_top:]

            if len(augmented_objects) == 0:
                return new_experiments

            new_experiments.append(Experiment(name=(self.name + "_augmentshiftback_" + str(x)), sig_objects=augmented_objects, top=top, fire_bin_list=new_bins_list, max_reflection=new_max_reflection, min_reflection=new_min_reflection))
        return new_experiments

    def parallelObjectSampleCreation(self, pass_arr):
        obj_dist = pass_arr[0] #[obj_dists, cnn, leave_one_out, svm]
        cnn = pass_arr[1]
        leave_one_out = pass_arr[2]
        svm = pass_arr[3]
        flat = pass_arr[4]
        hybrid = pass_arr[5]

        fire_samples, nonfire_samples, leave_one_out_samples_fire, leave_one_out_samples_nofire, fires, nonfires, leave_one_out_samples, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire = [], [], [], [], [], [], [], [], [], [], [], [], []

        if obj_dist not in list(self.objects.keys()):
            if flat:
                return [np.array(fire_samples), np.array(nonfire_samples), np.array(leave_one_out_fire), np.array(leave_one_out_nofire)]
            # print('return 1')
            return [fire_samples, nonfire_samples, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire]
        if ((obj_dist+2) not in list(self.objects.keys())) or ((obj_dist-2) not in list(self.objects.keys())) or ((obj_dist-1) not in list(self.objects.keys())) or ((obj_dist+1) not in list(self.objects.keys())):
            if flat:
                return [np.array(fire_samples), np.array(nonfire_samples), np.array(leave_one_out_fire), np.array(leave_one_out_nofire)]
            # print('return 2')
            return [fire_samples, nonfire_samples, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire]

        obj = self.objects[obj_dist]
        # print("Making samples for: ", o)
        # if obj_dist < (list(self.objects.keys())[:1][0] + 2) or obj_dist > (list(self.objects.keys())[-1:][0] - 2):

        start_time_pass = 0
        end_time_pass = obj.len_reflection

        if self.fire:
            # if obj.scenario == leave_one_out and obj.distance < (self.fire_bin_list[-1:][0] + 1):# + 10: # THIS LIMITS THE TESTING SEARCH SPACE TO JUST BEHIND THE FIRE
            if obj.scenario == leave_one_out and obj.distance < (min(self.fire_bin_list) + 35):
                leave_one_out_fire, leave_one_out_nofire, skipped_samples, hybrid_loo_fire, hybrid_loo_nofire = sample_factory.makeSamples(self, obj, start_time_pass, end_time_pass, flat, hybrid=hybrid)
            else:
                fires, nonfires, skipped_samples, hybrid_fire, hybrid_nofire = sample_factory.makeSamples(self, obj, start_time_pass, end_time_pass, flat, hybrid=hybrid)
        else:
            fires, nonfires, skipped_samples, hybrid_fire, hybrid_nofire = sample_factory.makeSamples(self, obj, start_time_pass, end_time_pass, flat, hybrid=hybrid)

        return [fires, nonfires, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire]


    def nonParallelObjectSampleCreation(self, obj_dist, cnn, leave_one_out, svm, flat, hybrid):
        fire_samples, nonfire_samples, leave_one_out_samples_fire, leave_one_out_samples_nofire, fires, nonfires, leave_one_out_samples, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire = [], [], [], [], [], [], [], [], [], [], [], [], []

        if obj_dist not in list(self.objects.keys()):
            if flat:
                return [np.array(fire_samples), np.array(nonfire_samples), np.array(leave_one_out_fire), np.array(leave_one_out_nofire)]
            # print('return 1')
            return [fire_samples, nonfire_samples, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire]
        if ((obj_dist+2) not in list(self.objects.keys())) or ((obj_dist-2) not in list(self.objects.keys())) or ((obj_dist-1) not in list(self.objects.keys())) or ((obj_dist+1) not in list(self.objects.keys())):
            if flat:
                return [np.array(fire_samples), np.array(nonfire_samples), np.array(leave_one_out_fire), np.array(leave_one_out_nofire)]
            # print('return 2')
            return [fire_samples, nonfire_samples, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire]

        obj = self.objects[obj_dist]
        # print("Making samples for: ", o)
        # if obj_dist < (list(self.objects.keys())[:1][0] + 2) or obj_dist > (list(self.objects.keys())[-1:][0] - 2):

        start_time_pass = 0
        end_time_pass = obj.len_reflection

        if self.fire:
            # if obj.scenario == leave_one_out and obj.distance < (self.fire_bin_list[-1:][0] + 1):# + 10: # THIS LIMITS THE TESTING SEARCH SPACE TO JUST BEHIND THE FIRE
            if obj.scenario == leave_one_out and obj.distance < (min(self.fire_bin_list) + 35):
                leave_one_out_fire, leave_one_out_nofire, skipped_samples, hybrid_loo_fire, hybrid_loo_nofire = sample_factory.makeSamples(self, obj, start_time_pass, end_time_pass, flat, hybrid=hybrid)
            else:
                fires, nonfires, skipped_samples, hybrid_fire, hybrid_nofire = sample_factory.makeSamples(self, obj, start_time_pass, end_time_pass, flat, hybrid=hybrid)
        else:
            fires, nonfires, skipped_samples, hybrid_fire, hybrid_nofire = sample_factory.makeSamples(self, obj, start_time_pass, end_time_pass, flat, hybrid=hybrid)

        return [fires, nonfires, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire]




    def __repr__(self):
        return "Experiment(Name: {}, Fire: {}, Number of Objects: {}, Fire bins: {}, Max: {}, Min: {})".format(self.name, self.fire, len(self.objects.keys()), self.fire_bin_list, self.max_reflection, self.min_reflection)


# Creates objects for the bins of interest for each experiment
class SigObject(object):

    def __init__(self, id=object_count, direction=90, distance=0, reflection=[], scenario='Unnamed', stationary=True, close_occlusions=None, far_occlusions=None, fire_start=0, augmented=0, significant=False, max_fluc=0):
        global object_count
        if augmented == 0:
            self.id = object_count
        else:
            self.id = augmented
        self.direction = direction
        self.distance = distance
        self.reflection = reflection
        self.len_reflection = len(reflection)
        self.scenario = scenario
        self.normal_reflection = average(self.reflection[:300])
        self.fluctuation_range = max(self.reflection[:300]) - min(self.reflection[:300])
        self.fire_flag = False
        self.fire_start = fire_start
        self.significant = significant
        self.stationary = stationary
        self.max_fluc = max_fluc       # This is the fluctuation from line of best fit. Updated in sample_factory.py

        vars = []
        fft_vars = []
        max_sig_var_arr = []
        start_time = 0
        end_time = 300
        step = 10
        while end_time < 600:
            variance, max_variance,_ = sample_factory.getVariance(self.reflection[start_time:end_time])
            fft_variance,_,_ = sample_factory.getVariance(self.reflection[start_time:end_time], fft_bool=True)
            vars.append(variance)
            fft_vars.append(fft_variance)
            max_sig_var_arr.append(max_variance)
            start_time+=step
            end_time+=step
        self.baseline_variance = max(vars)
        self.baseline_max_variance = max(max_sig_var_arr)
        self.baseline_fft_variance = max(fft_vars)
        object_count+=1

        if far_occlusions is None:
            self.close_occlusions = [0] * len(self.reflection)
            self.far_occlusions = [0] * len(self.reflection)
        else:
            self.close_occlusions = close_occlusions
            self.far_occlusions = far_occlusions


        split_scen = scenario.split('_')
        if 'fire' in split_scen and stationary:
            self.fire = True
        else:
            self.fire = False



    def object_reflection(self):
        # fig = plt.figure()
        # ax = fig.add_subplot(1,1,1,axisbg='1.0')
        # ax.scatter(x, y, alpha=0.7, edgecolors = 'none', s=30)
        time = list(range(len(self.reflection)))
        plt.plot(time, self.reflection)
        plt.ylim(bottom=0, top=400000000000)
        plt.xlim(left=0)
        plt.title("Scen: " + self.scenario + ", Object: " + str(self.id) + ", Distance: " + str(self.distance), fontsize=15)
        plt.ylabel('Reflection Power', fontsize=15)
        plt.xlabel('Timesteps', fontsize=15)
        plt.show()

    def augmentIncrease(self, loop_times=6):
        new_objects = []
        for x in range(1, loop_times):
            new_id = (self.id * 10) + x
            # new_reflection = [val - ((val - previous_obj.reflection[i]) * (x/loop_times)) for i, val in enumerate(self.reflection)]
            new_reflection = [val * (1.1 * x) for i, val in enumerate(self.reflection)]
            if max(new_reflection) > 5000000000000 or average(new_reflection[:300]) > 400000000000:
                return new_objects
            new_distance = self.distance
            for j in range(x):
                new_distance = new_distance * .9
            # new_normal_reflection = self.normal_reflection - ((self.normal_reflection - previous_obj.normal_reflection) * (x/loop_times))
            new_normal_reflection = self.normal_reflection * (1.1 * x)
            fire_start_interpolate = self.fire_start # - int((self.fire_start - previous_obj.fire_start) * (x/loop_times))
            augmented = (self.id * 10) + x
            new_objects.append(SigObject(direction=90, distance=new_distance, reflection=new_reflection, scenario=self.scenario, close_occlusions=self.close_occlusions, far_occlusions=self.far_occlusions, fire_start=fire_start_interpolate, augmented=round(augmented, 1)))
        return new_objects

    def augmentDecrease(self, loop_times=3):
        new_objects = []
        for x in range(1, loop_times):
            new_id = (self.id * 100) + x
            new_reflection = self.reflection
            new_normal_reflection = self.normal_reflection
            new_distance = self.distance
            for j in range(x):
                new_reflection = [val * 0.9 for i, val in enumerate(new_reflection)]
                new_normal_reflection = new_normal_reflection * 0.9
                new_distance = new_distance * 1.1
            if min(new_reflection) < 0 or average(new_reflection[:300]) < 100000:
                return new_objects
            # new_normal_reflection = self.normal_reflection - ((self.normal_reflection - previous_obj.normal_reflection) * (x/loop_times))
            fire_start_interpolate = self.fire_start # - int((self.fire_start - previous_obj.fire_start) * (x/loop_times))
            augmented = (self.id * 100) + x
            new_objects.append(SigObject(direction=90, distance=new_distance, reflection=new_reflection, scenario=self.scenario, close_occlusions=self.close_occlusions, far_occlusions=self.far_occlusions, fire_start=fire_start_interpolate, augmented=round(augmented, 1)))
        return new_objects

    def augmentInterpolate(self, previous_obj, loop_times=6):
        new_objects = []
        for x in range(1, loop_times):
            new_id = (self.id * 1000) + x
            new_reflection = [val - ((val - previous_obj.reflection[i]) * (x/loop_times)) for i, val in enumerate(self.reflection)]
            # new_reflection = [val * (1.1 * x) for i, val in enumerate(self.reflection)]
            if max(new_reflection) > 500000000000 or average(new_reflection[:300]) > 300000000000:
                return new_objects
            new_distance = self.distance - (x/loop_times)
            new_normal_reflection = self.normal_reflection - ((self.normal_reflection - previous_obj.normal_reflection) * (x/loop_times))
            fire_start_interpolate = self.fire_start - int((self.fire_start - previous_obj.fire_start) * (x/loop_times))
            augmented = (self.id * 1000) + x
            new_objects.append(SigObject(direction=90, distance=new_distance, reflection=new_reflection, scenario=self.scenario, close_occlusions=self.close_occlusions, far_occlusions=self.far_occlusions, fire_start=fire_start_interpolate, augmented=round(augmented, 1)))
        return new_objects


    # def __repr__(self):
    #     return "SigObject(id: {}, Fire?: {}, Fire Start: {}, direction: {}, distance: {}, Average Reflection: {}, Scenario: {})".format(self.id, self.fire, self.fire_start, self.direction, self.distance, average(self.reflection), self.scenario)
    def __repr__(self):
        return "SigObject(distance: {}, Fire?: {}, Start: {}, Avg Ref: {}, Sig: {})".format(self.distance, self.fire, self.fire_start, self.normal_reflection, self.significant)



def average(lst):
    return sum(lst) / len(lst)

def makeObjects(df, scenario, leave_one_out):
    sig_objects = []
    avg_reflections = []

    global fire_bins


    fire_bins = {'foam_fire_0m_device_1m':[25, 26, 27, 28], #21,
                'foam_bigger_fire_0m_device_3m':[73, 74, 75], #maybe 65, 66 Drywall: , 76, 77, 78, 79, 80 ,81

                'nonsoot_fire_0m_device_1m':[27, 28, 29], # DELETE
                'soot1_fire_0m_device_1m':[25, 26, 27], #27, 28, 29
                'nonsoot_fire_0m_device_3m':[72, 73],
                'soot1_fire_0m_device_3m':[75, 76, 77], #not good
                'nonsoot_fire_0m_device_5m':[119, 120, 121],
                'soot1_fire_0m_device_5m':[119, 120, 121], #maybe 118?
                'soot1_fire_0m_device_7m':[162, 165, 166, 167, 168], #weird

                'nonsoot_fire_1m_device_3m':[47, 48, 49],
                'soot1_fire_1m_device_3m':[47, 48, 49],
                'nonsoot_fire_1m_device_5m':[92, 93], #maybe 91    # DELETE
                'soot1_fire_1m_device_5m':[91, 92, 93],
                'soot1_fire_1m_device_7m':[137, 138, 139], #not good readings    #DELETE

                'nonsoot_fire_3m_sensor_5m':[47, 48], #maybe 45
                'soot1_fire_3m_device_5m':[47, 48], #maybe 49
                'soot1_fire_3m_device_7m':[91, 92], #more of a difference further back

                'soot1_fire_5m_device_7m':[47, 48, 49], #45, 46 also maybe

                'big_nonsoot_fire_85cm_device_198cm':[23, 24, 25],
                'big_soot1_fire_85cm_device_198cm':[23, 24, 25, 26],
                'big_nonsoot_fire_85cm_device_398cm':[68, 69, 70, 71, 72, 73, 74], #maybe 65 CHANGED: , 75, 76
                'big_soot1_fire_85cm_device_398cm':[66, 67, 68, 69, 70, 71], #maybe 65
                'big_nonsoot_fire_85cm_device_598cm':[112, 113, 114, 115, 116, 117, 118, 119, 120],
                'big_soot1_fire_85cm_device_598cm':[112, 113, 114, 115],
                'big_nonsoot_fire_85cm_device_798cm':[158, 159, 160],
                'big_soot1_fire_85cm_device_798cm':[158, 159, 160, 161],

                'big_soot1_fire_185cm_device_598cm':[87, 88, 89, 90],
                'big_nonsoot_fire_185cm_device_598cm':[87, 88, 89, 90], #, 91, 92, 93, 94

                'big_nonsoot_fire_385cm_device_598cm':[42, 43, 44, 45, 46, 47, 48, 49, 50], #maybe 41
                'big_soot1_fire_385cm_device_598cm':[42, 43, 44, 45, 46, 47],
                'big_soot2_fire_385cm_device_598cm':[42, 43, 44],

                'fire_1m':[27, 28, 29, 30],
                'fire_2m': [48, 49, 50, 51, 52],

                'tim_small_fire_1m': [25, 26],
                'tim_small_fire_3m': [71, 72, 73],
                'tim_small_fire_5m': [116, 117, 118, 119], #120
                'tim_small_fire_7m': [160, 161], #might take out
                'tim_large_fire_1m': [23, 24, 25, 26, 27], #, 28, 29, 30
                'tim_large_fire_3m': [69, 70, 71, 72], #, 73
                'tim_large_fire_5m': [116, 117, 118, 119], #120
                'tim_large_fire_7m': [161, 162, 163, 164], #160,
                'fire_occlusion_1m_device_3m': [71, 72, 73, 74],
                'fire_occlusion_1m_device_5m': [116, 117, 118, 119],
                'fire_occlusion_1m_device_7m': [161, 162, 163, 164], #160,
                'fire_occlusion_3m_device_5m': [117, 118], #might take out
                'fire_occlusion_3m_device_7m': [161, 162, 163, 164],
                'fire_occlusion_5m_device_7m': [161, 162, 163, 164]
                }

    split_scen = scenario.split('_')

    for i, col in enumerate(df.columns):
        if 'fire' in split_scen:
            # if i < 5 or i > (max(fire_bins[scenario]) + 5):
            if i < 5 or i > (min(fire_bins[scenario]) + 35):
                continue
        elif i < 5 or i > 200:
                continue
        reflections = df[col].tolist()
        # reflections = cutPersonInterfering(reflections)
        avg_reflections.append(average(reflections[:300]))

    top = sorted(avg_reflections)[num_top:] #CHANGED: top was 3... maybe for training, testing different

    significant_bins = []
    for i, col in enumerate(df.columns):
        reflections = df[col].tolist()
        # reflections = cutPersonInterfering(reflections)
        if max(reflections) > min(top) and i > 2 and i < 254:
            significant_bins.append(i)

    nonfire_object_dists = []
    created_object_dists = []
    reflections_2back = []
    reflections_1back = []
    reflections_1farther = []
    reflections_2farther = []
    is_fire_flag = False
    for i, col in enumerate(df.columns):
        reflections = df[col].tolist()
        # reflections = cutPersonInterfering(reflections)
        ref_baseline = average(reflections[:300])

        flag = False
        fluc_range = max(reflections[:300]) - min(reflections[:300])

        split_scen = scenario.split('_')
        if 'fire' in split_scen:
            is_fire_flag = True

# How to classify bins behind fire
# -------------------
            if scenario == leave_one_out:
                if i in fire_bins[scenario]:
                    flag = True
            else:

                # if i in fire_bins[scenario]: #only classifies bins on fire as "fire"
                if i >= min(fire_bins[scenario]): #Classifies all bins behind a fire as "fire"
                # if i >= min(fire_bins[scenario]) or ref_baseline > min(top): #classifies the fire, and the wall behind it as fire/could just append to the fire bins
                    flag = True

                # if i > (max(fire_bins[scenario]) + 2): #only trains with the bins up to the fire, none behind
                if i > (min(fire_bins[scenario]) + 37):
                    break
# -------------------



        if i < 3 or i > (len(df.columns) - 3):
            continue
        reflections_2back = df[df.columns[i-2]].tolist()
        reflections_1back = df[df.columns[i-1]].tolist()
        reflections_1farther = df[df.columns[i+1]].tolist()
        reflections_2farther = df[df.columns[i+2]].tolist()
        # reflections_2back = cutPersonInterfering(reflections_2back)
        # reflections_1back = cutPersonInterfering(reflections_1back)
        # reflections_1farther = cutPersonInterfering(reflections_1farther)
        # reflections_2farther = cutPersonInterfering(reflections_2farther)
        ref2b_baseline = average(reflections_2back[:300])
        ref1b_baseline = average(reflections_1back[:300])
        ref1f_baseline = average(reflections_1farther[:300])
        ref2f_baseline = average(reflections_2farther[:300])

# NOTE: the check for top is reducing fire samples

        if is_fire_flag:
            # if (flag and max(reflections) > min(top)) or (scenario == leave_one_out): #actually a fire bin
            if flag or (scenario == leave_one_out):
                sig_objects.append(SigObject(direction=90, distance=i, reflection=reflections, scenario=scenario, stationary=True, significant=True))
            else:
                if scenario == leave_one_out:
                    sig_objects.append(SigObject(direction=90, distance=i, reflection=reflections, scenario=scenario, stationary=False))
                else:
                    if (i in significant_bins or i+2 in significant_bins or i-2 in significant_bins or i-1 in significant_bins or i+1 in significant_bins) and i not in created_object_dists:
                        # if i in fire_bins[scenario]:
                        if i in fire_bins[scenario] or ref_baseline > min(top):
                            sig_objects.append(SigObject(direction=90, distance=i, reflection=reflections, scenario=scenario, stationary=True, significant=True))
                        else:
                            sig_objects.append(SigObject(direction=90, distance=i, reflection=reflections, scenario=scenario, stationary=False, significant=True))
                        created_object_dists.append(i)


                    elif (i in fire_bins[scenario] or i+2 in fire_bins[scenario] or i-2 in fire_bins[scenario] or i-1 in fire_bins[scenario] or i+1 in fire_bins[scenario]) and i not in created_object_dists:
                        sig_objects.append(SigObject(direction=90, distance=i, reflection=reflections, scenario=scenario, stationary=False, significant=True))
                    elif (ref_baseline > min(top) or ref2b_baseline > min(top) or ref1b_baseline > min(top) or ref1f_baseline > min(top) or ref2f_baseline > min(top)) and i not in created_object_dists: #same as above, but just for wall being fire
                        sig_objects.append(SigObject(direction=90, distance=i, reflection=reflections, scenario=scenario, stationary=False, significant=True))
                    else:
                        sig_objects.append(SigObject(direction=90, distance=i, reflection=reflections, scenario=scenario, stationary=False, significant=False))
        else:
            if (max(reflections) > min(top)) or (scenario == leave_one_out):
            # if True or (scenario == leave_one_out): #testing all distances
                reflection_arr = [reflections_2back, reflections_1back, reflections, reflections_1farther, reflections_2farther]
                idx = 0
                for d in range(i-2, i+3):
                    if d not in created_object_dists:
                        sig_objects.append(SigObject(direction=90, distance=d, reflection=reflection_arr[idx], scenario=scenario, stationary=False, significant=True))
                        created_object_dists.append(d)
                    idx+=1
            else:
                sig_objects.append(SigObject(direction=90, distance=i, reflection=reflections, scenario=scenario, stationary=False, significant=False))
                created_object_dists.append(i)

    findFireStart(sig_objects)
    try:
        return Experiment(scenario, sig_objects, top=top, fire_bin_list=fire_bins[scenario])
    except:
        return Experiment(scenario, sig_objects, top=top)
    # return sig_objects

def cutPersonInterfering(reflections):
    reflections1 = reflections[:600]
    reflections2 = reflections[700:]
    return reflections1 + reflections2

def getScenarioObjects(obj, objects):
    eligible_objects = []
    for j in objects:
        if (j.id != obj.id) and (j.scenario == obj.scenario) and (j.distance < obj.distance):
            eligible_objects.append(j)
    return eligible_objects

def calibrateObjects(objects):
    for i, obj in enumerate(objects): #grabs an object
        eligible_objects = getScenarioObjects(obj, objects)
        for x, val in enumerate(obj.reflection): #goes through every time
            for j in eligible_objects: #loops through other objects
                if j.reflection[x] > (val * 1.25):          #NOTE: this was changed to multiply by a scalar
                    if (obj.distance - j.distance) <= 5:
                        obj.close_occlusions[x]+=1
                    else:
                        obj.far_occlusions[x]+=1

def getMaxVarience(obj):
    start_time = 0
    end_time = start_time + 300
    step = 10
    last_varience_max = 0

    while end_time < obj.len_reflection:
        col_list = np.array(obj.reflection[start_time:end_time])
        time_col_list = np.linspace(0, len(col_list), len(col_list))
        p = np.poly1d(np.polyfit(time_col_list, col_list, 8))
        mean = p(time_col_list)

        variance_total = 0
        cur_max = 0
        for count, f in enumerate(col_list):
            cur = (abs(f - mean[count]) / f)
            if cur > cur_max:
                cur_max = cur

        if start_time != 0 and cur_max < (last_varience_max/3): # CHECK THIS
            return start_time+20

        last_varience_max = cur_max
        start_time+=step
        end_time+=step

    split_scen = obj.scenario.split('_')
    if obj.fire:
        if 'soot1' in split_scen or 'nonsoot' in split_scen or 'soot2' in split_scen:
            return 620
        else:
            return 1200 #this could be wrong
    return 10000000000

def findFireStart(objects):
    fire_starts = {}
    for o in objects:
        if o.scenario not in fire_starts.keys():
            fire_starts[o.scenario] = []

    for o in objects:
        if o.distance < 20:
            continue
        if not o.fire_flag and o.fire:
            o.fire_flag = True
            fire_starts[o.scenario].append(getMaxVarience(o))

    for o in objects:
        split_scen = o.scenario.split('_')
        if o.fire:
            if int(min(fire_starts[o.scenario])) > 600:
                o.fire_start = int(min(fire_starts[o.scenario]))
            else:
                o.fire_start = 600
        else:
            o.fire_start = 10000000000
