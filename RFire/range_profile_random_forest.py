

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
from os import listdir
import process_dat
import autoencoder


#Import scikit-learn dataset library
from sklearn import datasets
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import decomposition
from sklearn import datasets
from sklearn import metrics

from keras.models import Model

import time

DEPTH = 15
tree_number = 50

class FinalResult(object):

    def __init__(self, fire_distance=257, latency=10000, false_alarms=[], TP=0, TN=0, FP=0, FN=0):
        self.fire_distance = fire_distance
        self.latency = latency
        self.false_alarms = false_alarms
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "FinalResult(distance: {}, latency: {}, false alarms: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.fire_distance, self.latency, self.false_alarms, self.TP, self.TN, self.FP, self.FN)

class Result(object):

    def __init__(self, distance=0, fire=False, latency=10000, false_alarms=[], predicted_fire_time=10000, TP=0, TN=0, FP=0, FN=0):
        self.distance = distance
        self.fire = fire
        self.latency = latency
        self.false_alarms = false_alarms
        self.predicted_fire_time = predicted_fire_time
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "Result(distance: {}, latency: {}, false alarms: {}, predicted_fire_time: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.distance, self.latency, self.false_alarms, self.predicted_fire_time, self.TP, self.TN, self.FP, self.FN)

class Alarm(object):

    def __init__(self, distance=0, time=10000, fire=False, latency=10000):
        self.distance = distance
        self.time = time
        self.fire = fire
        self.latency = latency

    def __repr__(self):
        if self.fire:
            return "Alarm(distance: {}, time: {}, fire: {}, latency: {})".format(self.distance, self.time, self.fire, self.latency)
        else:
            return "Alarm(distance: {}, time: {}, fire: {})".format(self.distance, self.time, self.fire)

class RealAlarm(object):

    def __init__(self, distance=0, time=10000, fire=False, latency=10000):
        self.distance = distance
        self.time = time
        self.fire = fire
        self.latency = latency

    def __repr__(self):
        if self.fire:
            return "RealAlarm(distance: {}, time: {}, fire: {}, latency: {})".format(self.distance, self.time, self.fire, self.latency)
        else:
            return "RealAlarm(distance: {}, time: {}, fire: {})".format(self.distance, self.time, self.fire)




def PCA_3D(X):
    samples = []
    for x in X:
        pca = decomposition.PCA(n_components=5)
        pca.fit(x)
        x = pca.transform(x)
        flat = x.flatten()
        samples.append(x.flatten())
    return np.array(samples)

def flattenData(X):
    oneDimSamples = []
    for i in range(X.shape[0]):
        window = []
        for j in range(X[i].shape[0] - 1):
            for a in range(X[i][j].shape[0] - 1):
                window.append(X[i][j][a])
            normal_reflection = np.array(X[i][j][-1:])
        window = np.array(window)
        window = np.concatenate((window, normal_reflection), axis=0)
        oneDimSamples.append(window)
    return np.array(oneDimSamples)

def findIfFire(y_ground):
    for i in y_ground:
        if i == 1:
            return True
    return False

def lessThanIgnition(false_alarms, ignition):
    real_false_alarms = []
    for i in false_alarms:
        if i < ignition:
            real_false_alarms.append(i)
    return real_false_alarms

def alternativeTimeSeriesTest(y_test, y_pred, leave_one_out, min_ignition_sample, all_skipped_samples):
    results = {}
    closest_distance = 0
    alarms = []

    if min_ignition_sample < 10000:
        min_ignition_sample = min_ignition_sample - 30


    print("Length of y_test: ", len(y_test))
    print("Length of y_pred: ", len(y_pred))

    for idx, y_ground in enumerate(y_test):
        is_fire = findIfFire(y_ground)
        correct = 0
        incorrect = 0
        FN = 0
        FP = 0
        TP = 0
        TN = 0
        FN_before_fire = 0
        fire_flag = False
        early_classification = 0
        false_alarms = []
        real_false_alarm = 0
        ones_in_a_row = 0
        latency = 10000
        failed_detection = []
        predicted_fire_time = 10000
        pred_made = 0

        if y_pred[idx].size < 2:
            continue

        # print("Skipped: ", all_skipped_samples[idx])

        i = 0
        timestep = -1
        # for i, val in enumerate(y_ground): #actual values
        while pred_made < y_ground.shape[0]:
            val = y_ground[i]
            timestep+=1

            if timestep in all_skipped_samples[idx]:
                ones_in_a_row = 0
                continue

            pred_made+=1
            print(timestep, ' i: ', i, ' prediction: ', y_pred[idx][i], ' ground: ', val)

            if y_pred[idx][i] == 1: #high confidence of fire
                ones_in_a_row += 1

                if ones_in_a_row > 2:
                    predicted_fire_time = timestep #i
                    ones_in_a_row = 0
                    try:
                        if val == 1:
                            fire_flag = True
                            latency = (timestep - min_ignition_sample)
                            print("==== KNOWN FIRE BIN ====")
                            print("distance: ", idx + 4)
                            print("time: ", timestep)
                            # print("ignition_sample: ", min_ignition_sample)
                            print("Latency: ", latency, " second(s)")
                            alarms.append(Alarm(idx + 4, timestep, True, latency))
                        else: #fire detected but not a ground truth classified fire sample yet
                            if timestep < min_ignition_sample:
                                print("==== False Alarm at time ", timestep, " ====")
                                alarms.append(Alarm(idx + 4, timestep, False, 10000))
                                false_alarms.append(timestep)
                            else:
                                fire_flag = True
                                latency = (timestep - min_ignition_sample)
                                print("==== PREDICTED FIRE ====")
                                print("distance: ", idx + 4)
                                print("time: ", timestep)
                                # print("ignition_sample: ", min_ignition_sample)
                                print("Latency: ", latency, " second(s)")
                                alarms.append(Alarm(idx + 4, timestep, True, latency))
                    except:
                        print("---there was an error here---")
                        pass
                if val == 1: #actual fire
                    correct += 1
                    TP += 1
                else: # actual no fire
                    incorrect += 1
                    FP += 1
            else: #not predicted fire
                ones_in_a_row = 0
                if val == 0: #actual no fire
                    correct += 1
                    TN += 1
                else: #actual fire
                    incorrect += 1
                    FN += 1
                    # failed_detection.append(i)
                    if not fire_flag:
                        FN_before_fire += 1
            i+=1

        print("Distance: ", idx + 4)
        print("False Alarms: ", false_alarms)

        results[idx] = Result(distance=(idx + 4), fire=is_fire, latency=latency, false_alarms=false_alarms, predicted_fire_time=predicted_fire_time, TP=TP, TN=TN, FP=FP, FN=FN)

    min_fire_distance = 257
    min_latency = 10000
    alarm_count = {}
    for i in range(256):
        alarm_count[i] = []

    for i, a in enumerate(alarms):
        print(a)
        alarm_count[a.distance].append(a.time)
        if a.distance < min_fire_distance and a.time >= min_ignition_sample:
            min_fire_distance = a.distance
        if a.latency < min_latency and a.time >= min_ignition_sample:
            min_latency = a.latency


    # latencies = []
    false_alarm_list = []

    for idx, r in enumerate(results.keys()):
        false_alarm_list = false_alarm_list + results[r].false_alarms

    for r in results.keys():
        if results[r].fire:
            closest_known_fire = results[r]
            break

    my_set = set(false_alarm_list)
    unique_false_alarms = list(my_set)

    unique_false_alarms = lessThanIgnition(unique_false_alarms, min_ignition_sample)

    print("Fire starts at: ", min_ignition_sample)
    print("Fire detected at bin: ", min_fire_distance)
    print("Distance (meters):    ", min_fire_distance*.044)
    print("Fire latency:         ", min_latency)
    print("Unique false alarms:  ", unique_false_alarms)
    try:
        print("Actual Closest Fire:    ", closest_known_fire)
        print("Closest predicted fire: ", min_fire_distance)
    except:
        print("=== No fire detected ===")

    real_fire_alarms = []
    # print("alarm count: ", alarm_count)
    for dist in alarm_count.keys():
        fire = False
        if dist < 2 or dist > 200:
            continue
        cur_dist = alarm_count[dist]
        closer_list = alarm_count[dist-1]
        further_list = alarm_count[dist+1]
        for time in cur_dist:
            # print("cur_dist: ", cur_dist)
            # print("closer_list: ", closer_list)
            closer = [abs(x - time) for x in closer_list]
            # closer = map(lambda x: (abs(x - time)), closer_list)
            # print('closer: ', closer)
            # print("Further list: ", further_list)
            further = [abs(y - time) for y in further_list]
            # further = map(lambda y: (abs(y - time)), further_list)
            # print("further", further)
            total_list = closer + further
            # print('total_list: ', total_list)
            count = len([i for i in total_list if i < 3])
            # closer_count = len([i for i in closer if i < 3])
            # further_count = len([i for i in further if i < 3])
            # print("count: ", count)
            if count > 0:
            # if closer_count > 0 and further_count > 0:
            # if (0 in closer and 0 in further) or (1 in closer and 1 in further) or (1 in closer and 0 in further) or (0 in closer and 1 in further)or (2 in closer and 0 in further) or (0 in closer and 2 in further) or (2 in closer and 1 in further) or (1 in closer and 2 in further):
                if time >= min_ignition_sample:
                    fire = True
                    latency = time - min_ignition_sample
                    # print("fire: ", fire, " latency: ", latency)
                else:
                    fire = False
                    latency = 10000
                real_fire_alarms.append(RealAlarm(dist, time, fire, latency))

    final_result = FinalResult(min_fire_distance, min_latency, unique_false_alarms)
    return final_result, real_fire_alarms



    # results = []
    # for idx, y_ground in enumerate(y_test):
    #     correct = 0
    #     incorrect = 0
    #     FN = 0
    #     FP = 0
    #     TP = 0
    #     TN = 0
    #     FN_before_fire = 0
    #     fire_flag = False
    #     early_classification = 0
    #     false_alarms = []
    #     real_false_alarm = 0
    #     ones_in_a_row = 0
    #     latency = 10000000
    #
    #     # print('ground: ', y_ground)
    #     # print('pred: ', y_pred[idx])
    #     # exit()
    #
    #
    #     for i, val in enumerate(y_ground): #actual values
    #         # print(i, ' prediction: ', y_pred[idx][i], ' ground: ', val)
    #         if y_pred[idx][i] == 0: #predicted no fire
    #             ones_in_a_row = 0
    #             if val == 0: #actual no fire
    #                 correct += 1
    #                 TN += 1
    #             else: #actual fire
    #                 incorrect += 1
    #                 FN += 1
    #                 if not fire_flag:
    #                     FN_before_fire += 1
    #         else: # predicted fire
    #             ones_in_a_row += 1
    #             if not fire_flag: #if it is a leave_one_out test and a fire hasn't been detected
    #                 # if ones_in_a_row > 1:
    #                 try:
    #                     if val == 1 or y_ground[i+5] == 1: #if it truly is a fire
    #                         fire_flag = True
    #                         # print(" \U0001f525 \U0001f525 \U0001f525 Detected fire at time ", i, " \U0001f525 \U0001f525 \U0001f525")
    #                         latency = (i - ignition_sample[idx]) + 5
    #                         print("distance: ", i)
    #                         print("ignition_sample: ", ignition_sample[idx])
    #                         print("Latency: ", latency, " second(s)")
    #                 except:
    #                     pass
    #                 else: #fire detected but not a ground truth classified fire sample yet
    #                     false_alarms.append(i)
    #
    #             if val == 1: #actual fire
    #                 correct += 1
    #                 TP += 1
    #             else: # actual no fire
    #                 incorrect += 1
    #                 FP += 1
    #
    #     # print("Correct: ", correct)
    #     # print("incorrect: ", incorrect)
    #     # print("correct perc: ", correct/len(y_test))
    #     # print("TP: ", TP)
    #     # print("TN: ", TN)
    #     # print("FP (annoying) - no fire but predicted fire:     ", FP)
    #     # print("FN (bad) - actual fire but not predicted fire:  ", FN)
    #     # # print("Predicted no fire before time when fire starts: ", FN_before_fire)
    #     # print('Early Classification: ', early_classification)
    #     # print('False Alarms: ', false_alarm)
    #     # print('REAL False Alarms: ', real_false_alarm)
    #     results.append(Result(latency, false_alarms, TP, TN, FP, FN))
    #     # results.append(np.array([latency, TP, TN, false_alarms, FN]))
    #
    # latencies = []
    # false_alarm_list = []
    #
    # for r in results:
    #     latencies.append(r.latency)
    #     false_alarm_list = false_alarm_list + r.false_alarms
    #
    # my_set = set(false_alarm_list)
    # unique_false_alarms = list(my_set)
    #
    # print("Unique false alarms: ", unique_false_alarms)
    #
    # final_result = FinalResult(min(latencies), unique_false_alarms)
    # return final_result

def timeSeriesTest(y_test, y_pred, leave_one_out, ignition_sample, all_skipped_samples):
    if type(y_test) == list:
        return alternativeTimeSeriesTest(y_test, y_pred, leave_one_out, ignition_sample, all_skipped_samples)

    correct = 0
    incorrect = 0
    FN = 0
    FP = 0
    TP = 0
    TN = 0
    FN_before_fire = 0
    fire_flag = False
    early_classification = 0
    false_alarm = 0
    real_false_alarm = 0
    ones_in_a_row = 0
    latency = 60


    for i, val in enumerate(y_test):
        if y_pred[i] == 0: #predicted no fire
            ones_in_a_row = 0
            if val == 0: #actual no fire
                correct += 1
                TN += 1
            else: #actual fire
                incorrect += 1
                FN += 1
                if not fire_flag:
                    FN_before_fire += 1
        else: # predicted fire
            ones_in_a_row += 1
            if leave_one_out is not None:
                if not fire_flag: #if it is a leave_one_out test and a fire hasn't been detected
                    if val == 1 and ones_in_a_row == 2: #if it truly is a fire
                        fire_flag = True
                        # print(" \U0001f525 \U0001f525 \U0001f525 Detected fire at time ", i, " \U0001f525 \U0001f525 \U0001f525")
                        latency = i - ignition_sample
                        # print("Latency: ", latency, " second(s)")
                    else: #fire detected but not a ground truth classified fire sample yet
                        future = y_test[i:i+5]

                        if ones_in_a_row == 2:
                            real_false_alarm+=1

                        #checking if a fire is close due to the labeling format
                        if 1 not in future:
                            # print("\U0000274C \U0000274C \U0000274C FALSE ALARM at time ", i, " \U0000274C \U0000274C \U0000274C")
                            false_alarm += 1
                        else:
                            early_classification += 1

            if val == 1: #actual fire
                correct += 1
                TP += 1
            else: # actual no fire
                incorrect += 1
                FP += 1

    # print("Correct: ", correct)
    # print("incorrect: ", incorrect)
    # print("correct perc: ", correct/len(y_test))
    # print("TP: ", TP)
    # print("TN: ", TN)
    # print("FP (annoying) - no fire but predicted fire:     ", FP)
    # print("FN (bad) - actual fire but not predicted fire:  ", FN)
    # # print("Predicted no fire before time when fire starts: ", FN_before_fire)
    # print('Early Classification: ', early_classification)
    # print('False Alarms: ', false_alarm)
    # print('REAL False Alarms: ', real_false_alarm)

    if leave_one_out is not None:
        results = [latency, TP, TN, real_false_alarm, FN]
    else:
        results = [latency, TP, TN, FP, FN]

    return results

def random_forest(X_train, X_test, y_train, y_test, mode):

    clf=RandomForestClassifier(n_estimators=tree_number, criterion=mode, max_depth=DEPTH)

    #Train the model using the training sets y_pred=clf.predict(X_test)
    clf.fit(X_train,y_train)

    if type(X_test) == list:
        y_pred = []
        for x in X_test:
            y_pred.append(clf.predict(x))
    else:
        y_pred=clf.predict(X_test)
    return y_pred


def write_df_to_csv(write_df, leave_one_out):
    if leave_one_out is not None:
        dir = 'RRF_rangeprofile_results/'
    else:
        dir = 'RRF_rangeprofile_sample_wise_results/'
    save_str = 'RRF_10_times_depth_' + str(DEPTH) + '_trees_' + str(tree_number)
    today_string = time.strftime("%Y%m%d-%H%M%S")
    filename =  dir + today_string + save_str + '.csv'
    write_df.to_csv(filename, index=None, header=True)


def getIgnitionSample(leave_one_out, y_test):
    if type(y_test) == list:
        print("LEAVE ONE OUT: ", leave_one_out)
        ignition_samples = []
        for i, val in enumerate(y_test):
            for j, value in enumerate(val):
                if value == 1:
                    # print('Ignition sample: ', j)
                    ignition_samples.append(j)
                    break
        return ignition_samples
    else:
        return 0

def getMode():
    if len(sys.argv) == 2:
        # python3 range_profile_random_forest.py autoencode
        if sys.argv[1] == 'autoencode':
            print('===== Autoencode Sample-Wise =====')
            PCA = False
            AUTOENCODE = True
            leave_one_out = None
        # python3 range_profile_random_forest.py PCA
        elif sys.argv[1] == 'PCA':
            print('===== PCA Sample-Wise =====')
            PCA = True
            AUTOENCODE = False
            leave_one_out = None
        # python3 range_profile_random_forest.py [fire to leave out]
        else:
            print('===== Regular Leave-One-Out =====')
            PCA = False
            AUTOENCODE = False
            leave_one_out = sys.argv[1]
    elif len(sys.argv) == 3:
        # python3 range_profile_random_forest.py [fire to leave out] autoencode
        if sys.argv[2] == 'autoencode':
            print('===== Autoencode Leave-One-Out =====')
            PCA = False
            AUTOENCODE = True
            leave_one_out = sys.argv[1]
        # python3 range_profile_random_forest.py [fire to leave out] PCA
        elif sys.argv[2] == 'PCA':
            print('===== PCA Leave-One-Out =====')
            PCA = True
            AUTOENCODE = False
            leave_one_out = sys.argv[1]
    # python3 range_profile_random_forest.py
    else:
        print('===== Regular Sample-Wise =====')
        PCA = False
        AUTOENCODE = False
        leave_one_out = None
    return leave_one_out, AUTOENCODE, PCA

def flattenXData(leave_one_out, AUTOENCODE, PCA, X, X_test):
    if leave_one_out is None:
        print("Leave one out is None")
        if AUTOENCODE:
            X_train, X_test = autoencoder.train_autoencoder(X, X_test)
        elif PCA:
            X_train = PCA_3D(X)
            X_test = PCA_3D(X_test)
        else:
            X_train = flattenData(X)
            X_test = flattenData(X_test)
        return X_train, X_test
    else:
        print("Leave one out is a list")
        if AUTOENCODE:
            X_train, _ = autoencoder.train_autoencoder(X, np.array([]))
            X_test_arr = []
            for x in X_test:
                _, X_test_element = autoencoder.train_autoencoder(np.array([]), x)
                X_test_arr.append(X_test_element)
        elif PCA:
            X_train = PCA_3D(X)
            X_test_arr = []
            for x in X_test:
                X_test_arr.append(PCA_3D(x))
        else:
            X_train = flattenData(X)
            X_test_arr = []
            for x in X_test:
                X_test_arr.append(flattenData(x))
        return X_train, X_test_arr


# Driver code
def main(repeats=1):
    leave_one_out, AUTOENCODE, PCA = getMode()
    full_TP_gini = 0
    full_TN_gini = 0
    full_FP_gini = 0
    full_FN_gini = 0
    # total_latency_gini = 0
    # total_false_alarms_gini = 0

    full_TP_entropy = 0
    full_TN_entropy = 0
    full_FP_entropy = 0
    full_FN_entropy = 0
    # total_latency_ent = 0
    # total_false_alarms_ent = 0

    for j in range(repeats):
        X, y_train, X_train_flat, X_test, y_test, X_test_flat, leave_one_out_fires, all_skipped_samples, ignition_sample = process_dat.getRangeProfileSamples(cnn=False, leave_one_out=leave_one_out,  flat=False) #preprocess.loadDataset(leave_one_out)
        print(X.shape)
        print(y_train.shape)
        if type(X_test) == list:
            print("X test shape[0]: ", X_test[0].shape)
            print("y test shape: ",y_test[0].shape)
        else:
            print("X test shape[0]: ", X_test.shape)
            print("y test shape: ",y_test.shape)


        # ignition_sample = getIgnitionSample(leave_one_out, y_test)
        # X_train, X_test = flattenXData(leave_one_out, AUTOENCODE, PCA, X, X_test)
        X_train = X[:,:,8]


        cols = []
        result = ["result"]
        for i in range(0, len(X_train[0])):
            cols.append("col" + str(i))

        # if type(X_test) == list:
        #     X_test_df_lst = []
        #     y_test_df_lst = []
        #     for idx, x in enumerate(X_test):
        #         X_test_df = pd.DataFrame(columns=cols)
        #         y_test_df = pd.DataFrame(columns=result)
        #         for i in range(x.shape[0]):
        #             X_test_df.loc[i] = x[i]
        #             y_test_df.loc[i] = y_test[idx][i]
        #         y_test_df=y_test_df.astype('int')
        #         X_test_df_lst.append(X_test_df)
        #         y_test_df_lst.append(y_test_df)
        #     X_test_df = X_test_df_lst
        #     y_test_df = y_test_df_lst
        #     print("made dataframes")
        # else:
        #     X_test_df = pd.DataFrame(columns=cols)
        #     y_test_df = pd.DataFrame(columns=result)
        #     for i in range(X_test.shape[0]):
        #         X_test_df.loc[i] = X_test[i]
        #         y_test_df.loc[i] = y_test[i]
        #     y_test_df=y_test_df.astype('int')

        if type(X_test) == list:
            X_test_df_lst = []
            y_test_df_lst = []
            for idx, x in enumerate(X_test):

                # for i in range(x.shape[0]):
                a = x[:,:,8]

                X_test_df = pd.DataFrame(a, columns=cols)
                y_test_df = pd.DataFrame(y_test[idx], columns=result)
                y_test_df = y_test_df.astype('int')
                X_test_df_lst.append(X_test_df)
                y_test_df_lst.append(y_test_df)
            X_test_df = X_test_df_lst
            y_test_df = y_test_df_lst
            print("made dataframes")
        else:
            X_test_df = pd.DataFrame(X_test, columns=cols)
            y_test_df = pd.DataFrame(y_test, columns=result)
            # for i in range(X_test.shape[0]):
            #     X_test_df.loc[i] = X_test[i]
            #     y_test_df.loc[i] = y_test[i]
            y_test_df=y_test_df.astype('int')

        X_train_df = pd.DataFrame(columns=cols)
        y_train_df = pd.DataFrame(columns=result)

        # for i in range(X_train.shape[0]):
        #     X_train_df.loc[i] = X_train[i]
        X_train_df = pd.DataFrame(X_train, columns=cols)
        y_train_df = pd.DataFrame(y_train, columns=result)

        # print(X_train_df)
        print('made train dataframes')
        # exit()
        # for i in range(y_train.shape[0]):
        #     y_train_df.loc[i] = y_train[i]
        # print('made y train dataframe')

        y_train_df=y_train_df.astype('int')
        # y_test_df=y_test_df.astype('int')

        gini_results_arr = [tree_number, DEPTH]
        entropy_results_arr = [tree_number, DEPTH]

        # print("X_train: ")
        # print(X_train_df)
        # print("Y")
        # print(y_train_df)

        print("GINI:")
        y_pred_gini = random_forest(X_train_df, X_test_df, y_train_df, y_test_df, 'gini')
        final_result_gini, real_fire_alarms_gini = timeSeriesTest(y_test, y_pred_gini, leave_one_out, ignition_sample, all_skipped_samples)
        # full_gini_results = np.array([gini_results_arr + return_arr])

        if type(X_test) == list:
            # total_latency_gini+=gini_results.latency
            total_false_alarms_gini = 0
            # total_distance+=final_result_gini.fire_distance
            total_latency_gini = 100000
            false_alarm_times_gini = []
            for r in real_fire_alarms_gini:
                print(r)
                if not r.fire:
                    if r.time not in false_alarm_times_gini:
                        false_alarm_times_gini.append(r.time)
                        total_false_alarms_gini+=1
                if r.latency < total_latency_gini:
                    total_latency_gini = r.latency
        else:
            full_TP_gini+=gini_results.TP
            full_TN_gini+=gini_results.TN
            full_FN_gini+=gini_results.FN
            full_FP_gini+=gini_results.FP


        print('entropy')
        y_pred_entropy = random_forest(X_train_df, X_test_df, y_train_df, y_test_df, 'entropy')
        final_result_ent, real_fire_alarms_ent = timeSeriesTest(y_test, y_pred_entropy, leave_one_out, ignition_sample, all_skipped_samples)
        # full_entropy_results = np.array([entropy_results_arr + return_arr])

        if type(X_test) == list:
            # total_latency_entropy+=entropy_results.latency
            total_false_alarms_ent = 0
            # total_distance+=final_result_gini.fire_distance
            total_latency_ent = 100000
            false_alarm_times_ent = []
            for r in real_fire_alarms_ent:
                print(r)
                if not r.fire:
                    if r.time not in false_alarm_times_ent:
                        false_alarm_times_ent.append(r.time)
                        total_false_alarms_ent+=1
                if r.latency < total_latency_ent:
                    total_latency_ent = r.latency
        else:
            full_TP_entropy+=entropy_results.TP
            full_TN_entropy+=entropy_results.TN
            full_FN_entropy+=entropy_results.FN
            full_FP_entropy+=entropy_results.FP

    if type(X_test) == list:
        # print("Average Bin:               ", total_distance/repeats)
        # print("Average Distance (meters): ", (total_distance*(0.044))/repeats)
        # print("Known fire bins:           ", leave_one_out_fires)
        # # if (total_latency/repeats) > 5000:
        # #     print('==== NO ALARM ====')
        # # else:
        # print("Average Latency:           ", total_latency/repeats)
        # print("Average False Alarms:      ", total_false_alarms/repeats)
        print("Known fire bins:           ", leave_one_out_fires)
        print("Average Latency Gini:         ", total_latency_gini/repeats)
        print("Average False Alarms Gini:    ", total_false_alarms_gini/repeats)
        print()
        print("Average Latency Entropy:      ", total_latency_ent/repeats)
        print("Average False Alarms Entropy: ", total_false_alarms_ent/repeats)

    else:
        print("Full TP gini: ", full_TP_gini/repeats)
        print("Full TN gini: ", full_TN_gini/repeats)
        print("Full FP gini: ", full_FP_gini/repeats)
        print("Full FN gini: ", full_TP_gini/repeats)
        print()
        print("Full TP Entropy: ", full_TP_entropy/repeats)
        print("Full TN Entropy: ", full_TN_entropy/repeats)
        print("Full FP Entropy: ", full_FP_entropy/repeats)
        print("Full FN Entropy: ", full_TP_entropy/repeats)
    print("Finished")

        # if j == 0:
        #     np_gini_results = np.array(full_gini_results)
        #     np_entropy_results = np.array(full_entropy_results)
        #     print("Gini:    ", full_gini_results)
        #     print("Entropy: ", full_entropy_results)
        # else:
        #     print("Gini:    ", full_gini_results)
        #     print("Entropy: ", full_entropy_results)
        #     #find averages
        #     np_gini_results = np.add(np_gini_results, np.array(full_gini_results))
        #     np_entropy_results = np.add(np_entropy_results, np.array(full_entropy_results))
        #
        #     np_gini_results = np.divide(np_gini_results, 2)
        #     np_entropy_results = np.divide(np_entropy_results, 2)


    # if leave_one_out is not None:
    #     final_cols = ['tree_number', 'tree_depth', 'latency', 'TP', 'TN', 'false_alarm', 'failed_to_detect']
    # else:
    #     final_cols = ['tree_number', 'tree_depth', 'latency', 'TP', 'TN', 'wrongly_classified', 'failed_to_detect']
    #
    # gini_df = pd.DataFrame.from_records(np_gini_results, columns=final_cols)
    # entropy_df = pd.DataFrame.from_records(np_entropy_results, columns=final_cols)
    #
    # print(gini_df)
    # print(entropy_df)

    # write_df_to_csv(gini_df, leave_one_out)
    # write_df_to_csv(entropy_df, leave_one_out)


    print("RANDOM FOREST DEPTH: ", DEPTH)
    print("TREES: ", tree_number)


# Calling main function
if __name__=="__main__":
    main()
