# RFire
# 11/16/19
#
# New way to look at "bins" from the mmWave board.
# To run if one file: python3 viz_range_data.py [file]
# To run if a folder full of files: python3 viz_range_data.py [folder]

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
from os import listdir

#generates scatterplot or line plot of a single distance from the sensor, over all timesteps
def scatterplot(x, y, dist):
    # fig = plt.figure()
    # ax = fig.add_subplot(1,1,1,axisbg='1.0')
    # ax.scatter(x, y, alpha=0.7, edgecolors = 'none', s=30)
    plt.plot(x, y)
    plt.ylim(bottom=0, top=400000000000)
    plt.xlim(left=0)
    plt.title("Distance: " + str(dist), fontsize=18)
    plt.ylabel('Regression Power', fontsize=15)
    plt.xlabel('Timesteps', fontsize=15)
    plt.show()

#generates line plot for each timesetp, like the GUI on the mmwave site
def line_plot(x, y, time):
    plt.plot(x, y)
    plt.ylim(bottom=0, top=120000000000)
    plt.xlim(left=0)
    plt.title("Timestep: " + str(time), fontsize=18)
    plt.ylabel('Regression Power', fontsize=15)
    plt.xlabel('Distance', fontsize=15)
    plt.show()

#loads the data into pandas dataframe, deletes the last 5 lines to avoid anomalies
def loadData(fname=None, dirname=None):
    if fname is None:
        fname = sys.argv[len(sys.argv) - 1]
        path = fname + '/'+ fname + '.csv'
    else:
        path = dirname + fname + '/'+ fname + '.csv'
    data = pd.read_csv(path, index_col=False)
    return data.iloc[:-5]

#takes the columns of the CSVs. So there are 256 distances in a single direction
def parseByDistance(df):
    for i, col in enumerate(df.columns):
        if i < 40 and i > 25:
            col_list = df[col].tolist()
            # print(col_list[:20])
            time = list(range(len(col_list)))
            scatterplot(time, col_list, i)

#Plots each row of the CSV. This is like the GUI in the mmWave Demo, showing all distances for time t
def parseByTime(df):
    for idx, rows in df.iterrows():
        if idx%10 == 0:
            power_arr = rows.tolist()
            time = list(range(len(power_arr)))
            line_plot(time, power_arr, idx)


try:
    df = loadData()
except:
    path = (sys.argv[len(sys.argv) - 1]) + '/'
    flag = False
    for idx, f in enumerate(sorted(listdir(path))):
        if f[0] is not '_':
            if f[0] is not '.':
                returned_df = loadData(f, path)
                if flag == False:
                    flag = True
                    df = returned_df
                    continue

                df = pd.concat([df, returned_df])

    df = df.reset_index(drop=True)

parseByDistance(df)
# parseByTime(df)
