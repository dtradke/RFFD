# RFire
# 11/16/19
#
# 1DCNN model for the second RFire algorithm, using a CNN. Good results. Models have been trained already using temporal samples.


# cnn model
# import tensorflow as tf
import tensorflow as tf
from keras import backend as K
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
from matplotlib import pyplot
# from keras.models import Sequential, Model
# from keras.layers import Dense, GlobalAveragePooling1D, Bidirectional, TimeDistributed, LSTM, Concatenate, Reshape, Concatenate
# from keras.layers import Flatten, Input
# from keras.layers import Dropout
# from keras.layers.convolutional import Conv1D
# from keras.layers.convolutional import MaxPooling1D, UpSampling1D
# from keras.utils import to_categorical
# from keras.models import load_model
# from keras.optimizers import SGD
import numpy as np
from keras.models import *
from keras.layers import *
from keras.optimizers import *
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import optimizers
# from keras import backend as keras

from keras.layers import Lambda
from keras.layers import add

import keras.models
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Dropout, Flatten, Concatenate, Input
from keras.layers import BatchNormalization
from keras.optimizers import SGD, RMSprop
from keras.layers import Conv2D, MaxPooling2D, AveragePooling2D
from keras.optimizers import SGD
from keras.losses import mse, binary_crossentropy

from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
import time
import process_dat
import sys
import pickle


def make_keras_picklable():
    def __getstate__(self):
        model_str = ""
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            keras.models.save_model(self, fd.name, overwrite=True)
            model_str = fd.read()
        d = { 'model_str': model_str }
        return d

    def __setstate__(self, state):
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            fd.write(state['model_str'])
            fd.flush()
            model = keras.models.load_model(fd.name)
        self.__dict__ = model.__dict__


    cls = keras.models.Model
    cls.__getstate__ = __getstate__
    cls.__setstate__ = __setstate__



class FinalResult(object):

    def __init__(self, fire_distance=257, latency=10000, false_alarms=[], TP=0, TN=0, FP=0, FN=0):
        self.fire_distance = fire_distance
        self.latency = latency
        self.false_alarms = false_alarms
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "FinalResult(distance: {}, latency: {}, false alarms: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.fire_distance, self.latency, self.false_alarms, self.TP, self.TN, self.FP, self.FN)

class Result(object):

    def __init__(self, distance=0, fire=False, latency=10000, false_alarms=[], predicted_fire_time=10000, TP=0, TN=0, FP=0, FN=0):
        self.distance = distance
        self.fire = fire
        self.latency = latency
        self.false_alarms = false_alarms
        self.predicted_fire_time = predicted_fire_time
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "Result(distance: {}, latency: {}, false alarms: {}, predicted_fire_time: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.distance, self.latency, self.false_alarms, self.predicted_fire_time, self.TP, self.TN, self.FP, self.FN)

class Alarm(object):

    def __init__(self, distance=0, time=10000, fire=False, latency=10000):
        self.distance = distance
        self.time = time
        self.fire = fire
        self.latency = latency

    def __repr__(self):
        if self.fire:
            return "Alarm(distance: {}, time: {}, fire: {}, latency: {})".format(self.distance, self.time, self.fire, self.latency)
        else:
            return "Alarm(distance: {}, time: {}, fire: {})".format(self.distance, self.time, self.fire)

class RealAlarm(object):

    def __init__(self, distance=0, time=10000, fire=False, latency=10000):
        self.distance = distance
        self.time = time
        self.fire = fire
        self.latency = latency

    def __repr__(self):
        if self.fire:
            return "RealAlarm(distance: {}, time: {}, fire: {}, latency: {})".format(self.distance, self.time, self.fire, self.latency)
        else:
            return "RealAlarm(distance: {}, time: {}, fire: {})".format(self.distance, self.time, self.fire)


class HybridModel(Model):

    def __init__(self, trainX, trainy, X_fft, epochs=1, batch_size=32, weightsFileName=None):
        super(HybridModel, self).__init__()

        verbose, batch_size = 0, 32#500#32
        try:
            n_timesteps, n_features, n_outputs, fft_features = trainX.shape[1], 1, 1, X_fft.shape[1]
        except:
            n_timesteps, n_features, n_outputs, fft_features = 256, 1, 1, 299

        # input_size =    #masterDataSet.testX[0].shape

        inputs = Input((n_timesteps, n_features), name='Input')
        conv1 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
        conv1 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
        pool1 = MaxPooling1D(pool_size=2)(conv1)
        conv2 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
        conv2 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
        pool2 = MaxPooling1D(pool_size=2)(conv2)
        conv3 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
        conv3 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
        pool3 = MaxPooling1D(pool_size=2)(conv3)
        conv4 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
        conv4 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
        drop4 = Dropout(0.5)(conv4)
        pool4 = MaxPooling1D(pool_size=2)(drop4)

        conv5 = Conv1D(filters=1024, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
        conv5 = Conv1D(filters=1024, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
        drop5 = Dropout(0.5)(conv5)

        up6 = Conv1D(filters=512, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(drop5))
        merge6 = concatenate([drop4,up6], axis = 2)
        conv6 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
        conv6 = Conv1D(filters=512, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

        up7 = Conv1D(filters=256, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(conv6))
        merge7 = concatenate([conv3,up7], axis = 2)
        conv7 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
        conv7 = Conv1D(filters=256, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

        up8 = Conv1D(filters=128, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(conv7))
        merge8 = concatenate([conv2,up8], axis = 2)
        conv8 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
        conv8 = Conv1D(filters=128, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

        up9 = Conv1D(filters=64, kernel_size=2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling1D(size = 2)(conv8))
        merge9 = concatenate([conv1,up9], axis = 2)
        conv9 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
        conv9 = Conv1D(filters=64, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
        conv9 = Conv1D(filters=2, kernel_size=3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
        conv10 = Conv1D(filters=1, kernel_size=1, activation = 'sigmoid')(conv9)
        model = Model(input = inputs, output = conv10)

        # model = Model(input = inputs, output = conv10)
        super().__init__(input = inputs, output = conv10)

        # sgd = optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        # model.compile(optimizer = sgd, loss = 'binary_crossentropy', metrics = ['accuracy'])
        self.compile(optimizer = Adam(lr = 1e-4), loss = 'mse', metrics = ['accuracy'])

        print(self.summary())
        # self.t_ref = Input((n_timesteps,),name='Ref_Branch')
        # # self.t_fft = FFTBranch(fft_features)
        # self.t_fft = Input((fft_features,),name='FFT_Branch')
        # self.cnn_tm1 = CNNBranch(n_timesteps, n_features)
        # self.cnn_t = CNNBranch(n_timesteps, n_features)
        #
        # lstm_input = Concatenate(name='mergedBranches', axis=1)([self.cnn_tm1.output, self.cnn_t.output])
        # lstm_input = Reshape((2,64))(lstm_input)
        #
        # reflection = Dense(64, activation='relu')(self.t_ref)
        # reflection = Dense(128, activation='relu')(reflection)
        #
        # fc_dense = Dense(64, activation='relu')(self.t_fft)
        # # fc_dense = Dense(128, activation='relu')(fc_dense)
        # # fc_dense = Dense(256, activation='relu')(fc_dense)
        # fc_dense = Dense(128, activation='relu')(fc_dense)
        #
        # # fc_dense = Dense(128, activation='relu')(self.cnn_t.output)
        #
        # # lstm1 = LSTM(256, activation='relu', return_sequences=True)(lstm_input)
        # # lstm2 = LSTM(256, activation='relu', return_sequences=True)(lstm1)
        # lstm2 = LSTM(64, activation='relu')(lstm_input)
        #
        # ref_fft_concat = Concatenate(name='ref_fft_concat', axis=1)([reflection, fc_dense])
        # ref_fft_concat = Dense(128, activation='relu')(ref_fft_concat)
        #
        # to_output = Concatenate(name='to_output', axis=1)([ref_fft_concat, lstm2])
        # to_output = Dense(32, activation='relu')(to_output)
        # output_dropout = Dropout(0.5)(to_output)
        # out = Dense(n_outputs, activation='softmax')(output_dropout)

        # super().__init__([self.t_ref, self.t_fft, self.cnn_tm1.input, self.cnn_t.input], out) #RFIRE


        if weightsFileName is not None:
            fname = 'HYBRID_models/' + weightsFileName
            self.load_weights(fname)
            # model = pickle.load(open(fname, 'rb'))

    def fit(self, trainX, trainXsecond, trainy, X_fft, X_second_fft, epochs, batch_size=32):
        # trainy = np.expand_dims(np.expand_dims(np.argmax(trainy, axis=1), axis=1), axis=2)
        trainy = np.expand_dims(np.argmax(trainy, axis=1),axis=1)
        print(trainy.shape)
        trainy = np.expand_dims(np.repeat(trainy, 256, axis=1), axis=2)
        print(trainy.shape)

        val_thresh = int(trainXsecond.shape[0] * 0.9)
        # val_m1_fft = X_second_fft[val_thresh:]
        # val_m1 = trainXsecond[val_thresh:]
        # val_t_fft = X_fft[val_thresh:]
        # val_t = np.expand_dims(trainX[val_thresh:], axis=2)
        val_y = trainy[val_thresh:]
        # train_m1_fft = X_second_fft[:val_thresh]
        # train_m1 = trainXsecond[:val_thresh]
        # train_t_fft = X_fft[:val_thresh]
        # train_t = trainX[:val_thresh]
        train_y = trainy[:val_thresh]

        print("TRAIN Y SHAPE: ", trainy.shape)
        print("VAL Y Shape: ", val_y.shape)

        reflections_idx = 10

        # train_m1_fft = X_second_fft
        # train_m1 = trainXsecond
        # train_t_fft = X_fft
        train_t = trainX

        train_t_ref = train_t[:,:,reflections_idx]
        val_t = np.expand_dims(train_t_ref[val_thresh:], axis=2)
        print("VAL X: ", val_t.shape)
        train_t_ref = train_t_ref[:val_thresh]
        print("TRAIN X: ", train_t_ref.shape)

        # trainX = getNewInput(train_t)
        # trainX_m1 = getNewInput(train_m1)
        # train_m1_ref = trainXsecond[:,:,reflections_idx]

        # print("FFT SHAPE: ", train_t_fft.shape)
        tinputs = np.expand_dims(train_t_ref, axis=2)
        print("ins shape: ", tinputs.shape)
        # tinputs = [trainX_m1, trainX]
        toutputs = train_y
        es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=20)
        return super().fit(tinputs, toutputs, batch_size = batch_size, epochs=epochs, verbose=1, validation_data=(val_t, val_y), callbacks=[es])

    def predict(self, testX, testXsecond, testX_fft, testXsecond_fft):
        reflections_idx = 10
        testX_ref = testX[:,:,reflections_idx]
        second_test_ref = testXsecond[:,:,reflections_idx]

        # testX_ref = getNewInput(testX)
        # second_test_ref = getNewInput(testXsecond)


        inputs = np.expand_dims(testX_ref, axis=2) #RFIRE
        results = super().predict(inputs)
        # print("RESULTS: ", results.shape)
        results = np.median(results, axis=1)
        # print("RESULTS: ", results.shape)
        return results




# CNN LSTM hybrid
def train_model(trainX, trainXsecond, trainy, X_fft, X_second_fft, epochs=1, batch_size=500):
    verbose, batch_size = 0, 32#500#32
    n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]

    print('In training model')
    mod = HybridModel(trainX, trainy, X_fft, epochs, batch_size)
    mod.fit(trainX, trainXsecond, trainy, X_fft, X_second_fft, epochs, batch_size)
    return mod


# summarize scores
def summarize_results(scores):
	print(scores)
	m, s = mean(scores), std(scores)
	print('Accuracy: %.3f%% (+/-%.3f)' % (m, s))

def findIfFire(y_ground):
    for i in y_ground:
        if i[0] < i[1]:
            return True
    return False

def alternativeTimeSeriesTest(y_test, y_pred, leave_one_out, min_ignition_sample, all_skipped_samples):
    results = {}
    # min_ignition_sample = 10000
    closest_distance = 0
    alarms = []

# TAKE OUT WHEN CLASSIFICATION ISNT AT THE BEGINNING
    if min_ignition_sample < 10000:
        min_ignition_sample = min_ignition_sample - 30

    print("Length of y_test: ", len(y_test.keys()))
    print("Length of y_pred: ", len(y_pred.keys()))

    for idx, y_test_key in enumerate(y_test.keys()):
        y_ground = y_test[y_test_key]
        is_fire = findIfFire(y_ground)
        correct = 0
        incorrect = 0
        FN = 0
        FP = 0
        TP = 0
        TN = 0
        FN_before_fire = 0
        fire_flag = False
        early_classification = 0
        false_alarms = []
        real_false_alarm = 0
        ones_in_a_row = 0
        latency = 10000
        failed_detection = []
        predicted_fire_time = 10000
        pred_made = 0

        if y_pred[y_test_key].size < 2:
            continue

        # print("Skipped: ", all_skipped_samples[y_test_key])

        i = 0
        timestep = 0
        # for i, val in enumerate(y_ground): #actual values

        while pred_made < y_ground.shape[0]:
            val = y_ground[i]
            timestep+=1

            if timestep in all_skipped_samples[y_test_key]:
                ones_in_a_row = 0
                # print("Skipping timestep: ", timestep)
                continue

            pred_made+=1
            print(timestep, ' i: ', i, ' prediction: ', y_pred[y_test_key][i], ' ground: ', val)

            if y_pred[y_test_key][i] > 0.5: #high confidence of fire
                ones_in_a_row += 1
                if ones_in_a_row > 2:
                    predicted_fire_time = timestep+10 #i
                    ones_in_a_row = 0
                    try:
                        if np.argmax(val) == 1:
                            fire_flag = True
                            latency = (timestep - min_ignition_sample)
                            print("==== KNOWN FIRE BIN ====")
                            print("distance: ", y_test_key)
                            print("time: ", timestep)
                            # print("ignition_sample: ", min_ignition_sample)
                            print("Latency: ", latency, " second(s)")
                            alarms.append(Alarm(y_test_key, timestep, True, latency))
                        else: #fire detected but not a ground truth classified fire sample yet
                            if timestep < min_ignition_sample:
                                print("==== False Alarm at time ", timestep, " ====")
                                alarms.append(Alarm(y_test_key, timestep, False, 10000))
                                false_alarms.append(timestep)
                            else:
                                fire_flag = True
                                latency = (timestep - min_ignition_sample)
                                print("==== PREDICTED FIRE ====")
                                print("distance: ", y_test_key)
                                print("time: ", timestep)
                                # print("ignition_sample: ", min_ignition_sample)
                                print("Latency: ", latency, " second(s)")
                                alarms.append(Alarm(y_test_key, timestep, True, latency))
                    except:
                        print("---there was an error here---")
                        pass
                if np.argmax(val) == 1: #actual fire
                    correct += 1
                    TP += 1
                else: # actual no fire
                    incorrect += 1
                    FP += 1
            else: #not predicted fire
                ones_in_a_row = 0
                if np.argmax(val) == 0: #actual no fire
                    correct += 1
                    TN += 1
                else: #actual fire
                    incorrect += 1
                    FN += 1
                    # failed_detection.append(i)
                    if not fire_flag:
                        FN_before_fire += 1
            i+=1

        print("Distance: ", y_test_key)
        print("False Alarms: ", false_alarms)

        results[idx] = Result(distance=y_test_key, fire=is_fire, latency=latency, false_alarms=false_alarms, predicted_fire_time=predicted_fire_time, TP=TP, TN=TN, FP=FP, FN=FN)

    min_fire_distance = 257
    min_latency = 10000
    alarm_count = {}
    for i in range(256):
        alarm_count[i] = []

    for i, a in enumerate(alarms):
        print(a)
        alarm_count[a.distance].append(a.time)
        if a.distance < min_fire_distance and a.time >= min_ignition_sample:
            min_fire_distance = a.distance
        if a.latency < min_latency and a.time >= min_ignition_sample:
            min_latency = a.latency


    # latencies = []
    false_alarm_list = []

    for idx, r in enumerate(results.keys()):
        false_alarm_list = false_alarm_list + results[r].false_alarms


    for r in results.keys():
        if results[r].fire:
            closest_known_fire = results[r]
            break

    my_set = set(false_alarm_list)
    unique_false_alarms = list(my_set)

    unique_false_alarms = lessThanIgnition(unique_false_alarms, min_ignition_sample)

    print("Fire starts at: ", min_ignition_sample)
    print("Fire detected at bin: ", min_fire_distance)
    print("Distance (meters):    ", min_fire_distance*.044)
    print("Fire latency:         ", min_latency)
    print("Unique false alarms:  ", unique_false_alarms)


    real_fire_alarms = []
    for dist in alarm_count.keys():
        fire = False
        if dist < 2 or dist > 200:
            continue
        cur_dist = alarm_count[dist]
        closer_list = alarm_count[dist-1]
        further_list = alarm_count[dist+1]
        for time in cur_dist:
            # print("cur_dist: ", cur_dist)
            # print("closer_list: ", closer_list)
            closer = [abs(x - time) for x in closer_list]
            # closer = map(lambda x: (abs(x - time)), closer_list)
            # print('closer: ', closer)
            # print("Further list: ", further_list)
            further = [abs(y - time) for y in further_list]
            # further = map(lambda y: (abs(y - time)), further_list)
            # print("further", further)
            # total_list = closer + further
            # print('total_list: ', total_list)
            # count = len([i for i in total_list if i < 3])
            closer_count = len([i for i in closer if i < 4])
            further_count = len([i for i in further if i < 4])
            # print("count: ", count)
            # if count > 0:
            if closer_count > 0 and further_count > 0:
            # if (0 in closer and 0 in further) or (1 in closer and 1 in further) or (1 in closer and 0 in further) or (0 in closer and 1 in further)or (2 in closer and 0 in further) or (0 in closer and 2 in further) or (2 in closer and 1 in further) or (1 in closer and 2 in further):
                if time >= min_ignition_sample:
                    fire = True
                    latency = time - min_ignition_sample
                    # print("fire: ", fire, " latency: ", latency)
                else:
                    fire = False
                    latency = 10000
                real_fire_alarms.append(RealAlarm(dist, time, fire, latency))


    final_result = FinalResult(min_fire_distance, min_latency, unique_false_alarms)
    return final_result, real_fire_alarms

def lessThanIgnition(false_alarms, ignition):
    real_false_alarms = []
    for i in false_alarms:
        if i < ignition:
            real_false_alarms.append(i)
    return real_false_alarms


def timeSeriesTest(testY, y, leave_one_out, ignition_sample, all_skipped_samples):
    real_fire = 0
    real_nonfire = 0
    if type(testY) is dict:
        return alternativeTimeSeriesTest(testY, y, leave_one_out, ignition_sample, all_skipped_samples)

    correct = 0
    incorrect = 0
    FN = 0
    FP = 0
    TP = 0
    TN = 0
    FN_before_fire = 0
    early_classification = 0
    false_alarm = 0
    for i, val in enumerate(testY):
        if y[i][0] > y[i][1]: #predicted no fire
            if val[0] > val[1]: #actual no fire
                correct += 1
                TN += 1
                real_nonfire+=1
            else: #actual fire
                incorrect += 1
                FN += 1
                real_fire+=1
        else: # predicted fire
            if val[0] < val[1]: #actual fire
                correct += 1
                TP += 1
                real_fire+=1
            else: # actual no fire
                incorrect += 1
                FP += 1
                real_nonfire+=1

    print("TP: ", TP)
    print("TN: ", TN)
    print("FP (annoying) - no fire but predicted fire:     ", FP)
    print("FN (bad) - actual fire but not predicted fire:  ", FN)
    print("Amount of fire samples: ", real_fire)
    print("Amount of nonfire samples: ", real_nonfire)
    return Result(distance=0, fire=False, latency=0, false_alarms=[], TP=TP, TN=TN, FP=FP, FN=FN), []

def getIgnitionSample(leave_one_out, y_test):
    ignitions = []
    if leave_one_out is not None:
        print("LEAVE ONE OUT: ", leave_one_out)
        for i, val in enumerate(y_test):
            for j, value in enumerate(val):
                if value[0] < value[1]:
                    ignitions.append(j)
                    break
            else:
                ignitions.append(1000000)
        return ignitions
    else:
        return 0



def getModel(leave_one_out, model, X, X_train_second, y_train, X_fft, X_second_fft):
    if model is None:
        epochs = 300 #for RFIRE
        print("Training with: ", X.shape, " and ", X_train_second.shape)
        model = train_model(X, X_train_second, y_train, X_fft, X_second_fft, epochs=epochs)
        time_string = time.strftime("%Y%m%d-%H%M%S")
        fname = 'HYBRID_models/rfire-HYBRID_' + time_string + '_without-' + leave_one_out + '.h5'

        try:
            print("Saving: ", fname)
            model.save_weights(fname)
        except:
            print("Not saving model")
            pass
    else:

        # fname = 'HYBRID_models/' + model
        # model=load_model(fname)
        print("Loading: ", model)
        model = HybridModel(X, X_train_second, y_train, weightsFileName=model)
    return model


# run an experiment
def run_experiment(leave_one_out, model=None, repeats=1):
    full_TP = 0
    full_TN = 0
    full_FP = 0
    full_FN = 0
    total_latency = 0
    total_false_alarms = 0
    total_distance = 0
    for a in range(repeats):
        # model = None

        if model is None:
            X, y_train, X_train_second, X_fft, X_second_fft, X_test, y_test, X_test_second, leave_one_out_X_fft, second_loo_X_fft, leave_one_out_fires, all_skipped_samples, ignition_sample = process_dat.getRangeProfileSamples(cnn=True, leave_one_out=leave_one_out, hybrid=True)
        else:
            X, y_train, X_train_second, X_fft, X_second_fft, X_test, y_test, X_test_second, leave_one_out_X_fft, second_loo_X_fft, leave_one_out_fires, all_skipped_samples, ignition_sample = process_dat.getRangeProfileSamples(cnn=True, leave_one_out=leave_one_out, model=model, hybrid=True)

        print("X train: ", X.shape)
        print("second: ", X_train_second.shape)


        if type(X_test) is dict:
            print("Testing with: ", X_test[list(X_test.keys())[25]].shape)
            print("X second: ", X_test_second[list(X_test_second.keys())[25]].shape)
            # print(X_test[0])
        else:
            print("Testing with: ", X_test.shape)
            # print("X test flat: ", X_test_flat.shape)


        # ignition_sample = getIgnitionSample(leave_one_out, y_test)
        model = getModel(leave_one_out, model, X, X_train_second, y_train, X_fft, X_second_fft)

        version_1_nofires = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,109,124,135,136,148,149,150,151,152,153,154,155,156,157,159,160,161,194,195,196,197,198,199]

        y_preds = {}
        if type(X_test) is dict:
            for count, key in enumerate(X_test.keys()):
                x = X_test[key]
                if x.size > 2 and key not in version_1_nofires:
                    print("Predicting bin: ", key, " with shape: ", x.shape)
                    # x = np.expand_dims(x, axis=1)
                    y_p = model.predict(x, X_test_second[key], leave_one_out_X_fft[key], second_loo_X_fft[key])
                    y_preds[key] = y_p
                    print("y_pred shape: ", y_p.shape)
                else:
                    print("Distance ", key, " doesn't have any significant samples")
                    y_preds[key] = np.array([])
        else:
            y_preds = model.predict(X_test)
        final_result, real_fire_alarms = timeSeriesTest(y_test, y_preds, leave_one_out, ignition_sample, all_skipped_samples)


        if type(X_test) is dict:
            total_distance+=final_result.fire_distance
            # total_latency+=final_result.latency
            # total_false_alarms+=len(final_result.false_alarms)
            total_latency = 100000
            false_alarm_times = []
            for r in real_fire_alarms:
                print(r)
                if not r.fire:
                    if r.time not in false_alarm_times:
                        false_alarm_times.append(r.time)
                        # if r.time > 630: # out of the baseline
                        total_false_alarms+=1
                if r.latency < total_latency:
                    total_latency = r.latency
        else:
            full_TP+=final_result.TP
            full_TN+=final_result.TN
            full_FN+=final_result.FN
            full_FP+=final_result.FP


    if type(X_test) is dict:
        print("Average Bin:               ", total_distance/repeats)
        print("Average Distance (meters): ", (total_distance*(0.044))/repeats)
        print("Known fire bins:           ", leave_one_out_fires)
        # if (total_latency/repeats) > 5000:
        #     print('==== NO ALARM ====')
        # else:
        print("Average Latency:           ", total_latency/repeats)
        print("Average False Alarms:      ", total_false_alarms/repeats)
    else:
        print("Total TP: ", full_TP)
        print("Total TN: ", full_TN)
        print("Total FP: ", full_FP)
        print("Total FN: ", full_FN)
        print("Samples: ", X_test.shape[0])
        fires = 0
        for i in y_test:
            if i[0] < i[1]:
                fires+=1
        print("Fires: ", fires)
        print("Average TP: ", (100 * (full_TP/repeats)/fires), " %")
        print("Average TN: ", (100 * (full_TN/repeats)/(X_test.shape[0] - fires)), " %")
        print("Misclassified percentages:")
        print("Average FP: ", (100 * (full_FP/repeats)/(X_test.shape[0] - fires)), " %")
        print("Average FN: ", (100 * (full_FN/repeats)/fires), " %")
    print("Finished")


if __name__ == '__main__':
    # if len(sys.argv) == 2:
    #     mod_str = sys.argv[len(sys.argv) - 1]
    #     mod = load_model('models/' + mod_str)
    #     run_experiment(mod)
    # else:
    #     run_experiment()

    if len(sys.argv) == 2:
        leave_one_out = sys.argv[1]
        mod = None
    elif len(sys.argv) == 3:
        leave_one_out = sys.argv[1]
        mod = sys.argv[2]
    else:
        leave_one_out = None
        mod = None

    run_experiment(leave_one_out, mod)
