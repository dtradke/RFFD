// Alex's parser for the mmWave data from the board. Writes to csvs
// compile: g++ -std=c++11 parser.cc -o parser.out
// run: ./parser.out binary.dat


#include <iostream>
#include <string>
#include <fstream>
#include <sys/stat.h>
#include <string.h>
#include <cmath>
#include <bitset>

extern "C" {
    typedef enum MmwDemo_output_message_type_e
    {
        MMWDEMO_OUTPUT_MSG_DETECTED_POINTS = 1,

        MMWDEMO_OUTPUT_MSG_RANGE_PROFILE,

        MMWDEMO_OUTPUT_MSG_NOISE_PROFILE,

        MMWDEMO_OUTPUT_MSG_AZIMUT_STATIC_HEAT_MAP,

        MMWDEMO_OUTPUT_MSG_RANGE_DOPPLER_HEAT_MAP,

        MMWDEMO_OUTPUT_MSG_STATS,

        MMWDEMO_OUTPUT_MSG_MAX
    } MmwDemo_output_message_type_e;

    typedef struct MmwDemo_output_message_header_t
    {
        uint16_t    magicWord[4];

        uint32_t    version;

        uint32_t    totalPacketLen;

        uint32_t    platform;

        uint32_t    frameNumber;

        uint32_t    timeCpuCycles;

        uint32_t    numDetectedObj;

        uint32_t    numTLVs;

        uint32_t    subFrameNumber;
    } MmwDemo_output_message_header;

    typedef struct MmwDemo_output_message_tl_t
    {
        uint32_t    type;

        uint32_t    length;
    } MmwDemo_output_message_tl;

    typedef struct MmwDemo_output_message_dataObjDescr_t
    {
        uint16_t    numDetectedObj;

        uint16_t    xyzQFormat;
    } MmwDemo_output_message_dataObjDescr;

    typedef volatile struct MmwDemo_detectedObj_t
    {
        uint16_t    rangeIdx;
        int16_t     dopplerIdx;
        uint16_t    peakVal;
        int16_t     x;
        int16_t     y;
        int16_t     z;
    } MmwDemo_detectedObj;

    typedef struct MmwDemo_output_message_stats_t
    {
        uint32_t    interFrameProcessingTime;
        uint32_t    transmitOutputTime;
        uint32_t    interFrameProcessingMargin;
        uint32_t    interChirpProcessingMargin;
        uint32_t    activeFrameCPULoad;
        uint32_t    interFrameCPULoad;
    } MmwDemo_output_message_stats;

}


int main(int argc, char* argv[]) {
    const char* filen;
    if(argc != 2) {
        std::cout << "usage: ./parser [filename]" << std::endl;
        return 0;
    } else {
        filen = argv[1];
    }
    std::ifstream file;
    std::string filename = filen;

    size_t last_index = filename.find_last_of(".");

    std::string output_dir = filename.substr(0, last_index);

    const char* output_dir_name = output_dir.c_str();

    const int dir_err = mkdir(output_dir_name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    if (dir_err) {
        std::cerr << "Error creating directory: " << strerror(errno) << std::endl;
        throw 1;
    }

    std::string csv_name = output_dir + "/" + output_dir + ".csv";

    std::ofstream csv(csv_name);

    // csv << "frame_num";

    for (int i = 0; i < 256; i ++) {
      if(i == 0){
        csv << "obj_" + std::to_string(i + 1);
      }
      else{
        csv << ",obj_" + std::to_string(i + 1);
      }
    }

    csv << std::endl;





    //Skipping bytes
    int version_num_1 = 33554436;
    int version_num_2 = 16908293;
    size_t skip_count = 0;
    while(1) {
        file.open(filename.c_str(), std::ios::binary);
        char skip[99999];
        std::cout << "skip count is now: " << skip_count << std::endl;
        file.read((char*)skip, skip_count);
        MmwDemo_output_message_header th;
        file.read((char*)&th, sizeof(th));
        std::cout << "version: " << th.version << std::endl;
        std::cout << "totalPacketLen: " << th.totalPacketLen << std::endl;
        std::cout << "platform: " << th.platform << std::endl;
        std::cout << "frameNumber: " << th.frameNumber << std::endl;
        std::cout << "numDetectedObj: " << th.numDetectedObj << std::endl;
        std::cout << "numTLVs: " << th.numTLVs << std::endl;
        if ((th.version == version_num_1) && (th.numTLVs < 4)){
            std::cout << "Found a valid header!" << std::endl;
            file.close();
            break;
        }
        skip_count ++;
        if (file.eof()) {
          file.close();
          std::cout << "no valid header found!" << std::endl;
          return 0;
        }
        file.close();
    }


    file.open(filename.c_str(), std::ios::binary);
    char skip[99999];
    file.read((char*)skip, skip_count);
    if (!file.is_open()) {
        std::cerr << "cannot open file" << std::endl;
        exit(EXIT_FAILURE);
    }


    int frame_num = 0;
    while(!file.eof()) {
        size_t current_processed = 0;
        MmwDemo_output_message_header h;
        file.read((char*)&h, sizeof(h));
        current_processed += sizeof(h);
        std::cout << "version: " << h.version << std::endl;
        std::cout << "totalPacketLen: " << h.totalPacketLen << std::endl;
        std::cout << "platform: " << h.platform << std::endl;
        std::cout << "frameNumber: " << h.frameNumber << std::endl;
        std::cout << "numDetectedObj: " << h.numDetectedObj << std::endl;
        std::cout << "numTLVs: " << h.numTLVs << std::endl;
        frame_num ++;
        // csv << frame_num;

        for (int i = 0; i < h.numTLVs; i ++) {
            MmwDemo_output_message_tl t;
            file.read((char*)&t, sizeof(t));
            current_processed += sizeof(t);
            std::cout << "the TLV is of type: " << t.type << std::endl;
            std::cout << "it's length is: " << t.length << std::endl;
            if (t.type == MMWDEMO_OUTPUT_MSG_DETECTED_POINTS) {
                std::cout << "this is a detected points" << std::endl;
                MmwDemo_output_message_dataObjDescr objd;
                file.read((char*)&objd, sizeof(objd));
                current_processed += sizeof(objd);
                std::cout << "there are " << objd.numDetectedObj << " objects" << std::endl;
                for (int j = 0; j < objd.numDetectedObj; j ++) {
                    MmwDemo_detectedObj obj;
                    file.read((char*)&obj, sizeof(obj));
                    current_processed += sizeof(obj);
                    std::cout << "object " << j + 1 << " is : [";
                    std::cout << obj.x << " " << obj.y << " " << obj.z << " with peakval " << obj.peakVal << "]" << std::endl;
                }
            } else if (t.type == MMWDEMO_OUTPUT_MSG_RANGE_PROFILE) {
                std::cout << "this is a range profile" << std::endl;
                uint16_t nums = t.length / sizeof(uint16_t);

                std::cout << "there are " << nums << " points" << std::endl;
                uint16_t range_profile[nums];
                file.read((char*)range_profile, sizeof(uint16_t) * nums);
                current_processed += sizeof(uint16_t) * nums;

                // std::ofstream output_file(output_dir + "/" + output_dir + "_" + std::to_string(frame_num) + ".txt");

                for(int k = 0; k < nums; k ++) {
                    if (k != 0) {
                        // output_file << ",";
                        csv << ",";
                    }
                    // csv << ",";
                    double q8 = range_profile[k] / 4;
                    double actual = q8 / pow(2, 8);
                    double result = pow(2, actual);
                    csv << result;
                    // output_file << result;

                }
                csv << std::endl;
                // output_file.close();
            } else if (t.type == MMWDEMO_OUTPUT_MSG_NOISE_PROFILE) {
                std::cout << "this is a noise profile" << std::endl;
            } else if (t.type == MMWDEMO_OUTPUT_MSG_AZIMUT_STATIC_HEAT_MAP) {
                std::cout << "this is a heatmap" << std::endl;
            } else if (t.type == MMWDEMO_OUTPUT_MSG_RANGE_DOPPLER_HEAT_MAP) {
                std::cout << "this is a doppler heat map" << std::endl;
            } else if (t.type == MMWDEMO_OUTPUT_MSG_STATS) {
                std::cout << "this is a message stats" << std::endl;
                MmwDemo_output_message_stats st;
                file.read((char*)&st, sizeof(st));
                current_processed += sizeof(st);
            } else if (t.type == MMWDEMO_OUTPUT_MSG_MAX) {
                std::cout << "this is a message max" << std::endl;
            } else {
                std::cout << "not defined yet" << std::endl;
            }
        }
        std::streamsize padding = h.totalPacketLen - current_processed;
        std::cout << "there is " << padding << " byte left" << std::endl;
        char pad[99999];
        file.read(pad, padding);
    }
    std::cout << frame_num << " frames processed" << std::endl;
    csv.close();
    file.close();
}
