# RFire
# 11/16/19
#
# Original NON-AI RFire algorithm


import pandas as pd
import numpy as np
from numpy import genfromtxt
from matplotlib import pyplot as plt
import collections
import math
from scipy.signal import argrelmin, argrelmax

FLUCT_RANGE = .11
SEARCH_SPACE_MAX = 4
ENDBASELINE = 30
POINTTHRESH = 0.8
RECENT_TIME_BUFFER = 30

# NOTE: MAKE SURE THAT WHEN YOU USE DIFFERENT CSVS, THE INDICES ARE CORRECT.

# for first_experiments/beta_index and midul_apartment/ (add in the index_col=False)
# TIMESTEP_IDX = 0
# OBJAMT = 1 #CHANGED TO 0 from 1 FOR CONE EXPERIMENTS (I THINK OTHERS ARE GOOD)
# X_IDX = 3
# Y_IDX = 4
# POWER_IDX = 7

# for first_experiments/midul_index
# TIMESTEP_IDX = 1
# OBJAMT = 0
# X_IDX = 4
# Y_IDX = 5
# POWER_IDX = 8

#cone index
TIMESTEP_IDX = 0
OBJAMT = 0
X_IDX = 3
Y_IDX = 4
POWER_IDX = 7

class SigObject(object):

    def __init__(self, x=None, y=None, id=None, power=0):
        self.x = x
        self.y = y
        self.id = id
        self.power = collections.deque(40*[power], 40)
        self.distance = math.sqrt((x ** 2) + (y ** 2))
        self.movement = False
        self.avg_movement_x = 0
        self.avg_movement_y = 0
        self.cache_movement_x = 0 #cache movement calculates how much an object has moved in recent memory
        self.cache_movement_y = 0
        self.flag = False
        self.avgrelmax = power
        self.avgrelmin = power
        self.returning_obj = True
        self.fluc_flag = False
        self.fluc_zone = []
        self.timesteps = []
        # self.occluded = collections.deque(10*[1], 10) #1 means not occluded, 0 means it is
        self.occlusions = collections.deque(30*[0], 30)
        self.ram = []
        self.fire_flag = False
        self.has_baseline = False
        self.normal_reflection = sum(self.power) / len(self.power)
        self.time_fluctuating = 0
        try:
            self.direction = math.atan2(y,x)/math.pi*180
        except:
            self.direction = 90

        self.range = self.normal_reflection * FLUCT_RANGE

    def updateMetrics(self, cur_x, cur_y, cur_power, timestep):
        old_x = self.x
        old_y = self.y

        self.x = cur_x
        self.y = cur_y
        self.power.popleft()
        self.power.append(cur_power)
        self.distance = math.sqrt((self.x ** 2) + (self.y ** 2))
        self.timesteps.append(timestep)
        self.ram.append(cur_power)

        if timestep > ENDBASELINE:
            self.cache_movement_x = self.cache_movement_x + (old_x - cur_x)
            self.cache_movement_y = self.cache_movement_y + (old_y - cur_y)

        self.avg_movement_x = self.avg_movement_x + (old_x - cur_x)
        self.avg_movement_y = self.avg_movement_y + (old_y - cur_y)

        if abs(self.cache_movement_x) > abs(self.avg_movement_x) or abs(self.cache_movement_y) > abs(self.avg_movement_y):
            self.movement = True
        else:
            self.movement = False

        try:
            self.direction = math.atan2(cur_y,cur_x)/math.pi*180
        except:
            self.direction = 90

    def scatterplot(self, name):
        x_ax = self.timesteps
        y_ax = self.ram

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1,axisbg='1.0')
        #
        # for d, color, group in zip(d, colors, groups):
        #     x, y = d
        #     ax.scatter(x, y, alpha=0.8, c = color, edgecolors='none', s=30, label=group)


        x = x_ax
        y = y_ax

        ax.scatter(x, y, alpha=0.7, c='blue', edgecolors = 'none', s=30, label='Days')
        # plt.plot(x, np.poly1d(np.polyfit(x, y, 1))(x), polymin=0)

        plt.title(name, fontsize=18)
        # plt.legend(loc=2)


        ax.set_ylim(ymin=0)
        ax.set_xlim(xmin=0)
        plt.yticks(fontsize=15)
        plt.xticks(fontsize=15)
        plt.ylabel('Reflection Power', fontsize=15)
        plt.xlabel('Time (0.1 seconds)', fontsize=15)

        plt.show()

    def graphItNoFire(self):
        data = self.ram#[:400]

        # data2 = []
        # s = 0
        # e = 30
        # for i in range(0, (len(data) // 30)):
        #     new_arr = data[s:e]
        #     data2.append(min(new_arr))
        #     s = s + 30
        #     e = e + 30



        # data = np.array(data2)

        t = np.arange(0,len(data))
        data1 = data

        # fig, ax1 = plt.subplots()

        color = 'tab:blue'
        # ax1.set_title('Fire on Drywall at 1 Meter', fontsize=15)
        plt.xlabel('Time (Seconds)', fontsize=15)
        plt.ylabel('Reflection Power', color=color, fontsize=15)
        # ax1.set_ylim(0,4800)
        line1, = plt.plot(t, data1, color=color, label='Reflection Power')
        plt.tick_params(axis='y', labelcolor=color, labelsize=15)
        plt.tick_params(axis='x', labelsize=15)
        # plt.set_xticklabels(('0', '0', '50', '100', '150', '200', '250'))

        plt.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.legend((line1,), ('Reflection Power', ))
        plt.show()


    def getAvgFluctuation(self):
        np_power = np.array(self.power)
        self.avgrelmax = ((self.avgrelmax + len(argrelmax(np_power)[0])) / 2) / len(self.power)
        self.avgrelmin = ((self.avgrelmin + len(argrelmin(np_power)[0])) / 2) / len(self.power)
        return

    def checkFluctuation(self, timestep):
        # np_power = np.array(self.power)
        np_power = np.array(self.fluc_zone)
        np_recent = np.array(self.power)
        np_occlusions = np.array(self.occlusions)

        # if len(np_power) % 30 == 0 and len(np_power) > 30:
        #     np_power2 = []
        #     s = 0
        #     e = 30
        #     for x in range(1, (int((len(np_power) / 30)) + 1)):
        #         new_arr = np_power[s:e]
        #         np_power2.append(min(new_arr))
        #         s = s + 30
        #         e = e + 30
        #     np_power = np.array(np_power2)
        # else:
        #     return


        cur_rel_max = argrelmax(np_power)
        cur_rel_min = argrelmin(np_power)
        rel_max_amt = argrelmax(np_recent)
        rel_min_amt = argrelmin(np_recent)
        cur_occlusion_rel_max = argrelmax(np_occlusions)
        cur_occlusion_rel_min = argrelmin(np_occlusions)

        # if self.id == 1:
        #     print(np_power)
        #     print(cur_rel_max[0])
        #     print(cur_rel_min[0])
        #     print()
            # exit()

        # if len(cur_rel_max[0]) > 2 and len(cur_rel_min[0]) > 2:
        # NOTE:  and (len(cur_rel_max[0]) + len(cur_rel_min[0])) < 8 ADDED BELOW AND NOT TESTED YET
        if (len(cur_rel_max[0]) + len(cur_rel_min[0])) >= 4 and (len(cur_rel_max[0]) + len(cur_rel_min[0])) < 8:
            distances = self.getFluctuationDistances(cur_rel_max[0], cur_rel_min[0], np_power)
            avg_fluc_dist = sum(distances) / len(distances)
            max_fluc_dist = max(distances)
            rel_max_metric = len(cur_rel_max[0]) / len(np_power)
            rel_min_metric = len(cur_rel_min[0]) / len(np_power)
            avg_extrema = (rel_max_metric + rel_min_metric) / 2


            # if avg_fluc_dist > self.range or len(rel_max_amt[0]) > self.avgrelmax and len(rel_min_amt[0]) > self.avgrelmin:
            if avg_fluc_dist > self.range and rel_max_metric > self.avgrelmax and rel_min_metric > self.avgrelmin and self.fire_flag == False and np.mean(np_power) < (self.normal_reflection - self.range):
                if self.has_baseline == True and self.distance < SEARCH_SPACE_MAX and self.movement == False and len(self.ram) > 600:
                    occ_diff = self.occlusions[len(self.occlusions) - 1] - self.occlusions[0]
                    if len(set(self.occlusions)) ==  1 or len(set(self.occlusions)) == 2 and occ_diff < 0:
                        if len(self.timesteps) > (POINTTHRESH * timestep):
                            self.fire_flag = True
                            print('CONCLUSION: OBJECT {} IS ON FIRE! Occuring at time {} \U0001f525 \U0001f525 \U0001f525'.format(self, timestep))
        return

    def getFluctuationDistances(self, cur_rel_max, cur_rel_min, arr):
        idx = 0
        important_idx = []
        for x in cur_rel_max:
            important_idx.append(x)
        for x in cur_rel_min:
            important_idx.append(x)
        important_idx = sorted(important_idx)
        fluctuation_diff = []
        for i in range(0, (len(important_idx) - 1)):
            diff = arr[important_idx[i]] - arr[important_idx[i+1]]
            fluctuation_diff.append(abs(diff))
        return fluctuation_diff

    def getCurAvgPower(self):
        numerator = list(self.power)[(-1 * RECENT_TIME_BUFFER):]
        min_recent = min(numerator)
        max_recent = max(numerator)
        return min_recent, max_recent

    def __repr__(self):
        return "SigObject({},{} meters,x: {}, y: {}, {} degrees, Normal: {})".format(self.id, self.distance, self.x, self.y, self.direction, self.normal_reflection)


def makeInitialObj(data_df):
    cols = data_df.columns
    first = data_df.loc[data_df[cols[OBJAMT]] == 1]
    id = 0
    objs = []
    old_obj_amt = 0

    for index, row in first.iterrows():
        old_obj_amt = row[cols[OBJAMT]]
        objs.append(SigObject(float(row[cols[X_IDX]]), float(row[cols[Y_IDX]]), id, int(row[cols[POWER_IDX]])))
        id = id + 1
    return objs, old_obj_amt, data_df

def getOccluded(sig_objects, timestep):
    for obj in sig_objects:
        occlusions = 0
        for a in sig_objects:
            if a.id != obj.id:
                if a.direction <= (obj.direction + 10) and a.direction >= (obj.direction - 10):
                    if a.distance < obj.distance:
                        if timestep in a.timesteps:
                            if obj.distance < SEARCH_SPACE_MAX:
                                occlusions = occlusions + 1
        obj.occlusions.popleft()
        obj.occlusions.append(occlusions)
    return sig_objects

def checkForFire(sig_objects, new_sig_objects, timestep):
    sig_objects = getOccluded(sig_objects, timestep)

    for obj in sig_objects:
        # current_state = obj.power[0] - obj.power[len(obj.power) - 1]
        comp_to_avg = obj.normal_reflection - obj.power[len(obj.power) - 1]
        current_max_min = max(obj.power) - min(obj.power)
        min_recent, max_recent = obj.getCurAvgPower()
        recent_diff = max_recent - min_recent
        within_10_perc = obj.normal_reflection * .1

        #still decreasing
        if abs((obj.power[len(obj.power) - 1] - obj.power[len(obj.power) - 2])) < obj.range and comp_to_avg > obj.range: #current_state
            obj.fluc_flag = True

        #back to normal level
        # if obj.flag == True and recent_diff <= (obj.normal_reflection + within_10_perc) and recent_diff >= (obj.normal_reflection - within_10_perc):
        if obj.flag == True and max_recent <= (obj.normal_reflection + within_10_perc) and min_recent >= (obj.normal_reflection - within_10_perc) and recent_diff < within_10_perc:
            obj.flag = False
            obj.fluc_flag = False
            obj.time_fluctuating = 0

        if obj.fluc_flag:
            obj.fluc_zone.append(obj.power[len(obj.power) - 1])
        else:
            obj.fluc_zone = []

        #dropped and not returned and stabilized yet
        if obj.flag and obj.has_baseline:
            obj.time_fluctuating += 1
            if obj.time_fluctuating > (RECENT_TIME_BUFFER*2):
                obj.checkFluctuation(timestep)

        #drops
        if comp_to_avg > obj.range and obj.has_baseline:
            obj.flag = True
            if obj.time_fluctuating > (RECENT_TIME_BUFFER*2):
                obj.checkFluctuation(timestep)

    return



def getBaselineRange(sig_objects):
    for obj in sig_objects:
        avg_power = (sum(obj.power) / len(obj.power))
        if avg_power > 50:
            cur_range = avg_power * FLUCT_RANGE
        else:
            cur_range = 15
        obj.range = cur_range
    return sig_objects


def runSim(sig_objects, data_df, old_obj_amt, directory):
    cols = data_df.columns


    idx = 2
    length = data_df[cols[TIMESTEP_IDX]][data_df.shape[0] - 1]


    # for a in sig_objects:
    #     print(a)



    for z in range(0, int(length)):
        new_sig_objects = []
        cur_df = data_df.loc[data_df[cols[TIMESTEP_IDX]] == idx]

        # print(cur_df)
        # print()
        # if idx == 3:
        #     for a in sig_objects:
        #         print(a)
        #     exit()

        for index, row in cur_df.iterrows():
            cur_x = float(row[cols[X_IDX]])
            cur_y = float(row[cols[Y_IDX]])
            cur_power = int(row[cols[POWER_IDX]])
            cur_obj_amt = int(row[cols[OBJAMT]])
            obj_amt = 0

            for a in sig_objects:
                obj_amt+=1
                #updates
                if cur_x >= (a.x - 0.05) and cur_x <= (a.x + 0.05):# and idx not in a.timesteps: #was 0.5      and idx not in a.timesteps
                    if  cur_y >= (a.y - 0.05) and cur_y <= (a.y + 0.05): #was .2
                        # if a.id == 1:
                        #     print('obj: ', idx)
                        a.updateMetrics(cur_x, cur_y, cur_power, (idx+1))
                        a.returning_obj = True
                        break
            else:
                #new object
                new_id = (len(sig_objects) + len(new_sig_objects))
                new_obj = SigObject(cur_x, cur_y, new_id, cur_power)
                new_obj.power.popleft()
                new_obj.power.append(cur_power)
                new_obj.returning_obj = False
                new_obj.timesteps.append(idx+1)
                new_obj.ram.append(cur_power)
                new_sig_objects.append(new_obj)

        sig_objects = sig_objects + new_sig_objects

        idx+=1



        if idx < ENDBASELINE:
            sig_objects = getBaselineRange(sig_objects)
            sig_objects = getOccluded(sig_objects, idx)
            for obj in sig_objects:
                obj.getAvgFluctuation()
                obj.has_baseline = True
        elif idx >= ENDBASELINE:
            # for i in sig_objects:
            #     print(i)
            # exit()
            if idx == ENDBASELINE:
                print('Going into test phase now...')
            checkForFire(sig_objects, new_sig_objects, idx)


    # exit()
    flags = 0

    for a in sig_objects:
        # print(a.has_baseline)
        print(a)
        if a.fire_flag == True:
            flags = flags + 1
        if a.id <= 10:
            # a.scatterplot(directory)
            a.graphItNoFire()

    if flags == 0:
        print("Significant objects: ", len(sig_objects))
        print('CONCLUSION: There was no fire present')

if __name__ == '__main__':
    directory = 'csv_mapping.csv'
    print('================ ', directory, '================')
    data_df = pd.read_csv(directory, index_col=False) # , index_col=False (for midul_apartment)
    print('Read data...')


    initial_objects, old_obj_amt, data_df = makeInitialObj(data_df)
    runSim(initial_objects, data_df, old_obj_amt, directory)
