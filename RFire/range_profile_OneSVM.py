

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
from os import listdir
import process_dat
import autoencoder


#Import scikit-learn dataset library
from sklearn import datasets
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import decomposition
from sklearn import datasets
from sklearn import metrics
from sklearn.svm import OneClassSVM

from keras.models import Model

import time

DEPTH = 90
tree_number = 50

def PCA_3D(X):
    samples = []
    for x in X:
        pca = decomposition.PCA(n_components=3)
        pca.fit(x)
        x = pca.transform(x)
        flat = x.flatten()
        samples.append(x.flatten())
    return np.array(samples)

def flattenData(X):
    oneDimSamples = []
    for i in range(X.shape[0]):
        window = []
        for j in range(X[i].shape[0] - 1):
            for a in range(X[i][j].shape[0] - 1):
                window.append(X[i][j][a])
            normal_reflection = np.array(X[i][j][-1:])
        window = np.array(window)
        window = np.concatenate((window, normal_reflection), axis=0)
        oneDimSamples.append(window)
    return np.array(oneDimSamples)

def alternativeTimeSeriesTest(y_test, y_pred, leave_one_out, ignition_sample):
    results = []
    for idx, y_ground in enumerate(y_test):
        correct = 0
        incorrect = 0
        FN = 0
        FP = 0
        TP = 0
        TN = 0
        FN_before_fire = 0
        fire_flag = False
        early_classification = 0
        false_alarm = 0
        real_false_alarm = 0
        ones_in_a_row = 0
        latency = 60

        print('ground: ', y_ground)
        print('pred: ', y_pred[idx])


        for i, val in enumerate(y_ground): #actual values
            print(i, ' prediction: ', y_pred[idx][i], ' ground: ', val)
            if y_pred[idx][i] == 0: #predicted no fire
                ones_in_a_row = 0
                if val == 0: #actual no fire
                    correct += 1
                    TN += 1
                else: #actual fire
                    incorrect += 1
                    FN += 1
                    if not fire_flag:
                        FN_before_fire += 1
            else: # predicted fire
                ones_in_a_row += 1
                if not fire_flag: #if it is a leave_one_out test and a fire hasn't been detected
                    if val == 1 and ones_in_a_row > 1: #if it truly is a fire
                        fire_flag = True
                        # print(" \U0001f525 \U0001f525 \U0001f525 Detected fire at time ", i, " \U0001f525 \U0001f525 \U0001f525")
                        latency = i - ignition_sample[idx]
                        print("i: ", i)
                        print("ignition_sample: ", ignition_sample[idx])
                        print("Latency: ", latency, " second(s)")
                    else: #fire detected but not a ground truth classified fire sample yet
                        future = y_ground[i:i+5]

                        if ones_in_a_row == 2:
                            real_false_alarm+=1

                        #checking if a fire is close due to the labeling format
                        if 1 not in future:
                            # print("\U0000274C \U0000274C \U0000274C FALSE ALARM at time ", i, " \U0000274C \U0000274C \U0000274C")
                            false_alarm += 1
                        else:
                            early_classification += 1

                if val == 1: #actual fire
                    correct += 1
                    TP += 1
                else: # actual no fire
                    incorrect += 1
                    FP += 1

        # print("Correct: ", correct)
        # print("incorrect: ", incorrect)
        # print("correct perc: ", correct/len(y_test))
        # print("TP: ", TP)
        # print("TN: ", TN)
        # print("FP (annoying) - no fire but predicted fire:     ", FP)
        # print("FN (bad) - actual fire but not predicted fire:  ", FN)
        # # print("Predicted no fire before time when fire starts: ", FN_before_fire)
        # print('Early Classification: ', early_classification)
        # print('False Alarms: ', false_alarm)
        # print('REAL False Alarms: ', real_false_alarm)
        results.append(np.array([latency, TP, TN, real_false_alarm, FN]))

    results = np.array(results)
    print(results)
    result = []
    min_latency = 1000
    max_TP = 0
    max_TN = 0
    total_false_alarm = 0
    total_FN = 0
    for r in results:

        if r[0] < min_latency:
            min_latency = r[0]
        max_TP+=r[1]
        max_TN+=r[2]
        total_false_alarm+=r[3]
        total_FN+=r[4]
        # if r[1] > max_TP:
        #     max_TP = r[1]
        # if r[2] > max_TN:
        #     max_TN = r[2]
        # total_false_alarm+=r[3]
        # if r[4] > max_FN:
        #     max_TN = r[4]
    result = [min_latency, max_TP, max_TN, total_false_alarm, total_FN]
    print(result)
    exit()

    return [min_latency, max_TP, max_TN, total_false_alarm, total_FN]


def timeSeriesTest(y_test, y_pred, leave_one_out, ignition_sample):
    if type(y_test) == list:
        return alternativeTimeSeriesTest(y_test, y_pred, leave_one_out, ignition_sample)

    correct = 0
    incorrect = 0
    FN = 0
    FP = 0
    TP = 0
    TN = 0
    FN_before_fire = 0
    fire_flag = False
    early_classification = 0
    false_alarm = 0
    real_false_alarm = 0
    ones_in_a_row = 0
    latency = 60


    for i, val in enumerate(y_test):
        if y_pred[i] == 0: #predicted no fire
            ones_in_a_row = 0
            if val == 0: #actual no fire
                correct += 1
                TN += 1
            else: #actual fire
                incorrect += 1
                FN += 1
                if not fire_flag:
                    FN_before_fire += 1
        else: # predicted fire
            ones_in_a_row += 1
            if leave_one_out is not None:
                if not fire_flag: #if it is a leave_one_out test and a fire hasn't been detected
                    if val == 1 and ones_in_a_row == 2: #if it truly is a fire
                        fire_flag = True
                        # print(" \U0001f525 \U0001f525 \U0001f525 Detected fire at time ", i, " \U0001f525 \U0001f525 \U0001f525")
                        latency = i - ignition_sample
                        # print("Latency: ", latency, " second(s)")
                    else: #fire detected but not a ground truth classified fire sample yet
                        future = y_test[i:i+5]

                        if ones_in_a_row == 2:
                            real_false_alarm+=1

                        #checking if a fire is close due to the labeling format
                        if 1 not in future:
                            # print("\U0000274C \U0000274C \U0000274C FALSE ALARM at time ", i, " \U0000274C \U0000274C \U0000274C")
                            false_alarm += 1
                        else:
                            early_classification += 1

            if val == 1: #actual fire
                correct += 1
                TP += 1
            else: # actual no fire
                incorrect += 1
                FP += 1

    # print("Correct: ", correct)
    # print("incorrect: ", incorrect)
    # print("correct perc: ", correct/len(y_test))
    # print("TP: ", TP)
    # print("TN: ", TN)
    # print("FP (annoying) - no fire but predicted fire:     ", FP)
    # print("FN (bad) - actual fire but not predicted fire:  ", FN)
    # # print("Predicted no fire before time when fire starts: ", FN_before_fire)
    # print('Early Classification: ', early_classification)
    # print('False Alarms: ', false_alarm)
    # print('REAL False Alarms: ', real_false_alarm)

    if leave_one_out is not None:
        results = [latency, TP, TN, real_false_alarm, FN]
    else:
        results = [latency, TP, TN, FP, FN]

    return results

def random_forest(X_train, X_test, y_train, y_test, mode):

    clf=RandomForestClassifier(n_estimators=tree_number, criterion=mode, max_depth=DEPTH)

    #Train the model using the training sets y_pred=clf.predict(X_test)
    clf.fit(X_train,y_train)

    if type(X_test) == list:
        y_pred = []
        for x in X_test:
            y_pred.append(clf.predict(x))
    else:
        y_pred=clf.predict(X_test)
    return y_pred


def write_df_to_csv(write_df, leave_one_out):
    if leave_one_out is not None:
        dir = 'RRF_rangeprofile_results/'
    else:
        dir = 'RRF_rangeprofile_sample_wise_results/'
    save_str = 'RRF_10_times_depth_' + str(DEPTH) + '_trees_' + str(tree_number)
    today_string = time.strftime("%Y%m%d-%H%M%S")
    filename =  dir + today_string + save_str + '.csv'
    write_df.to_csv(filename, index=None, header=True)


def getIgnitionSample(leave_one_out, y_test):
    if type(y_test) == list:
        print("LEAVE ONE OUT")
        ignition_samples = []
        for i, val in enumerate(y_test):
            for j, value in enumerate(val):
                if value == 1:
                    print('Ignition sample: ', j)
                    ignition_samples.append(j)
                    break
        return ignition_samples
    else:
        return 0

def getMode():
    if len(sys.argv) == 2:
        # python3 range_profile_random_forest.py autoencode
        if sys.argv[1] == 'autoencode':
            print('===== Autoencode Sample-Wise =====')
            PCA = False
            AUTOENCODE = True
            leave_one_out = None
        # python3 range_profile_random_forest.py PCA
        elif sys.argv[1] == 'PCA':
            print('===== PCA Sample-Wise =====')
            PCA = True
            AUTOENCODE = False
            leave_one_out = None
        # python3 range_profile_random_forest.py [fire to leave out]
        else:
            print('===== Regular Leave-One-Out =====')
            PCA = False
            AUTOENCODE = False
            leave_one_out = sys.argv[1]
    elif len(sys.argv) == 3:
        # python3 range_profile_random_forest.py [fire to leave out] autoencode
        if sys.argv[2] == 'autoencode':
            print('===== Autoencode Leave-One-Out =====')
            PCA = False
            AUTOENCODE = True
            leave_one_out = sys.argv[1]
        # python3 range_profile_random_forest.py [fire to leave out] PCA
        elif sys.argv[2] == 'PCA':
            print('===== PCA Leave-One-Out =====')
            PCA = True
            AUTOENCODE = False
            leave_one_out = sys.argv[1]
    # python3 range_profile_random_forest.py
    else:
        print('===== Regular Sample-Wise =====')
        PCA = False
        AUTOENCODE = False
        leave_one_out = None
    return leave_one_out, AUTOENCODE, PCA

def flattenXData(leave_one_out, AUTOENCODE, PCA, X, X_test):
    if leave_one_out is None:
        print("Leave one out is None")
        if AUTOENCODE:
            X_train, X_test = autoencoder.train_autoencoder(X, X_test)
        elif PCA:
            X_train = PCA_3D(X)
            X_test = PCA_3D(X_test)
        else:
            X_train = flattenData(X)
            X_test = flattenData(X_test)
        return X_train, X_test
    else:
        print("Leave one out is a list")
        if AUTOENCODE:
            X_train, _ = autoencoder.train_autoencoder(X, np.array([]))
            X_test_arr = []
            for x in X_test:
                _, X_test_element = autoencoder.train_autoencoder(np.array([]), x)
                X_test_arr.append(X_test_element)
        elif PCA:
            X_train = PCA_3D(X)
            X_test_arr = []
            for x in X_test:
                X_test_arr.append(PCA_3D(x))
        else:
            X_train = flattenData(X)
            X_test_arr = []
            for x in X_test:
                X_test_arr.append(flattenData(x))
        return X_train, X_test_arr

def fitSVM(train_X):
    clf = OneClassSVM(gamma=0.00001, nu=0.03).fit(train_X)
    return clf


# Driver code
def main():
    leave_one_out, AUTOENCODE, PCA = getMode()

    # for j in range(10):
    X, y_train, X_test, y_test = process_dat.getRangeProfileSamples(cnn=False, leave_one_out=leave_one_out, svm=True) #preprocess.loadDataset(leave_one_out)
    print('before: ', X.shape)
    np.random.shuffle(X)
    other_X = X[-1000:]
    X = X[:-1000]
    print('after: ', X.shape)
    new_test = np.concatenate((X_test, other_X), axis=0)

    fire = np.array([-1] * X_test.shape[0])
    no_fire = np.array([1] * other_X.shape[0])
    new_y = np.concatenate((fire, no_fire), axis=0)

    print(new_test.shape)
    print(new_y.shape)
    print(new_y)


    # print(y_train.shape)
    if type(X_test) == list:
        print("X test shape: ", X_test[0].shape)
        # print("y test shape: ",y_test[0].shape)
    else:
        print("X test shape: ", X_test.shape)
        # print("y test shape: ",y_test.shape)

    ignition_sample = getIgnitionSample(leave_one_out, y_test)
    X_train, X_test = flattenXData(leave_one_out, AUTOENCODE, PCA, X, new_test)
    mod = fitSVM(X_train)
    y = mod.predict(X_test)

    TP = 0
    FP = 0
    TN = 0
    FN = 0

    for i, val in enumerate(y):
        if val == -1 and new_y[i] == -1:
            TP+=1
        elif val == -1 and new_y[i] == 1:
            FP+=1
        elif val == 1 and new_y[i] == -1:
            FN+=1
        else:
            TN+=1

    print("TP: ", TP)
    print("FP: ", FP)
    print("FN: ", FN)
    print("TN: ", TN)

    print(y)
    exit()

    # cols = []
    # result = ["result"]
    # for i in range(0, len(X_train[0])):
    #     cols.append("col" + str(i))
    #
    # if type(X_test) == list:
    #     X_test_df_lst = []
    #     y_test_df_lst = []
    #     for idx, x in enumerate(X_test):
    #         X_test_df = pd.DataFrame(columns=cols)
    #         y_test_df = pd.DataFrame(columns=result)
    #         for i in range(x.shape[0]):
    #             X_test_df.loc[i] = x[i]
    #             y_test_df.loc[i] = y_test[idx][i]
    #         y_test_df=y_test_df.astype('int')
    #         X_test_df_lst.append(X_test_df)
    #         y_test_df_lst.append(y_test_df)
    #     X_test_df = X_test_df_lst
    #     y_test_df = y_test_df_lst
    # else:
    #     X_test_df = pd.DataFrame(columns=cols)
    #     y_test_df = pd.DataFrame(columns=result)
    #     for i in range(X_test.shape[0]):
    #         X_test_df.loc[i] = X_test[i]
    #         y_test_df.loc[i] = y_test[i]
    #     y_test_df=y_test_df.astype('int')
    #
    # X_train_df = pd.DataFrame(columns=cols)
    # y_train_df = pd.DataFrame(columns=result)
    #
    # for i in range(X_train.shape[0]):
    #     X_train_df.loc[i] = X_train[i]
    #
    # for i in range(y_train.shape[0]):
    #     y_train_df.loc[i] = y_train[i]
    #
    #
    # y_train_df=y_train_df.astype('int')
    # # y_test_df=y_test_df.astype('int')
    #
    # gini_results_arr = [tree_number, DEPTH]
    # entropy_results_arr = [tree_number, DEPTH]
    #
    #
    # y_pred_gini = random_forest(X_train_df, X_test_df, y_train_df, y_test_df, 'gini')
    # return_arr = timeSeriesTest(y_test, y_pred_gini, leave_one_out, ignition_sample)
    # full_gini_results = np.array([gini_results_arr + return_arr])
    #
    # y_pred_entropy = random_forest(X_train_df, X_test_df, y_train_df, y_test_df, 'entropy')
    # return_arr = timeSeriesTest(y_test, y_pred_entropy, leave_one_out, ignition_sample)
    # full_entropy_results = np.array([entropy_results_arr + return_arr])
    #
    # if j == 0:
    #     np_gini_results = np.array(full_gini_results)
    #     np_entropy_results = np.array(full_entropy_results)
    #     print("Gini:    ", full_gini_results)
    #     print("Entropy: ", full_entropy_results)
    # else:
    #     print("Gini:    ", full_gini_results)
    #     print("Entropy: ", full_entropy_results)
    #     #find averages
    #     np_gini_results = np.add(np_gini_results, np.array(full_gini_results))
    #     np_entropy_results = np.add(np_entropy_results, np.array(full_entropy_results))
    #
    #     np_gini_results = np.divide(np_gini_results, 2)
    #     np_entropy_results = np.divide(np_entropy_results, 2)


    # if leave_one_out is not None:
    #     final_cols = ['tree_number', 'tree_depth', 'latency', 'TP', 'TN', 'false_alarm', 'failed_to_detect']
    # else:
    #     final_cols = ['tree_number', 'tree_depth', 'latency', 'TP', 'TN', 'wrongly_classified', 'failed_to_detect']
    #
    # gini_df = pd.DataFrame.from_records(np_gini_results, columns=final_cols)
    # entropy_df = pd.DataFrame.from_records(np_entropy_results, columns=final_cols)


    # write_df_to_csv(gini_df, leave_one_out)
    # write_df_to_csv(entropy_df, leave_one_out)



# Calling main function
if __name__=="__main__":
    main()
