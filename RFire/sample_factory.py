# RFire
# 11/21/19
#
# Works with the range profile code starting with process_dat.py

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
from os import listdir
import math
from scipy.signal import argrelextrema



SIGNAL_VARIANCE_MAX_THRESHOLD = 0.25 # 0.25 for rfire_v-1.h5
SIGNAL_VARIANCE_MIN_THRESHOLD = 0.05 #0.09
SIGNAL_MVAR_MAX_THRESHOLD = 9 # MVAR means this is the threshold for the max_variance variable
SIGNAL_MVAR_MIN_THRESHOLD = 0.05

FFT_MAX_THRESHOLD = 0.0065
FFT_MIN_THRESHOLD = 0.0025
FFT_MAX_VARIANCE = 0.007
FFT_MIN_VARIANCE = 0.0015
FFT_SLOPE_MAX_THRESHOLD = 0
FFT_SLOPE_MIN_THRESHOLD = -0.001
min_slope = 0
max_slope = -10

R_SQUARED_MAX_THRESHOLD = 0.4 # MAYBE 0.25
R_SQUARED_MIN_THRESHOLD = 0.001
SIG_SQUARED_MAX_THRESHOLD = 1
SIG_SQUARED_MIN_THRESHOLD = 0.001

MIN_REF_THRESH = 100000
PERIODICY_MIN_THRESH = 70



class KernelExtrema(object):
    def __init__(self, last_extrema=0, next_extrema=0, last_extrema_time=0, next_extrema_time=0):
        self.last_extrema = last_extrema
        self.last_extrema_time = last_extrema_time
        self.next_extrema = next_extrema
        self.next_extrema_time = next_extrema_time


def makeLine(arr, reg):
    y = []

    for a, val in enumerate(arr):
        y.append((reg[0] * a) + reg[1])
    return y


def makeFlatFrom3D(input_samples, exp):
    if input_samples.size < 2:
        return np.array([]) # NOTE: THIS JUST RETURNS EMPTY NOW

    flat_samples = []
    len_sample = input_samples.shape[1]
    x_axis = np.linspace(0, len_sample, len_sample)

    for i, val in enumerate(input_samples):
        samplemax = np.amax(val, axis=0)
        samplemin = np.amin(val, axis=0)

        dists_arr = [3, 8, 13, 18, 23] # len 5
        # dists_arr = [1, 3, 5, 7, 9] # len 2

        l_obj = exp.objects[int(val[0][dists_arr[2]])]
        lm2_obj = exp.objects[int(val[0][dists_arr[0]])]
        lm1_obj = exp.objects[int(val[0][dists_arr[1]])]
        lp1_obj = exp.objects[int(val[0][dists_arr[3]])]
        lp2_obj = exp.objects[int(val[0][dists_arr[4]])]

        refs_arr = [0, 5, 10, 15, 20] # len 5
        # refs_arr = [0, 2, 4, 6, 8] # len 2

        l_refs = val[:,refs_arr[2]]
        lm2_refs = val[:,refs_arr[0]]
        lm1_refs = val[:,refs_arr[1]]
        lp1_refs = val[:,refs_arr[3]]
        lp2_refs = val[:,refs_arr[4]]


        l_perc_fluc = samplemax[refs_arr[2]] - samplemin[refs_arr[2]] / l_obj.fluctuation_range
        l_linear_regression = np.polyfit(x_axis, l_refs, 1)
        l_diff = l_refs[-1:][0] - l_refs[:1][0]

        lm2_perc_fluc = samplemax[refs_arr[0]] - samplemin[refs_arr[0]] / lm2_obj.fluctuation_range
        lm2_linear_regression = np.polyfit(x_axis, lm2_refs, 1)
        lm2_diff = lm2_refs[-1:][0] - lm2_refs[:1][0]

        lm1_perc_fluc = samplemax[refs_arr[1]] - samplemin[refs_arr[1]] / lm1_obj.fluctuation_range
        lm1_linear_regression = np.polyfit(x_axis, lm1_refs, 1)
        lm1_diff = lm1_refs[-1:][0] - lm1_refs[:1][0]

        lp2_perc_fluc = samplemax[refs_arr[4]] - samplemin[refs_arr[4]] / lp2_obj.fluctuation_range
        lp2_linear_regression = np.polyfit(x_axis, lp2_refs, 1)
        lp2_diff = lp2_refs[-1:][0] - lp2_refs[:1][0]

        lp1_perc_fluc = samplemax[refs_arr[3]] - samplemin[refs_arr[3]] / lp1_obj.fluctuation_range
        lp1_linear_regression = np.polyfit(x_axis, lp1_refs, 1)
        lp1_diff = lp1_refs[-1:][0] - lp1_refs[:1][0]

        if l_perc_fluc > exp.max_perc_fluc:
            exp.max_perc_fluc = l_perc_fluc
        if l_perc_fluc < exp.min_perc_fluc:
            exp.min_perc_fluc = l_perc_fluc
        if math.degrees(math.atan(l_linear_regression[0]/len_sample)) < exp.min_degree:
            exp.min_degree = math.degrees(math.atan(l_linear_regression[0]/len_sample))
        if math.degrees(math.atan(l_linear_regression[0]/len_sample)) > exp.max_degree:
            exp.max_degree = math.degrees(math.atan(l_linear_regression[0]/len_sample))
        if l_diff > exp.max_diff:
            exp.max_diff = l_diff
        if l_diff < exp.min_diff:
            exp.min_diff = l_diff

        l = [l_perc_fluc, l_diff, math.degrees(math.atan(l_linear_regression[0]/len_sample)), l_obj.distance, l_obj.normal_reflection]
        l_one_closer = [lm1_perc_fluc, lm1_diff, math.degrees(math.atan(lm1_linear_regression[0]/len_sample)), lm1_obj.distance, lm1_obj.normal_reflection]
        l_two_closer = [lm2_perc_fluc, lm2_diff, math.degrees(math.atan(lm2_linear_regression[0]/len_sample)), lm2_obj.distance, lm2_obj.normal_reflection]
        l_one_further = [lp1_perc_fluc, lp1_diff, math.degrees(math.atan(lp1_linear_regression[0]/len_sample)), lp1_obj.distance, lp1_obj.normal_reflection]
        l_two_further = [lp2_perc_fluc, lp2_diff, math.degrees(math.atan(lp2_linear_regression[0]/len_sample)), lp2_obj.distance, lp2_obj.normal_reflection]

        flat_samples.append(np.array(l_two_closer + l_one_closer + l + l_one_further + l_two_further))

    return np.array(flat_samples)


def generateFlatSample(exp, obj, start_time, end_time):
    x_axis = np.linspace(0, (end_time-start_time), (end_time-start_time))
    l_perc_fluc = (max(obj.reflection[start_time:end_time]) - min(obj.reflection[start_time:end_time])) / obj.fluctuation_range
    l_linear_regression = np.polyfit(x_axis, obj.reflection[start_time:end_time], 1)
    l_diff = obj.reflection[start_time:end_time][-1:][0] - obj.reflection[start_time:end_time][:1][0]

    # y = makeLine(obj.reflection[start_time:end_time], l_linear_regression)
    # plt.plot(x_axis, y)
    # plt.plot(x_axis, obj.reflection[start_time:end_time])
    # plt.show()
    # exit()

    lm2_perc_fluc = (max(exp.objects[obj.distance - 2].reflection[start_time:end_time]) - min(exp.objects[obj.distance - 2].reflection[start_time:end_time])) / exp.objects[obj.distance - 2].fluctuation_range
    lm2_linear_regression = np.polyfit(x_axis, exp.objects[obj.distance - 2].reflection[start_time:end_time], 1)
    lm2_diff = exp.objects[obj.distance - 2].reflection[start_time:end_time][-1:][0] - exp.objects[obj.distance - 2].reflection[start_time:end_time][:1][0]
    lm1_perc_fluc = (max(exp.objects[obj.distance - 1].reflection[start_time:end_time]) - min(exp.objects[obj.distance - 1].reflection[start_time:end_time])) / exp.objects[obj.distance - 1].fluctuation_range
    lm1_linear_regression = np.polyfit(x_axis, obj.reflection[start_time:end_time], 1)
    lm1_diff = exp.objects[obj.distance - 1].reflection[start_time:end_time][-1:][0] - exp.objects[obj.distance - 1].reflection[start_time:end_time][:1][0]

    lp2_perc_fluc = (max(exp.objects[obj.distance + 2].reflection[start_time:end_time]) - min(exp.objects[obj.distance + 2].reflection[start_time:end_time])) / exp.objects[obj.distance + 2].fluctuation_range
    lp2_linear_regression = np.polyfit(x_axis, exp.objects[obj.distance + 2].reflection[start_time:end_time], 1)
    lp2_diff = exp.objects[obj.distance + 2].reflection[start_time:end_time][-1:][0] - exp.objects[obj.distance + 2].reflection[start_time:end_time][:1][0]
    lp1_perc_fluc = (max(exp.objects[obj.distance + 1].reflection[start_time:end_time]) - min(exp.objects[obj.distance + 1].reflection[start_time:end_time])) / exp.objects[obj.distance + 1].fluctuation_range
    lp1_linear_regression = np.polyfit(x_axis, exp.objects[obj.distance + 1].reflection[start_time:end_time], 1)
    lp1_diff = exp.objects[obj.distance + 1].reflection[start_time:end_time][-1:][0] - exp.objects[obj.distance + 1].reflection[start_time:end_time][:1][0]


    l = [l_perc_fluc, l_diff, math.degrees(math.atan(l_linear_regression[0]/(end_time-start_time))), obj.distance, obj.normal_reflection]
    l_one_closer = [lm1_perc_fluc, lm1_diff, math.degrees(math.atan(lm1_linear_regression[0]/(end_time-start_time))), exp.objects[obj.distance - 1].distance, exp.objects[obj.distance - 1].normal_reflection]
    l_two_closer = [lm2_perc_fluc, lm2_diff, math.degrees(math.atan(lm2_linear_regression[0]/(end_time-start_time))), exp.objects[obj.distance - 2].distance, exp.objects[obj.distance - 2].normal_reflection]
    l_one_further = [lp1_perc_fluc, lp1_diff, math.degrees(math.atan(lp1_linear_regression[0]/(end_time-start_time))), exp.objects[obj.distance + 1].distance, exp.objects[obj.distance + 1].normal_reflection]
    l_two_further = [lp2_perc_fluc, lp2_diff, math.degrees(math.atan(lp2_linear_regression[0]/(end_time-start_time))), exp.objects[obj.distance + 2].distance, exp.objects[obj.distance + 2].normal_reflection]

    return np.array(l_two_closer + l_one_closer + l + l_one_further + l_two_further)




def getFirstExtremas(exp, obj, start_time, cur_step, end_time):
    loop_end = 10
    loop_end_inc = 10
    last_extrema = 0
    last_extrema_time = 0
    next_extrema = 0
    next_extrema_time = 0

    if start_time < 10:
        loop_end = 0
    else:
        loop_end = start_time - 10

    found = False
    for i in range(start_time, loop_end, -1): #this should count backwards
        if (obj.reflection[i-1] < obj.reflection[i] and obj.reflection[i+1] < obj.reflection[i]) or (obj.reflection[i-1] > obj.reflection[i] and obj.reflection[i+1] > obj.reflection[i]): #extrema
            last_extrema = obj.reflection[i]
            last_extrema_time = cur_step - i
            break
    else:
        last_extrema = obj.reflection[0]
        last_extrema_time = cur_step

    # find next extrema
    if (end_time - cur_step) < loop_end_inc:
        loop_end_inc = end_time

    for i in range(cur_step, (cur_step + loop_end_inc)): #this should count backwards
        if (obj.reflection[i-1] < obj.reflection[i] and obj.reflection[i+1] < obj.reflection[i]) or (obj.reflection[i-1] > obj.reflection[i] and obj.reflection[i+1] > obj.reflection[i]): #extrema
            next_extrema = obj.reflection[i]
            next_extrema_time = i - cur_step
            break
    else: #didn't find a future extrema, set it to last one
        next_extrema = obj.reflection[end_time]
        next_extrema_time = end_time - cur_step

    return KernelExtrema(last_extrema, next_extrema, last_extrema_time, next_extrema_time)

def updateExtrema(Extremas, obj, time, start_time, end_time):
    loop_end_inc = 10

    # checks if current sample is extrema
    if (obj.reflection[time-1] < obj.reflection[time] and obj.reflection[time+1] < obj.reflection[time]) or (obj.reflection[time-1] > obj.reflection[time] and obj.reflection[time+1] > obj.reflection[time]): #extrema
        Extremas.last_extrema = obj.reflection[time]
        Extremas.last_extrema_time = 0
        # print("This is extrema: ", obj.reflection[time-1], " ", obj.reflection[time], " and ", obj.reflection[time+1], " ", obj.reflection[time])
        # print("This is extrema: ", obj.reflection[time-1] - obj.reflection[time], " and ", obj.reflection[time+1] - obj.reflection[time])
    else:
        Extremas.last_extrema_time+=1

    # find next extrema
    if (end_time - time) < loop_end_inc:
        loop_end_inc = (end_time - time)

    for i in range(time+1, (time + loop_end_inc)): #this should count backwards
        if (obj.reflection[i-1] < obj.reflection[i] and obj.reflection[i+1] < obj.reflection[i]) or (obj.reflection[i-1] > obj.reflection[i] and obj.reflection[i+1] > obj.reflection[i]): #extrema
            Extremas.next_extrema = obj.reflection[i]
            Extremas.next_extrema_time = (i - time)
            break
    else: #didn't find a future extrema, set it to last one
        Extremas.next_extrema = obj.reflection[end_time]
        Extremas.next_extrema_time = (end_time - time)

    return Extremas




#IDEA: take out reflection and normal reflection and just have the difference between them...
# ensemble by clustering fft, or SVM with fft (MAYBE: 3d clustering using max, min, avg... can visualize too)


def generateSample(exp, obj, start_time, end_time, fire, flat):
    if flat:
        return generateFlatSample(exp, obj, start_time, end_time)
    window = []

    space = exp.max_reflection - exp.min_reflection

    time = np.linspace(0, 1, (end_time - start_time))
    pm2 = np.poly1d(np.polyfit(time, exp.objects[obj.distance - 2].reflection[start_time:end_time], 3))
    pm1 = np.poly1d(np.polyfit(time, exp.objects[obj.distance - 1].reflection[start_time:end_time], 3))
    p = np.poly1d(np.polyfit(time, obj.reflection[start_time:end_time], 3))
    pp1 = np.poly1d(np.polyfit(time, exp.objects[obj.distance + 1].reflection[start_time:end_time], 3))
    pp2 = np.poly1d(np.polyfit(time, exp.objects[obj.distance + 2].reflection[start_time:end_time], 3))


    count = 0
    for j in range(start_time, end_time):

        l_space = abs(obj.reflection[j] - obj.normal_reflection) / space
        lm1_space = abs(exp.objects[obj.distance - 1].reflection[j] - exp.objects[obj.distance - 1].normal_reflection) / space
        lm2_space = abs(exp.objects[obj.distance - 2].reflection[j] - exp.objects[obj.distance - 2].normal_reflection) / space
        lp1_space = abs(exp.objects[obj.distance + 1].reflection[j] - exp.objects[obj.distance + 1].normal_reflection) / space
        lp2_space = abs(exp.objects[obj.distance + 2].reflection[j] - exp.objects[obj.distance + 2].normal_reflection) / space

        #for extremas values: [distance_to_last_max, time_since_last_max, distance_to_last_min, time_to_last_min]
        l = [obj.reflection[j], l_space, abs(obj.reflection[j] - p(time)[count]), obj.distance, obj.normal_reflection] #add the extremas
        l_one_closer = [exp.objects[obj.distance - 1].reflection[j], lm1_space, abs(exp.objects[obj.distance - 1].reflection[j] - pm1(time)[count]), exp.objects[obj.distance - 1].distance, exp.objects[obj.distance - 1].normal_reflection]
        l_two_closer = [exp.objects[obj.distance - 2].reflection[j], lm2_space, abs(exp.objects[obj.distance - 2].reflection[j] - pm2(time)[count]), exp.objects[obj.distance - 2].distance, exp.objects[obj.distance - 2].normal_reflection]
        l_one_further = [exp.objects[obj.distance + 1].reflection[j], lp1_space, abs(exp.objects[obj.distance + 1].reflection[j] - pp1(time)[count]), exp.objects[obj.distance + 1].distance, exp.objects[obj.distance + 1].normal_reflection]
        l_two_further = [exp.objects[obj.distance + 2].reflection[j], lp2_space, abs(exp.objects[obj.distance + 2].reflection[j] - pp2(time)[count]), exp.objects[obj.distance + 2].distance, exp.objects[obj.distance + 2].normal_reflection]

        # NOTE: small vectors... just the space difference from regular, and the distance of the bin
        # l = [l_space, obj.distance] #add the extremas
        # l_one_closer = [lm1_space, exp.objects[obj.distance - 1].distance]
        # l_two_closer = [lm2_space, exp.objects[obj.distance - 2].distance]
        # l_one_further = [lp1_space, exp.objects[obj.distance + 1].distance]
        # l_two_further = [lp2_space, exp.objects[obj.distance + 2].distance]


        full_vector = np.array(l_two_closer + l_one_closer + l + l_one_further + l_two_further)
        window.append(full_vector)
        count+=1
    return np.array(window)

# def generateSample(exp, obj, start_time, end_time, fire, flat):
#     if flat:
#         return generateFlatSample(exp, obj, start_time, end_time)
#     window = []
#
#     space = exp.max_reflection - exp.min_reflection
#
#     time = np.linspace(0, 1, (end_time - start_time))
#     pm2 = np.poly1d(np.polyfit(time, exp.objects[obj.distance - 2].reflection[start_time:end_time], 3))
#     pm1 = np.poly1d(np.polyfit(time, exp.objects[obj.distance - 1].reflection[start_time:end_time], 3))
#     p = np.poly1d(np.polyfit(time, obj.reflection[start_time:end_time], 3))
#     pp1 = np.poly1d(np.polyfit(time, exp.objects[obj.distance + 1].reflection[start_time:end_time], 3))
#     pp2 = np.poly1d(np.polyfit(time, exp.objects[obj.distance + 2].reflection[start_time:end_time], 3))
#
#
#     count = 0
#     for j in range(start_time, end_time):
#         if count == 0:
#             Extremas_m2 = getFirstExtremas(exp, exp.objects[obj.distance - 2], start_time, j, end_time)
#             Extremas_m1 = getFirstExtremas(exp, exp.objects[obj.distance - 1], start_time, j, end_time)
#             Extremas = getFirstExtremas(exp, obj, start_time, j, end_time)
#             Extremas_p1 = getFirstExtremas(exp, exp.objects[obj.distance + 1], start_time, j, end_time)
#             Extremas_p2 = getFirstExtremas(exp, exp.objects[obj.distance + 2], start_time, j, end_time)
#         else:
#             Extremas_m2 = updateExtrema(Extremas_m2, exp.objects[obj.distance - 2], j, start_time, end_time)
#             Extremas_m1 = updateExtrema(Extremas_m1, exp.objects[obj.distance - 1], j, start_time, end_time)
#             Extremas = updateExtrema(Extremas, obj, j, start_time, end_time)
#             Extremas_p1 = updateExtrema(Extremas_p1, exp.objects[obj.distance + 1], j, start_time, end_time)
#             Extremas_p2 = updateExtrema(Extremas_p2, exp.objects[obj.distance + 2], j, start_time, end_time)
#
#
#         # print('Time: ', j, " object: ", obj)
#         # print('Current', obj.reflection[j])
#         # print("Extremas: last val: ", Extremas.last_extrema, "    last time: ", Extremas.last_extrema_time, "    next val: ", Extremas.next_extrema, "    next time: ", Extremas.next_extrema_time)
#         # print()
#
#         l_space = abs(obj.reflection[j] - obj.normal_reflection) / space
#         lm1_space = abs(exp.objects[obj.distance - 1].reflection[j] - exp.objects[obj.distance - 1].normal_reflection) / space
#         lm2_space = abs(exp.objects[obj.distance - 2].reflection[j] - exp.objects[obj.distance - 2].normal_reflection) / space
#         lp1_space = abs(exp.objects[obj.distance + 1].reflection[j] - exp.objects[obj.distance + 1].normal_reflection) / space
#         lp2_space = abs(exp.objects[obj.distance + 2].reflection[j] - exp.objects[obj.distance + 2].normal_reflection) / space
#
#         #for extremas values: [distance_to_last_max, time_since_last_max, distance_to_last_min, time_to_last_min]
#         l = [obj.reflection[j], l_space, abs(obj.reflection[j] - p(time)[count]), obj.distance, obj.normal_reflection, \
#                         abs(Extremas.last_extrema - obj.reflection[j]), Extremas.last_extrema_time, abs(Extremas.next_extrema - obj.reflection[j]), Extremas.next_extrema_time] #add the extremas
#         l_one_closer = [exp.objects[obj.distance - 1].reflection[j], lm1_space, abs(exp.objects[obj.distance - 1].reflection[j] - pm1(time)[count]), exp.objects[obj.distance - 1].distance, exp.objects[obj.distance - 1].normal_reflection, \
#                         abs(Extremas_m1.last_extrema - exp.objects[obj.distance - 1].reflection[j]), Extremas_m1.last_extrema_time, abs(Extremas_m1.next_extrema - exp.objects[obj.distance - 1].reflection[j]), Extremas_m1.next_extrema_time]
#         l_two_closer = [exp.objects[obj.distance - 2].reflection[j], lm2_space, abs(exp.objects[obj.distance - 2].reflection[j] - pm2(time)[count]), exp.objects[obj.distance - 2].distance, exp.objects[obj.distance - 2].normal_reflection, \
#                         abs(Extremas_m2.last_extrema - exp.objects[obj.distance - 2].reflection[j]), Extremas_m2.last_extrema_time, abs(Extremas_m2.next_extrema - exp.objects[obj.distance - 2].reflection[j]), Extremas_m2.next_extrema_time]
#         l_one_further = [exp.objects[obj.distance + 1].reflection[j], lp1_space, abs(exp.objects[obj.distance + 1].reflection[j] - pp1(time)[count]), exp.objects[obj.distance + 1].distance, exp.objects[obj.distance + 1].normal_reflection, \
#                         abs(Extremas_p1.last_extrema - exp.objects[obj.distance + 1].reflection[j]), Extremas_p1.last_extrema_time, abs(Extremas_p1.next_extrema - exp.objects[obj.distance + 1].reflection[j]), Extremas_p1.next_extrema_time]
#         l_two_further = [exp.objects[obj.distance + 2].reflection[j], lp2_space, abs(exp.objects[obj.distance + 2].reflection[j] - pp2(time)[count]), exp.objects[obj.distance + 2].distance, exp.objects[obj.distance + 2].normal_reflection, \
#                         abs(Extremas_p2.last_extrema - exp.objects[obj.distance + 2].reflection[j]), Extremas_p2.last_extrema_time, abs(Extremas_p2.next_extrema - exp.objects[obj.distance + 2].reflection[j]), Extremas_p2.next_extrema_time]
#
#
#         full_vector = np.array(l_two_closer + l_one_closer + l + l_one_further + l_two_further)
#         window.append(full_vector)
#         count+=1
#     return np.array(window)

def makeZeroSample(window_size):
    arr = []
    for i in range(window_size):
        sample = []
        for j in range(15):
            sample.append(0)
        arr.append(np.array(sample))
    return np.array(arr)

def getVariance(lst, fft_bool=False):

    lst = np.array(lst)

    if fft_bool:
        degree = 1
        lst_fft = np.absolute(np.fft.fft(lst))
        fft = np.divide(lst_fft[1:], lst_fft[0])
        lst = fft[5:150]
        x_axis_fft = list(range(len(lst)))
    else:
        degree = 8

# this gets the signal variance (percentage of signal strength instead of total number)
# fires are around 0.10 < fire < 0.17
    time = np.linspace(0, len(lst), len(lst))
    p = np.poly1d(np.polyfit(time, lst, degree))
    mean = p(time)

    variance_total = 0
    # cur_max = 0
    for count, f in enumerate(lst):
        if fft_bool:
            variance_total+= abs(f - mean[count])
        else:
            cur = (abs(f - mean[count]) / f)
            variance_total+= cur
            # if cur > cur_max:
            #     cur_max = cur

    # cur_max = np.amax(np.absolute(np.gradient(lst[::2]))) / np.mean(np.absolute(np.gradient(lst[::2])))
    cur_max = max([ np.amax(np.absolute(np.diff(lst))) / np.mean(np.absolute(np.diff(lst))), np.amax(np.absolute(np.diff(lst[::2]))) / np.mean(np.absolute(np.diff(lst[::2])))])

    return variance_total/(lst.shape[0]), cur_max, mean


# this gets the fft variance
    # lst_fft = np.absolute(np.fft.fft(lst))
    # fft = np.divide(lst_fft[1:], lst_fft[0])
    # fft = fft[2:150]
    #
    # time = np.linspace(0, len(fft), len(fft))
    # p = np.poly1d(np.polyfit(time, fft, 8))
    # mean = p(time)
    #
    # variance_total = 0
    # for fft_count, f in enumerate(fft):
    #     variance_total+= abs(f - mean[fft_count])
    #
    # return variance_total/(fft.shape[0])

def getSlope(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[1:150]

    time = np.linspace(0, len(fft), len(fft))
    fft_p = np.poly1d(np.polyfit(time, fft, 1))
    slope = fft_p[1]
    return slope

def getRandSigma(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[1:150]

    time = np.linspace(0, len(fft), len(fft))
    fft_p = np.poly1d(np.polyfit(time, fft, 1))
    fft_linear_fit = fft_p(time)

    # sigma^2
    sum = 0
    for c in lst:
        sum+=(((c - np.mean(lst)) / c)**2)
    sigma_squared = sum / lst.shape[0]

    s_tot = 0
    s_res = 0
    for fft_count, f in enumerate(fft):
        s_tot += ((f - np.mean(fft))**2)
        s_res += ((f - fft_linear_fit[fft_count])**2)
    R_squared = (1 - (s_res / s_tot))

    return R_squared, sigma_squared


def getPeriodicy(lst, fit):
    x = np.linspace(0, len(lst), len(lst))

    if lst[0] > fit[0]:
        prev_max = "lst"
        cur_max = "lst"
    else:
        prev_max = "fit"
        cur_max = "fit"

    crosses = 0
    for i, val in enumerate(x):
        prev_max = cur_max
        if lst[i] > fit[i]:
            cur_max = "lst"
        if fit[i] > lst[i]:
            cur_max = "fit"
        if prev_max != cur_max:
            crosses+=1

    return crosses



def getFFTAvg(lst):
    lst = np.array(lst)
    lst_fft = np.absolute(np.fft.fft(lst))
    fft = np.divide(lst_fft[1:], lst_fft[0])
    fft = fft[2:150]

    max_idx = argrelextrema(fft, np.greater)
    min_idx = argrelextrema(fft, np.less)
    maxs = fft[argrelextrema(fft, np.greater)[0]]
    mins = fft[argrelextrema(fft, np.less)[0]]

    try:
        if max_idx[0][0] < min_idx[0][0]:
            a = maxs
            b = mins
        else:
            a = mins
            b = maxs

        c = np.empty((a.size + b.size,), dtype=a.dtype)
        c[0::2] = a
        c[1::2] = b

    except:
        if max_idx[0].size == 0:
            c = mins
        elif min_idx[0].size == 0:
            c = maxs
        else:
            c = np.array([])

    total_diff = 0
    count = 0
    for key, val in enumerate(c):
        try:
            total_diff+=abs(val - c[key+1])
            count+=1
        except:
            pass

    try:
        avg = total_diff/count
    except:
        avg = 0
    return avg

def burningExperiment(exp, obj, flat, leave_one_out_bool, hybrid):
    global min_slope
    global max_slope

    fire_samples = []
    nonfire_samples = []
    skipped_samples = []
    hybrid_fire, hybrid_nofire = [], []
    start_time = 0
    end_time = 300 #300 #150
    step = 10
    count = 0
    # print(obj.fire_start)
    while end_time < len(obj.reflection):
        sample = []

        if not leave_one_out_bool: #training

            # if its hybrid, then get the samples before
            if hybrid:
                if count > 0:
                    # print('count > 0')
                    if end_time > obj.fire_start and start_time < (obj.fire_start + step):
                        # print(end_time, " > ", obj.fire_start, " and ", start_time, " < ", (obj.fire_start + step))
                        count+=1
                        end_time+=step
                        start_time+=step
                        continue
                    else:
                        if checkThresholds(obj.reflection[start_time:end_time]):
                            if start_time < obj.fire_start: #change to this to see how the false alarms decrease
                                fire = False
                                nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False, flat))
                                hybrid_nofire.append(generateSample(exp, obj, (start_time - step), (end_time - step), False, flat))

                            # if hybrid, this doesnt include the "drop" from cutting the person
                            elif (start_time - (2 * step)) > obj.fire_start:
                                fire = True
                                fire_samples.append(generateSample(exp, obj, start_time, end_time, True, flat))
                                hybrid_fire.append(generateSample(exp, obj, (start_time - step), (end_time - step), True, flat))
            else:
                if end_time > obj.fire_start and start_time < obj.fire_start:
                    count+=1
                    end_time+=step
                    start_time+=step
                    continue
                else:
                    if start_time < obj.fire_start: #change to this to see how the false alarms decrease
                    # if end_time < obj.fire_start:
                        fire = False
                        nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False, flat))
                    else:
                        fire = True
                        fire_samples.append(generateSample(exp, obj, start_time, end_time, True, flat))
        else:
            # if its hybrid, then get the samples before
            if hybrid:
                if count > 0:
                    if checkThresholds(obj.reflection[start_time:end_time]):
                        if start_time < obj.fire_start: #change to this to see how the false alarms decrease
                            # print("Not a fire yet")
                            fire = False
                            nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False, flat))
                            hybrid_nofire.append(generateSample(exp, obj, (start_time - step), (end_time - step), False, flat))

                        # if hybrid, this doesnt include the "drop" from cutting the persoon
                        # elif (start_time - (2 * step)) > obj.fire_start:
                        else:
                            # print("         now a fire")
                            fire = True
                            fire_samples.append(generateSample(exp, obj, start_time, end_time, True, flat))
                            hybrid_fire.append(generateSample(exp, obj, (start_time - step), (end_time - step), True, flat))
                    else:
                        skipped_samples.append(int(start_time/10))
            else:
                if start_time < obj.fire_start: #change to this to see how the false alarms decrease
                # if end_time < obj.fire_start:
                    fire = False
                    nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False, flat))
                else:
                    fire = True
                    fire_samples.append(generateSample(exp, obj, start_time, end_time, True, flat))


        count+=1
        end_time+=step
        start_time+=step
        # print(obj.distance ,' count: ', count, ' start: ', start_time, ' end: ', end_time)

    if flat:
        return np.array(fire_samples), np.array(nonfire_samples)
    # print('returning samples')
    return fire_samples, nonfire_samples, skipped_samples, hybrid_fire, hybrid_nofire

def nonburningExperiment(exp, obj, passed_start_time, passed_end_time, flat, leave_one_out_bool, hybrid):
    nonfire_samples = []
    skipped_samples = []
    hybrid_nofire = []
    step = 10
    sample_size = 300

    # since the sample is being broken up for parallelizm, I need to change the start time.
    if passed_start_time == 0:
        start_time = 0
    else:
        start_time = (passed_start_time - sample_size) + step
    end_time = start_time + sample_size #150

    # print(obj.len_reflection)
    # print("obj: ", obj.distance, "start: ", start_time, " passed: ", passed_start_time)
    # print("obj: ", obj.distance, "end: ", end_time, " passed: ", passed_end_time)

    count = 0
    # while end_time < len(obj.reflection):
    while end_time <= passed_end_time and end_time <= obj.len_reflection:
        sample = []

        if not leave_one_out_bool: #training
            # if its hybrid, then get the samples before
            if hybrid:
                if start_time >= 10:
                    if checkThresholds(obj.reflection[start_time:end_time]):
                        nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False, flat))
                        hybrid_nofire.append(generateSample(exp, obj, (start_time - step), (end_time - step), False, flat))
            else:
                if average(obj.reflection[0:300]) > min(exp.top) or max(obj.reflection[start_time:end_time]) > min(exp.top):
                    nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False, flat))
        # leave one out
        else:
            if hybrid:
                if start_time >= 10:
                    if checkThresholds(obj.reflection[start_time:end_time]):
                        nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False, flat))
                        hybrid_nofire.append(generateSample(exp, obj, (start_time - step), (end_time - step), False, flat))
                    else:
                        skipped_samples.append(int(start_time/10))
            else:
                if True:
                    nonfire_samples.append(generateSample(exp, obj, start_time, end_time, False, flat))
                else:
                    skipped_samples.append(int(start_time/10))
        end_time+=step
        start_time+=step
        count+=1
    if flat:
        return np.array([]), np.array(nonfire_samples)

    return [], nonfire_samples, skipped_samples, [], hybrid_nofire

def checkThresholds(reflection_arr):
    variance, max_variance, signal_fit = getVariance(reflection_arr)
    fft_variance,_,_ = getVariance(reflection_arr, fft_bool=True)
    fft_slope = getSlope(reflection_arr)
    R_squared, sigma_squared = getRandSigma(reflection_arr)
    periodicy = getPeriodicy(reflection_arr, signal_fit)
    # if R_squared < R_SQUARED_MAX_THRESHOLD and R_squared > R_SQUARED_MIN_THRESHOLD and sigma_squared < SIG_SQUARED_MAX_THRESHOLD and sigma_squared > SIG_SQUARED_MIN_THRESHOLD:
    # if variance < SIGNAL_VARIANCE_MAX_THRESHOLD and variance > SIGNAL_VARIANCE_MIN_THRESHOLD and fft_slope < FFT_SLOPE_MAX_THRESHOLD and fft_variance < FFT_MAX_VARIANCE and fft_variance > FFT_MIN_VARIANCE and max(obj.reflection[start_time:end_time]) > MIN_REF_THRESH and periodicy > PERIODICY_MIN_THRESH:
    # if variance < SIGNAL_VARIANCE_MAX_THRESHOLD and variance > SIGNAL_VARIANCE_MIN_THRESHOLD and fft_slope < FFT_SLOPE_MAX_THRESHOLD and fft_variance < FFT_MAX_VARIANCE and fft_variance > FFT_MIN_VARIANCE: #rfire_v-1.h5
    if variance < SIGNAL_VARIANCE_MAX_THRESHOLD and variance > SIGNAL_VARIANCE_MIN_THRESHOLD and fft_slope < FFT_SLOPE_MAX_THRESHOLD and fft_variance < FFT_MAX_VARIANCE and fft_variance > FFT_MIN_VARIANCE and max_variance < SIGNAL_MVAR_MAX_THRESHOLD and periodicy > PERIODICY_MIN_THRESH: #new run
        return True
    return False

def average(lst):
    return sum(lst) / len(lst)

def makeSamples(experiment, object, start_time, end_time, flat, leave_one_out_bool=False, hybrid=False):
    if experiment.fire:
        return burningExperiment(experiment, object, flat, leave_one_out_bool, hybrid)
    else:
        return nonburningExperiment(experiment, object, start_time, end_time, flat, leave_one_out_bool, hybrid)
