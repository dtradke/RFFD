# RFire
# 11/16/19
#
# 1DCNN model for the second RFire algorithm, using a CNN. Good results. Models have been trained already using temporal samples.


# cnn model
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
from matplotlib import pyplot
from keras.models import Sequential
from keras.layers import Dense, GlobalAveragePooling1D
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.utils import to_categorical
from keras.models import load_model
import time
import process_dat
import sys

class FinalResult(object):

    def __init__(self, fire_distance=257, latency=10000, false_alarms=[], TP=0, TN=0, FP=0, FN=0):
        self.fire_distance = fire_distance
        self.latency = latency
        self.false_alarms = false_alarms
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "FinalResult(distance: {}, latency: {}, false alarms: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.fire_distance, self.latency, self.false_alarms, self.TP, self.TN, self.FP, self.FN)

class Result(object):

    def __init__(self, distance=0, fire=False, latency=10000, false_alarms=[], predicted_fire_time=10000, TP=0, TN=0, FP=0, FN=0):
        self.distance = distance
        self.fire = fire
        self.latency = latency
        self.false_alarms = false_alarms
        self.predicted_fire_time = predicted_fire_time
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "Result(distance: {}, latency: {}, false alarms: {}, predicted_fire_time: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.distance, self.latency, self.false_alarms, self.predicted_fire_time, self.TP, self.TN, self.FP, self.FN)

class Alarm(object):

    def __init__(self, distance=0, latency=False):
        self.distance = distance
        self.latency = latency

    def __repr__(self):
        return "Alarm(distance: {}, latency: {})".format(self.distance, self.latency)



# # real model
def train_model(trainX, trainy, epochs=1, batch_size=32):
    n_features = trainX.shape[1]
    n_samples = trainX.shape[0]

    model = Sequential()
    model.add(Dense(32, input_dim=n_features, activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(64, activation='relu'))
    # model.add(Dropout(0.25))
    # model.add(Dense(128, activation='relu'))
    # model.add(Dropout(0.25))
    # model.add(Dense(128, activation='relu'))
    # model.add(Dense(256, activation='relu'))
    # model.add(Dropout(0.25))
    # model.add(Dense(256, activation='relu'))
    # model.add(Dropout(0.25))
    # model.add(Dense(128, activation='relu'))
    # model.add(Dropout(0.25))
    # model.add(Dense(128, activation='relu'))
    # model.add(Dense(64, activation='relu'))
    # model.add(Dense(32, activation='relu'))
    model.add(Dense(2, activation='softmax'))
    print(model.summary())
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=1)
    return model




# summarize scores
def summarize_results(scores):
	print(scores)
	m, s = mean(scores), std(scores)
	print('Accuracy: %.3f%% (+/-%.3f)' % (m, s))

def findIfFire(y_ground):
    for i in y_ground:
        if i[0] < i[1]:
            return True
    return False

def alternativeTimeSeriesTest(y_test, y_pred, leave_one_out, ignition_sample):
    results = {}
    min_ignition_sample = 10000
    closest_distance = 0
    alarms = []


    for i in ignition_sample:
        if i < min_ignition_sample:
            min_ignition_sample = i


    print("Length of y_test: ", len(y_test))

    for idx, y_ground in enumerate(y_test):
        is_fire = findIfFire(y_ground)
        correct = 0
        incorrect = 0
        FN = 0
        FP = 0
        TP = 0
        TN = 0
        FN_before_fire = 0
        fire_flag = False
        early_classification = 0
        false_alarms = []
        real_false_alarm = 0
        ones_in_a_row = 0
        latency = 10000
        failed_detection = []
        predicted_fire_time = 10000

        for i, val in enumerate(y_ground): #actual values
            print(i, ' prediction: ', y_pred[idx][i], ' ground: ', val)


            if y_pred[idx][i][1] > y_pred[idx][i][0]: #high confidence of fire
                ones_in_a_row += 1
                if not fire_flag:
                    if ones_in_a_row > 1:
                        predicted_fire_time = i
                        ones_in_a_row = 0
                        try:
                            if val[0] < val[1]:# or y_ground[i+5][0] < y_ground[i+5][1]:#abs(min_ignition_sample - i) <= 5: #if it actually is a fire
                                fire_flag = True
                                latency = (i - min_ignition_sample)
                                print("==== KNOWN FIRE BIN ====")
                                print("distance: ", idx + 5)
                                print("time: ", i)
                                # print("ignition_sample: ", min_ignition_sample)
                                print("Latency: ", latency, " second(s)")
                                alarms.append(Alarm(idx + 5, latency))
                            else: #fire detected but not a ground truth classified fire sample yet
                                if i < min_ignition_sample - 2:
                                    print("==== False Alarm at time ", i, " ====")
                                    false_alarms.append(i)
                                else:
                                    fire_flag = True
                                    latency = (i - min_ignition_sample)
                                    print("==== PREDICTED FIRE ====")
                                    print("distance: ", idx + 5)
                                    print("time: ", i)
                                    # print("ignition_sample: ", min_ignition_sample)
                                    print("Latency: ", latency, " second(s)")
                                    alarms.append(Alarm(idx + 5, latency))
                        except:
                            print("---there was an error here---")
                            pass
                if val[0] < val[1]: #actual fire
                    correct += 1
                    TP += 1
                else: # actual no fire
                    incorrect += 1
                    FP += 1
            else:
                ones_in_a_row = 0
                if val[0] > val[1]: #actual no fire
                    correct += 1
                    TN += 1
                else: #actual fire
                    incorrect += 1
                    FN += 1
                    # failed_detection.append(i)
                    if not fire_flag:
                        FN_before_fire += 1




            # if y_pred[idx][i][0] > y_pred[idx][i][1]: #predicted no fire
            #     ones_in_a_row = 0
            #     if val[0] > val[1]: #actual no fire
            #         correct += 1
            #         TN += 1
            #     else: #actual fire
            #         incorrect += 1
            #         FN += 1
            #         # failed_detection.append(i)
            #         if not fire_flag:
            #             FN_before_fire += 1
            # else: # predicted fire
            #
            #     ones_in_a_row += 1
            #     if not fire_flag: #if it is a leave_one_out test and a fire hasn't been detected
            #         if ones_in_a_row > 2:
            #             predicted_fire_time = i
            #             ones_in_a_row = 0
            #             # if abs(min_ignition_sample - i) > 6:
            #             try:
            #                 if val[0] < val[1]:# or y_ground[i+5][0] < y_ground[i+5][1]:#abs(min_ignition_sample - i) <= 5: #if it actually is a fire
            #                     fire_flag = True
            #                     latency = (i - min_ignition_sample)
            #                     print("==== KNOWN FIRE BIN ====")
            #                     print("distance: ", idx + 5)
            #                     print("time: ", i)
            #                     # print("ignition_sample: ", min_ignition_sample)
            #                     print("Latency: ", latency, " second(s)")
            #                     alarms.append(Alarm(idx + 5, latency))
            #                 else: #fire detected but not a ground truth classified fire sample yet
            #                     if i < min_ignition_sample - 2:
            #                         print("==== False Alarm at time ", i, " ====")
            #                         false_alarms.append(i)
            #                     else:
            #                         fire_flag = True
            #                         latency = (i - min_ignition_sample)
            #                         print("==== KNOWN FIRE BIN ====")
            #                         print("distance: ", idx + 5)
            #                         print("time: ", i)
            #                         # print("ignition_sample: ", min_ignition_sample)
            #                         print("Latency: ", latency, " second(s)")
            #                         alarms.append(Alarm(idx + 5, latency))
            #             except:
            #                 print("---there was an error here---")
            #                 pass
            #
            #     if val[0] < val[1]: #actual fire
            #         correct += 1
            #         TP += 1
            #     else: # actual no fire
            #         incorrect += 1
            #         FP += 1

        print("Distance: ", idx + 5)
        print("False Alarms: ", false_alarms)
        # print("Failed detection: ", failed_detection)

        results[idx] = Result(distance=(idx + 5), fire=is_fire, latency=latency, false_alarms=false_alarms, predicted_fire_time=predicted_fire_time, TP=TP, TN=TN, FP=FP, FN=FN)

    min_fire_distance = 257
    min_latency = 10000

    for a in alarms:
        print(a)
        if a.distance < min_fire_distance:
            min_fire_distance = a.distance
        if a.latency < min_latency:
            min_latency = a.latency


    # latencies = []
    false_alarm_list = []

    for idx, r in enumerate(results.keys()):
        false_alarm_list = false_alarm_list + results[r].false_alarms

    for r in results.keys():
        if predicted_fire_time < 10000:
            closest_predicted_fire = results[r]
            break
    for r in results.keys():
        if results[r].fire:
            closest_known_fire = results[r]
            break

    my_set = set(false_alarm_list)
    unique_false_alarms = list(my_set)

    unique_false_alarms = lessThanIgnition(unique_false_alarms, min_ignition_sample)

    print("Fire starts at: ", min_ignition_sample)
    print("Fire detected at bin: ", min_fire_distance)
    print("Distance (meters):    ", min_fire_distance*.044)
    print("Fire latency:         ", min_latency)
    print("Unique false alarms:  ", unique_false_alarms)
    try:
        print("Actual Closest Fire:    ", closest_known_fire)
        print("Closest predicted fire: ", closest_predicted_fire)
    except:
        print("=== No fire detected ===")

    final_result = FinalResult(min_fire_distance, min_latency, unique_false_alarms)
    return final_result

def lessThanIgnition(false_alarms, ignition):
    real_false_alarms = []
    for i in false_alarms:
        if i < ignition:
            real_false_alarms.append(i)
    return real_false_alarms




def timeSeriesTest(testY, y, leave_one_out, ignition_sample):
    real_fire = 0
    real_nonfire = 0
    if type(testY) == list:
        return alternativeTimeSeriesTest(testY, y, leave_one_out, ignition_sample)

    correct = 0
    incorrect = 0
    FN = 0
    FP = 0
    TP = 0
    TN = 0
    FN_before_fire = 0
    early_classification = 0
    false_alarm = 0
    for i, val in enumerate(testY):
        if y[i][0] > y[i][1]: #predicted no fire
            if val[0] > val[1]: #actual no fire
                correct += 1
                TN += 1
                real_nonfire+=1
            else: #actual fire
                incorrect += 1
                FN += 1
                real_fire+=1
        else: # predicted fire
            if val[0] < val[1]: #actual fire
                correct += 1
                TP += 1
                real_fire+=1
            else: # actual no fire
                incorrect += 1
                FP += 1
                real_nonfire+=1

    # print("Correct: ", correct)
    # print("incorrect: ", incorrect)
    # print("correct perc: ", correct/len(testY))
    print("TP: ", TP)
    print("TN: ", TN)
    print("FP (annoying) - no fire but predicted fire:     ", FP)
    print("FN (bad) - actual fire but not predicted fire:  ", FN)
    print("Amount of fire samples: ", real_fire)
    print("Amount of nonfire samples: ", real_nonfire)
    # print('Early Classification: ', early_classification)
    # print('False Alarms: ', false_alarm)
    return Result(distance=0, fire=False, latency=0, false_alarms=[], TP=TP, TN=TN, FP=FP, FN=FN)

def getIgnitionSample(leave_one_out, y_test):
    ignitions = []
    if leave_one_out is not None:
        print("LEAVE ONE OUT: ", leave_one_out)
        for i, val in enumerate(y_test):
            for j, value in enumerate(val):
                if value[0] < value[1]:
                    ignitions.append(j)
                    break
            else:
                ignitions.append(1000000)
        return ignitions
    else:
        return 0

def getModel(leave_one_out, model, X, y_train):
    if model is None:
        epochs = 50
        print("Training with: ", X.shape)
        model = train_model(X, y_train, epochs=epochs)
        # time_string = time.strftime("%Y%m%d-%H%M%S")
        # if leave_one_out is not None:
        #     fname = 'CNN_models/' + time_string + '_' + str(epochs) + 'epochs_leaveoneout.h5'
        # else:
        #     fname = 'CNN_models/' + time_string + '_' + str(epochs) + 'epochs.h5'
        # model.save(fname)
    return model


# run an experiment
def run_experiment(leave_one_out, model=None, repeats=1):
    full_TP = 0
    full_TN = 0
    full_FP = 0
    full_FN = 0
    total_latency = 0
    total_false_alarms = 0
    total_distance = 0
    for a in range(repeats):
        model = None
        X, y_train, X_test, y_test, leave_one_out_fires = process_dat.getRangeProfileSamples(cnn=True, leave_one_out=leave_one_out, flat=True)

        if type(X_test) == list:
            print("Testing with: ", X_test[0].shape)
        else:
            print("Testing with: ", X_test.shape)


        ignition_sample = getIgnitionSample(leave_one_out, y_test)
        model = getModel(leave_one_out, model, X, y_train)

        y_preds = []
        if type(X_test) == list:
            for count, x in enumerate(X_test):
                print("Predicting bin: ", count+5)
                y_p = model.predict(x)
                y_preds.append(y_p)
        else:
            y_preds = model.predict(X_test)
        final_result = timeSeriesTest(y_test, y_preds, leave_one_out, ignition_sample)

        # real_fire_distances = []
        # for o in leave_one_out_fires:
        #     real_fire_distances.append(o.distance)

        if type(X_test) == list:
            if final_result.fire_distance in leave_one_out_fires:
                print("Correct fire classification")
            else:
                print("Fire detected at bin ", final_result.fire_distance, " when it should be at ", min(leave_one_out_fires))
            total_distance+=final_result.fire_distance
            total_latency+=final_result.latency
            total_false_alarms+=len(final_result.false_alarms)
        else:
            full_TP+=final_result.TP
            full_TN+=final_result.TN
            full_FN+=final_result.FN
            full_FP+=final_result.FP

    if type(X_test) == list:
        print("Average Bin:               ", total_distance/repeats)
        print("Average Distance (meters): ", (total_distance*(0.044))/repeats)
        print("Known fire bins:           ", leave_one_out_fires)
        print("Average Latency:           ", total_latency/repeats)
        print("Average False Alarms:      ", total_false_alarms/repeats)
    else:
        print("Total TP: ", full_TP)
        print("Total TN: ", full_TN)
        print("Total FP: ", full_FP)
        print("Total FN: ", full_FN)
        print("Samples: ", X_test.shape[0])
        fires = 0
        for i in y_test:
            if i[0] < i[1]:
                fires+=1
        print("Fires: ", fires)
        print("Average TP: ", (100 * (full_TP/repeats)/fires), " %")
        print("Average TN: ", (100 * (full_TN/repeats)/(X_test.shape[0] - fires)), " %")
        print("Misclassified percentages:")
        print("Average FP: ", (100 * (full_FP/repeats)/(X_test.shape[0] - fires)), " %")
        print("Average FN: ", (100 * (full_FN/repeats)/fires), " %")
    print("Finished")


if __name__ == '__main__':
    # if len(sys.argv) == 2:
    #     mod_str = sys.argv[len(sys.argv) - 1]
    #     mod = load_model('models/' + mod_str)
    #     run_experiment(mod)
    # else:
    #     run_experiment()

    if len(sys.argv) == 2:
        leave_one_out = sys.argv[1]
        mod = None
    elif len(sys.argv) == 3:
        leave_one_out = sys.argv[1]
        mod = sys.argv[2]
    else:
        leave_one_out = None
        mod = None

    run_experiment(leave_one_out, mod)
