# RFire
# 11/16/19
#
# New way to look at "bins" from the mmWave board.
# To run if one file: python3 process_dat.py [optional leave one out file]
# All data in folders in 'data/' after parser has decoded them

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
from os import listdir
import object_factory
import sample_factory
import util

augment = False

#loads the data into pandas dataframe, deletes the last 5 lines to avoid anomalies
def loadData(fname=None, dirname=None):
    try:
        if dirname is None:
            path = '../../../../home/dtradke/rfire_data/data/' + fname + '/'+ fname + '.csv'
        else:
            path = '../../../../home/dtradke/rfire_data/data/' + dirname + '/' + fname + '/'+ fname + '.csv'
        print("On local disk")
    except:
        if dirname is None:
            path = '../../rfire_data/data/' + fname + '/'+ fname + '.csv'
        else:
            path = '../../rfire_data/data/' + dirname + '/' + fname + '/'+ fname + '.csv'
        print("On shared disk")
    data = pd.read_csv(path, index_col=False)
    print(path, " shape: ", data.shape)
    return data.iloc[:-5]

def cutFat(df, model):
    beginning = True
    start = 400000
    step = 50000 #250000#200000 #400000
    offset = 0

    df = df.iloc[:-30] # was -80

    cut_amt = 50000
    if df.shape[0] > cut_amt:
        df = df.iloc[:cut_amt] #added for the long ones


    if model is not None:
        if beginning:
            df = df.iloc[offset:(offset + step)]
        else:
            baseline = df.iloc[:600]
            print("From: ", (start + offset), " to: ", (start+step+offset), " in: ", df.shape)
            df = df.iloc[(start + offset):(start+step+offset)] #(start+step)
            print("df.shape: ", df.shape)
            df = pd.concat([baseline, df], axis=0)
        rows = len(df)
        seconds = rows//10
        minutes = seconds//60
        hours = minutes//60
        hour_remainder = minutes%60
        print("This is a total of: ", hours, " hours and ", hour_remainder, " minutes, shape: ", df.shape)
    return df

def getDF(path, s):
    flag = False
    for idx, f in enumerate(sorted(listdir(path + s))):
        if f[0] is not '_':
            if f[0] is not '.':
                # print(f[0])
                returned_df = loadData(f, s)
                if flag == False:
                    flag = True
                    df = returned_df
                    continue

                df = pd.concat([df, returned_df])

    return df


def getRangeProfileSamples(cnn=False, leave_one_out=None, svm=False, flat=False, model=None, hybrid=False):
    try:
        scenarios = listdir('../../../../home/dtradke/rfire_data/data/')
    except:
        scenarios = listdir('../../rfire_data/data/')
    objects = []
    experiments = []

    for s in scenarios:

        if model is not None and s != leave_one_out:
            # print('Testing... Skip: ', s)
            continue

        #Skipping wood fires
        split_scen = s.split('_')
        if 'fire' in split_scen:
            # if ('big' in split_scen or 'soot1' in split_scen or 'soot2' in split_scen or 'nonsoot' in split_scen or 'foam' in split_scen):
            if s != leave_one_out:
                # if ('big' not in split_scen and s != leave_one_out) or (s == 'big_soot1_fire_185cm_device_598cm' and s != leave_one_out) or (s == 'big_soot1_fire_385cm_device_598cm' and s != leave_one_out):
                # if 'tim' in split_scen or 'occlusion' in split_scen: #for sample-wise
                if 'big' not in split_scen: #all big fires
                    print("Skipping: ", s)
                    continue
        else:
            if 'test' == split_scen[0] and s != leave_one_out:
            # if 'test' == split_scen[0] and 'office' == split_scen[1] and s != leave_one_out:
                # if 'networks' == split_scen[1]:
                if 'QP' in split_scen:
                    print("Skipping: ", s)
                    continue
        # if 'fire' not in split_scen or 'big' not in split_scen or '85cm' not in split_scen or 'soot1' not in split_scen:
        #     continue

        if s[0] is not '_':
            if s[0] is not 'p':
                print("Loading: ", s)
                try:
                    df = loadData(s)
                except:
                    try:
                        df = getDF('../../../../home/dtradke/rfire_data/data/', s)
                    except:
                        df = getDF('../../rfire_data/data/', s)


                    df = df.reset_index(drop=True)

                df = cutFat(df, model)
                print("Cut to: ", df.shape)

                # new_objects = object_factory.makeObjects(df, s, leave_one_out)
                # objects = objects + new_objects
                experiments.append(object_factory.makeObjects(df, s, leave_one_out))

    util.calibrateExperiments(experiments, leave_one_out) #moved below augment

    new_experiments = []
    if augment:
        new_experiments = util.augmentExperiments(experiments, leave_one_out)
    experiments = experiments + new_experiments

    # for e in experiments:
    #     print(e)
    #     objs = e.objects
    #     for o in objs:
    #         object = objs[o]
    #         print(object)
    # exit()

    dataset, y, second_X, X_fft, X_second_fft, leave_one_out_X, leave_one_out_y, second_loo_X, leave_one_out_X_fft, second_loo_X_fft, all_skipped_samples = util.makeDataset(experiments, cnn, leave_one_out, svm, flat, model, hybrid)

    print("Train FFT SHAPE: ", X_fft.shape)
    print(X_second_fft.shape)
    print("Test FFT SHAPE: ", leave_one_out_X_fft[list(leave_one_out_X_fft.keys())[0]].shape)
    print(second_loo_X_fft[list(second_loo_X_fft.keys())[0]].shape)

    if svm:
        return dataset, 0, y, 0

    if leave_one_out is not None:
        for e in experiments:
            if e.name == leave_one_out:
                leave_one_out_experiment = e
                fire_start = 100000000
                for o in e.objects.keys():
                    obj = e.objects[o]
                    if obj.fire_start < fire_start and (obj.fire_start > 580):
                        fire_start = obj.fire_start
                fire_start = int(fire_start / 10)
                break
        print("Fire starting at: ", fire_start)
        return dataset, y, second_X, X_fft, X_second_fft, leave_one_out_X, leave_one_out_y, second_loo_X, leave_one_out_X_fft, second_loo_X_fft, leave_one_out_experiment.fire_bin_list, all_skipped_samples, fire_start

    else:
        X_test = dataset[int((len(dataset)) * .75):]
        y_test = y[int((len(y)) * .75):]
        X_test_second = second_X[int((len(second_X)) * .75):]
        X_train = dataset[:int((len(dataset)) * .75)]
        y_train = y[:int((len(y)) * .75)]
        X_train_second = second_X[:int((len(second_X)) * .75)]
        return X_train, y_train, X_test_second, X_test, y_test, X_test_second, [], all_skipped_samples

# NOTE: TO RUN - python3 process_dat.py [optional fire to be left out]
# main()
