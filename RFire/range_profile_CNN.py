# RFire
# 11/16/19
#
# 1DCNN model for the second RFire algorithm, using a CNN. Good results. Models have been trained already using temporal samples.


# cnn model
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
from matplotlib import pyplot
from keras.models import Sequential
from keras.layers import Dense, GlobalAveragePooling1D, Bidirectional, TimeDistributed, LSTM
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.utils import to_categorical
from keras.models import load_model
from keras.optimizers import SGD
import time
import process_dat
import sys
import pickle


def make_keras_picklable():
    def __getstate__(self):
        model_str = ""
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            keras.models.save_model(self, fd.name, overwrite=True)
            model_str = fd.read()
        d = { 'model_str': model_str }
        return d

    def __setstate__(self, state):
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            fd.write(state['model_str'])
            fd.flush()
            model = keras.models.load_model(fd.name)
        self.__dict__ = model.__dict__


    cls = keras.models.Model
    cls.__getstate__ = __getstate__
    cls.__setstate__ = __setstate__



class FinalResult(object):

    def __init__(self, fire_distance=257, latency=10000, false_alarms=[], TP=0, TN=0, FP=0, FN=0):
        self.fire_distance = fire_distance
        self.latency = latency
        self.false_alarms = false_alarms
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "FinalResult(distance: {}, latency: {}, false alarms: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.fire_distance, self.latency, self.false_alarms, self.TP, self.TN, self.FP, self.FN)

class Result(object):

    def __init__(self, distance=0, fire=False, latency=10000, false_alarms=[], predicted_fire_time=10000, TP=0, TN=0, FP=0, FN=0):
        self.distance = distance
        self.fire = fire
        self.latency = latency
        self.false_alarms = false_alarms
        self.predicted_fire_time = predicted_fire_time
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def __repr__(self):
        return "Result(distance: {}, latency: {}, false alarms: {}, predicted_fire_time: {}, TP: {}, TN: {}, FP: {}, FN: {})".format(self.distance, self.latency, self.false_alarms, self.predicted_fire_time, self.TP, self.TN, self.FP, self.FN)

class Alarm(object):

    def __init__(self, distance=0, time=10000, fire=False, latency=10000):
        self.distance = distance
        self.time = time
        self.fire = fire
        self.latency = latency

    def __repr__(self):
        if self.fire:
            return "Alarm(distance: {}, time: {}, fire: {}, latency: {})".format(self.distance, self.time, self.fire, self.latency)
        else:
            return "Alarm(distance: {}, time: {}, fire: {})".format(self.distance, self.time, self.fire)

class RealAlarm(object):

    def __init__(self, distance=0, time=10000, fire=False, latency=10000):
        self.distance = distance
        self.time = time
        self.fire = fire
        self.latency = latency

    def __repr__(self):
        if self.fire:
            return "RealAlarm(distance: {}, time: {}, fire: {}, latency: {})".format(self.distance, self.time, self.fire, self.latency)
        else:
            return "RealAlarm(distance: {}, time: {}, fire: {})".format(self.distance, self.time, self.fire)



# TODO: Maybe concat the output of this input/with an autoencoder to the flattened data about percentage of fluct

# NOTE: maybe concat an autoencoder output of the cnn to some position vector, kind of like the flattened data

# bidirectional LSTM
# def train_model(trainX, trainy, epochs=1, batch_size=500):
#     verbose, epochs, batch_size = 0, epochs, 32#500#32
#     n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]
#     trainy = trainy.reshape(1, n_timesteps, 1)
#     model = Sequential()
#     model.add(Bidirectional(LSTM(20, return_sequences=True), input_shape=(n_timesteps, n_features), merge_mode='concat'))
#     model.add(TimeDistributed(Dense(1, activation='sigmoid')))
#     model.compile(loss='binary_crossentropy', optimizer='adam')
#     model.fit(trainX, trainy, epochs=1, batch_size=batch_size, verbose=1) #verbose
#     return model

# play model
# def train_model(trainX, trainy, epochs=1, batch_size=500):
#     verbose, epochs, batch_size = 0, epochs, 32#500#32
#     n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]
#     model = Sequential()
#
#     #higher false alarms, lower missed detections
#     model.add(Conv1D(filters=64, kernel_size=10, activation='relu', input_shape=(n_timesteps,n_features)))
#     # model.add(Conv1D(filters=64, kernel_size=10, activation='relu'))
#     # model.add(Dropout(0.5))
#     model.add(Conv1D(filters=128, kernel_size=10, activation='relu'))
#     model.add(Dropout(0.5))
#     model.add(Conv1D(filters=128, kernel_size=10, activation='relu'))
#     # model.add(Conv1D(filters=256, kernel_size=10, activation='relu'))
#     # model.add(Dropout(0.5))
#     # model.add(Conv1D(filters=512, kernel_size=10, activation='relu'))
#
#     model.add(MaxPooling1D(pool_size=2))
#     model.add(Flatten())
#     model.add(Dense(64, activation='relu'))
#     model.add(Dense(n_outputs, activation='softmax'))
#     print(model.summary())
#     # exit()
#     # model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
#     opt = SGD(lr=0.01)
#     model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
#     model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=1) #verbose
#     return model


# CNN LSTM hybrid
def train_model(trainX, trainy, epochs=1, batch_size=500):
    verbose, epochs, batch_size = 0, epochs, 32#500#32
    n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]

    # trainX = trainX.reshape((trainX.shape[0], 1, n_timesteps, n_features))
    print(trainX)
    trainX = np.expand_dims(trainX, axis=1)
    print(trainX.shape)
    print(trainX)


    model = Sequential()

    #higher false alarms, lower missed detections
    model.add(TimeDistributed(Conv1D(filters=64, kernel_size=10, activation='relu', input_shape=(n_timesteps,n_features))))
    model.add(TimeDistributed(Conv1D(filters=128, kernel_size=10, activation='relu')))
    model.add(TimeDistributed(Dropout(0.5)))
    model.add(TimeDistributed(Conv1D(filters=128, kernel_size=10, activation='relu')))


    model.add(TimeDistributed(MaxPooling1D(pool_size=2)))
    model.add(TimeDistributed(Flatten()))
    model.add(LSTM(64, activation='relu'))
    model.add(Dense(n_outputs, activation='softmax'))
    # print(model.summary())
    # exit()
    # model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    opt = SGD(lr=0.01)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=1) #verbose
    return model




# summarize scores
def summarize_results(scores):
	print(scores)
	m, s = mean(scores), std(scores)
	print('Accuracy: %.3f%% (+/-%.3f)' % (m, s))

def findIfFire(y_ground):
    for i in y_ground:
        if i[0] < i[1]:
            return True
    return False

def alternativeTimeSeriesTest(y_test, y_pred, leave_one_out, min_ignition_sample, all_skipped_samples):
    results = {}
    # min_ignition_sample = 10000
    closest_distance = 0
    alarms = []


    # for i in ignition_sample:
    #     if i < min_ignition_sample:
    #         min_ignition_sample = i

# TAKE OUT WHEN CLASSIFICATION ISNT AT THE BEGINNING
    if min_ignition_sample < 10000:
        min_ignition_sample = min_ignition_sample - 30


    print("Length of y_test: ", len(y_test))
    print("Length of y_pred: ", len(y_pred))

    for idx, y_ground in enumerate(y_test):
        is_fire = findIfFire(y_ground)
        correct = 0
        incorrect = 0
        FN = 0
        FP = 0
        TP = 0
        TN = 0
        FN_before_fire = 0
        fire_flag = False
        early_classification = 0
        false_alarms = []
        real_false_alarm = 0
        ones_in_a_row = 0
        latency = 10000
        failed_detection = []
        predicted_fire_time = 10000
        pred_made = 0

        if y_pred[idx].size < 2:
            continue

        # print("Skipped: ", all_skipped_samples[idx])

        i = 0
        timestep = -1
        # for i, val in enumerate(y_ground): #actual values
        while pred_made < y_ground.shape[0]:
            val = y_ground[i]
            timestep+=1

            if timestep in all_skipped_samples[idx]:
                ones_in_a_row = 0
                continue

            pred_made+=1
            print(timestep, ' i: ', i, ' prediction: ', y_pred[idx][i], ' ground: ', val)

            if y_pred[idx][i][1] > 0.8: #high confidence of fire
                ones_in_a_row += 1

# Took this out to get multiple real fire alarms
                # if not fire_flag:
                if ones_in_a_row > 2:
                    predicted_fire_time = timestep #i
                    ones_in_a_row = 0
                    try:
                        if val[0] < val[1]:# or y_ground[i+5][0] < y_ground[i+5][1]:#abs(min_ignition_sample - i) <= 5: #if it actually is a fire
                            fire_flag = True
                            latency = (timestep - min_ignition_sample)
                            print("==== KNOWN FIRE BIN ====")
                            print("distance: ", idx + 4)
                            print("time: ", timestep)
                            # print("ignition_sample: ", min_ignition_sample)
                            print("Latency: ", latency, " second(s)")
                            alarms.append(Alarm(idx + 4, timestep, True, latency))
                        else: #fire detected but not a ground truth classified fire sample yet
                            if timestep < min_ignition_sample:
                                print("==== False Alarm at time ", timestep, " ====")
                                alarms.append(Alarm(idx + 4, timestep, False, 10000))
                                false_alarms.append(timestep)
                            else:
                                fire_flag = True
                                latency = (timestep - min_ignition_sample)
                                print("==== PREDICTED FIRE ====")
                                print("distance: ", idx + 4)
                                print("time: ", timestep)
                                # print("ignition_sample: ", min_ignition_sample)
                                print("Latency: ", latency, " second(s)")
                                alarms.append(Alarm(idx + 4, timestep, True, latency))
                    except:
                        print("---there was an error here---")
                        pass
                if val[0] < val[1]: #actual fire
                    correct += 1
                    TP += 1
                else: # actual no fire
                    incorrect += 1
                    FP += 1
            else: #not predicted fire
                ones_in_a_row = 0
                if val[0] > val[1]: #actual no fire
                    correct += 1
                    TN += 1
                else: #actual fire
                    incorrect += 1
                    FN += 1
                    # failed_detection.append(i)
                    if not fire_flag:
                        FN_before_fire += 1
            i+=1

        print("Distance: ", idx + 4)
        print("False Alarms: ", false_alarms)

        results[idx] = Result(distance=(idx + 4), fire=is_fire, latency=latency, false_alarms=false_alarms, predicted_fire_time=predicted_fire_time, TP=TP, TN=TN, FP=FP, FN=FN)

    min_fire_distance = 257
    min_latency = 10000
    alarm_count = {}
    for i in range(256):
        alarm_count[i] = []

    for i, a in enumerate(alarms):
        print(a)
        alarm_count[a.distance].append(a.time)
        if a.distance < min_fire_distance and a.time >= min_ignition_sample:
            min_fire_distance = a.distance
        if a.latency < min_latency and a.time >= min_ignition_sample:
            min_latency = a.latency


    # latencies = []
    false_alarm_list = []

    for idx, r in enumerate(results.keys()):
        false_alarm_list = false_alarm_list + results[r].false_alarms

    for r in results.keys():
        if results[r].fire:
            closest_known_fire = results[r]
            break

    my_set = set(false_alarm_list)
    unique_false_alarms = list(my_set)

    unique_false_alarms = lessThanIgnition(unique_false_alarms, min_ignition_sample)

    print("Fire starts at: ", min_ignition_sample)
    print("Fire detected at bin: ", min_fire_distance)
    print("Distance (meters):    ", min_fire_distance*.044)
    print("Fire latency:         ", min_latency)
    print("Unique false alarms:  ", unique_false_alarms)
    try:
        print("Actual Closest Fire:    ", closest_known_fire)
        print("Closest predicted fire: ", min_fire_distance)
    except:
        print("=== No fire detected ===")

    real_fire_alarms = []
    # print("alarm count: ", alarm_count)
    for dist in alarm_count.keys():
        fire = False
        if dist < 2 or dist > 200:
            continue
        cur_dist = alarm_count[dist]
        closer_list = alarm_count[dist-1]
        further_list = alarm_count[dist+1]
        for time in cur_dist:
            # print("cur_dist: ", cur_dist)
            # print("closer_list: ", closer_list)
            closer = [abs(x - time) for x in closer_list]
            # closer = map(lambda x: (abs(x - time)), closer_list)
            # print('closer: ', closer)
            # print("Further list: ", further_list)
            further = [abs(y - time) for y in further_list]
            # further = map(lambda y: (abs(y - time)), further_list)
            # print("further", further)
            total_list = closer + further
            # print('total_list: ', total_list)
            count = len([i for i in total_list if i < 3])
            # closer_count = len([i for i in closer if i < 3])
            # further_count = len([i for i in further if i < 3])
            # print("count: ", count)
            if count > 0:
            # if closer_count > 0 and further_count > 0:
            # if (0 in closer and 0 in further) or (1 in closer and 1 in further) or (1 in closer and 0 in further) or (0 in closer and 1 in further)or (2 in closer and 0 in further) or (0 in closer and 2 in further) or (2 in closer and 1 in further) or (1 in closer and 2 in further):
                if time >= min_ignition_sample:
                    fire = True
                    latency = time - min_ignition_sample
                    # print("fire: ", fire, " latency: ", latency)
                else:
                    fire = False
                    latency = 10000
                real_fire_alarms.append(RealAlarm(dist, time, fire, latency))


    final_result = FinalResult(min_fire_distance, min_latency, unique_false_alarms)
    return final_result, real_fire_alarms

def lessThanIgnition(false_alarms, ignition):
    real_false_alarms = []
    for i in false_alarms:
        if i < ignition:
            real_false_alarms.append(i)
    return real_false_alarms


def timeSeriesTest(testY, y, leave_one_out, ignition_sample, all_skipped_samples):
    real_fire = 0
    real_nonfire = 0
    if type(testY) == list:
        return alternativeTimeSeriesTest(testY, y, leave_one_out, ignition_sample, all_skipped_samples)

    correct = 0
    incorrect = 0
    FN = 0
    FP = 0
    TP = 0
    TN = 0
    FN_before_fire = 0
    early_classification = 0
    false_alarm = 0
    for i, val in enumerate(testY):
        if y[i][0] > y[i][1]: #predicted no fire
            if val[0] > val[1]: #actual no fire
                correct += 1
                TN += 1
                real_nonfire+=1
            else: #actual fire
                incorrect += 1
                FN += 1
                real_fire+=1
        else: # predicted fire
            if val[0] < val[1]: #actual fire
                correct += 1
                TP += 1
                real_fire+=1
            else: # actual no fire
                incorrect += 1
                FP += 1
                real_nonfire+=1

    print("TP: ", TP)
    print("TN: ", TN)
    print("FP (annoying) - no fire but predicted fire:     ", FP)
    print("FN (bad) - actual fire but not predicted fire:  ", FN)
    print("Amount of fire samples: ", real_fire)
    print("Amount of nonfire samples: ", real_nonfire)
    return Result(distance=0, fire=False, latency=0, false_alarms=[], TP=TP, TN=TN, FP=FP, FN=FN), []

def getIgnitionSample(leave_one_out, y_test):
    ignitions = []
    if leave_one_out is not None:
        print("LEAVE ONE OUT: ", leave_one_out)
        for i, val in enumerate(y_test):
            for j, value in enumerate(val):
                if value[0] < value[1]:
                    ignitions.append(j)
                    break
            else:
                ignitions.append(1000000)
        return ignitions
    else:
        return 0


# TODO: save a model on the cluster and begin to use it as an actual deployment - quicker experiments

def getModel(leave_one_out, model, X, y_train):
    if model is None:
        epochs = 50
        print("Training with: ", X.shape)
        model = train_model(X, y_train, epochs=epochs)
        time_string = time.strftime("%Y%m%d-%H%M%S")
        # # if leave_one_out is not None:
        fname = 'CNN_models/rfire-HYBRID_' + time_string + '_' + str(epochs) + 'epochs_3layer-kernel10_40binsbehindfire-noaug.h5'
        # else:
            # fname = 'CNN_models/' + time_string + '_' + str(epochs) + 'epochs.h5'
        pickle.dump(model, open(fname, 'wb'))
    else:
        fname = 'CNN_models/' + model
        model = pickle.load(open(fname, 'rb'))
        # model = load_model(fname)
    return model


# run an experiment
def run_experiment(leave_one_out, model=None, repeats=1):
    full_TP = 0
    full_TN = 0
    full_FP = 0
    full_FN = 0
    total_latency = 0
    total_false_alarms = 0
    total_distance = 0
    for a in range(repeats):
        # model = None
        if model is None:
            X, y_train, X_train_flat, X_test, y_test, X_test_flat, leave_one_out_fires, all_skipped_samples, ignition_sample = process_dat.getRangeProfileSamples(cnn=True, leave_one_out=leave_one_out)
        else:
            X, y_train, X_train_flat, X_test, y_test, X_test_flat, leave_one_out_fires, all_skipped_samples, ignition_sample = process_dat.getRangeProfileSamples(cnn=True, leave_one_out=leave_one_out, model=model)


        print("X train: ", X.shape)
        # print("flat: ", X_train_flat.shape)

        # print()
        # print("X train: ", X_test[0])


        if type(X_test) == list:
            print("Testing with: ", X_test[0].shape)
            # print("X test flat: ", X_test_flat[0].shape)
            # print(X_test[0])
        else:
            print("Testing with: ", X_test.shape)
            # print("X test flat: ", X_test_flat.shape)


        # ignition_sample = getIgnitionSample(leave_one_out, y_test)
        model = getModel(leave_one_out, model, X, y_train)

        y_preds = []
        if type(X_test) == list:
            for count, x in enumerate(X_test):
                if x.size > 2:
                    print("Predicting bin: ", count+4, " with shape: ", x.shape)
                    x = np.expand_dims(x, axis=1)
                    y_p = model.predict(x)
                    y_preds.append(y_p)
                else:
                    print("Distance ", count + 4, " doesn't have any significant samples")
                    y_preds.append(np.array([]))
        else:
            y_preds = model.predict(X_test)
        final_result, real_fire_alarms = timeSeriesTest(y_test, y_preds, leave_one_out, ignition_sample, all_skipped_samples)


        if type(X_test) == list:
            total_distance+=final_result.fire_distance
            # total_latency+=final_result.latency
            # total_false_alarms+=len(final_result.false_alarms)
            total_latency = 100000
            false_alarm_times = []
            for r in real_fire_alarms:
                print(r)
                if not r.fire:
                    if r.time not in false_alarm_times:
                        false_alarm_times.append(r.time)
                        total_false_alarms+=1
                if r.latency < total_latency:
                    total_latency = r.latency
        else:
            full_TP+=final_result.TP
            full_TN+=final_result.TN
            full_FN+=final_result.FN
            full_FP+=final_result.FP


    if type(X_test) == list:
        print("Average Bin:               ", total_distance/repeats)
        print("Average Distance (meters): ", (total_distance*(0.044))/repeats)
        print("Known fire bins:           ", leave_one_out_fires)
        # if (total_latency/repeats) > 5000:
        #     print('==== NO ALARM ====')
        # else:
        print("Average Latency:           ", total_latency/repeats)
        print("Average False Alarms:      ", total_false_alarms/repeats)
    else:
        print("Total TP: ", full_TP)
        print("Total TN: ", full_TN)
        print("Total FP: ", full_FP)
        print("Total FN: ", full_FN)
        print("Samples: ", X_test.shape[0])
        fires = 0
        for i in y_test:
            if i[0] < i[1]:
                fires+=1
        print("Fires: ", fires)
        print("Average TP: ", (100 * (full_TP/repeats)/fires), " %")
        print("Average TN: ", (100 * (full_TN/repeats)/(X_test.shape[0] - fires)), " %")
        print("Misclassified percentages:")
        print("Average FP: ", (100 * (full_FP/repeats)/(X_test.shape[0] - fires)), " %")
        print("Average FN: ", (100 * (full_FN/repeats)/fires), " %")
    print("Finished")


if __name__ == '__main__':
    # if len(sys.argv) == 2:
    #     mod_str = sys.argv[len(sys.argv) - 1]
    #     mod = load_model('models/' + mod_str)
    #     run_experiment(mod)
    # else:
    #     run_experiment()

    if len(sys.argv) == 2:
        leave_one_out = sys.argv[1]
        mod = None
    elif len(sys.argv) == 3:
        leave_one_out = sys.argv[1]
        mod = sys.argv[2]
    else:
        leave_one_out = None
        mod = None

    run_experiment(leave_one_out, mod)
