# RFire
# 11/21/19
#
# Works with the range profile code starting with process_dat.py
from scipy.fftpack import fft
import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
import multiprocessing as mp
from multiprocessing import Pool
from os import listdir
import object_factory
import sample_factory
import normalizing
from sklearn.utils import shuffle

globalmaxref = 0
globalminref = 1000000000
globalmaxpercfluc = 0
globalminpercfluc = 100000000000
globalmaxdiff = 0
globalmindiff = 1000000000000
globalmaxdegree = 0
globalmindegree = 360

bin_count = {}

for i in range(200):
    bin_count[i] = 0


def augmentExperiments(experiments, leave_one_out):
    new_experiments = []
    new_exp_shift = []
    new_exp_inc = []
    new_exp_dec = []
    new_exp_shiftback = []
    for exp in experiments:
        split_scen = exp.name.split('_')
        if not exp.fire and exp.name != leave_one_out: #  or 'wall' in split_scen
            # new_exp_inc = exp.augmentIncrease(loop_times=2) #4
            # new_exp_dec = exp.augmentDecrease(loop_times=2)
            new_exp_shift = exp.augmentShift(loop_times=1)
            new_exp_shiftback = exp.augmentShiftBack(loop_times=1)
            new_experiments = new_experiments + new_exp_inc + new_exp_dec + new_exp_shift + new_exp_shiftback
    return new_experiments

def augmentObjects(objects, leave_one_out):
    new_objects = []
    new_objs = []
    previous = None
    for o in objects:
        if o.scenario == leave_one_out:
            continue
        new_objs = []
        if previous is not None and previous.scenario != o.scenario:
            previous = None
        if previous is not None:
            new_objs = o.augmentInterpolate(previous, loop_times=5)
        previous = o
        new_objects = new_objects + new_objs

    for o in objects:
        if o.scenario == leave_one_out:
            continue
        new_objs_inc = o.augmentIncrease(loop_times=4)
        new_objs_dec = o.augmentDecrease(loop_times=2)
        new_objects = new_objects + new_objs_inc
        new_objects = new_objects + new_objs_dec
    return new_objects

def calibrateExperiments(experiments, leave_one_out):
    global globalmaxref
    global globalminref

    for e in experiments:
        to_keep = []
        sig_objects = {}
        if e.name == leave_one_out:
            e.leave_one_out = True
            if not e.fire:
                for i, o in enumerate(e.objects.keys()):
                    obj = e.objects[o]
                    if min(obj.reflection) < e.min_reflection:
                        e.min_reflection = min(obj.reflection)
                    if max(obj.reflection) > e.max_reflection:
                        e.max_reflection = max(obj.reflection)
                continue
            for k in range(3, max(e.fire_bin_list) + 37):
                sig_objects[k] = e.objects[k]
            e.objects = sig_objects
            # continue

        for i, o in enumerate(e.objects.keys()):
            obj = e.objects[o]
            if i > 2:
                if min(obj.reflection) < e.min_reflection:
                    e.min_reflection = min(obj.reflection)
                if max(obj.reflection) > e.max_reflection:
                    e.max_reflection = max(obj.reflection)
                if min(obj.reflection) < globalminref:
                    globalminref = min(obj.reflection)
                if max(obj.reflection) > globalmaxref:
                    globalmaxref = max(obj.reflection)
            try:
                if obj.significant or e.objects[o+1].significant or e.objects[o+2].significant or e.objects[o-1].significant or e.objects[o-2].significant:
                    # if obj.good_baseline or e.objects[o+1].good_baseline or e.objects[o+2].good_baseline or e.objects[o-1].good_baseline or e.objects[o-2].good_baseline:
                    to_keep.append(obj.distance)
            except:
                pass

        if e.leave_one_out:
            continue
        for k in to_keep:
            sig_objects[k] = e.objects[k]
        e.objects = sig_objects



def shuffleFormatDataLabels(fire_dataset, nonfire_dataset, second_fire_dataset, second_nonfire_dataset, fire_samples_fft=[], hybrid_fire_fft=[], new_nonfire_fft=[], new_hybrid_fft_nofire=[], flat=False, cnn=True, leave_one_out=None):
    if cnn:
        fire_zeros = np.full((fire_dataset.shape[0], 1), 0)
        fire_ones = np.full((fire_dataset.shape[0], 1), 1)
        fire_labels = np.concatenate((fire_zeros, fire_ones), axis=1)

        nonfire_zeros = np.full((nonfire_dataset.shape[0], 1), 0)
        nonfire_ones = np.full((nonfire_dataset.shape[0], 1), 1)
        nonfire_labels = np.concatenate((nonfire_ones, nonfire_zeros), axis=1)
    else:
        fire_labels = np.full((fire_dataset.shape[0], 1), 1)
        nonfire_labels = np.full((nonfire_dataset.shape[0], 1), 0)

    if fire_dataset.shape[0] > 0 and nonfire_dataset.shape[0] > 0:
        dataset = np.concatenate((nonfire_dataset, fire_dataset), axis=0)
        all_labels = np.concatenate((nonfire_labels, fire_labels), axis=0)
        second_dataset = np.concatenate((second_nonfire_dataset, second_fire_dataset), axis=0)
        dataset_fft = np.concatenate((new_nonfire_fft, fire_samples_fft), axis=0)
        second_dataset_fft = np.concatenate((new_hybrid_fft_nofire, hybrid_fire_fft), axis=0)
    elif fire_dataset.shape[0] > 0:
        dataset = fire_dataset
        all_labels = fire_labels
        second_dataset = second_fire_dataset
        dataset_fft = fire_samples_fft
        second_dataset_fft = hybrid_fire_fft
    else:
        dataset = nonfire_dataset
        all_labels = nonfire_labels
        second_dataset = second_nonfire_dataset
        dataset_fft = new_nonfire_fft
        second_dataset_fft = new_hybrid_fft_nofire

    if leave_one_out is None:
        try:
            X, y, X_second, X_fft, hybrid_X_fft = shuffle(dataset, all_labels, second_dataset, dataset_fft, second_dataset_fft)
        except:
            print("SHUFFLE DIDNT WORK")
            X = dataset
            y = all_labels
            X_second = second_dataset
            X_fft = dataset_fft
            hybrid_X_fft = second_dataset_fft

    else:
        X = dataset
        y = all_labels
        X_second = second_dataset
        X_fft = dataset_fft
        hybrid_X_fft = second_dataset_fft
    return X, y, X_second, X_fft, hybrid_X_fft


def formatData(fire_dataset, nonfire_dataset, flat_fire_dataset, flat_nonfire_samples, cnn, leave_one_out_fire, leave_one_out_nofire, flat_loo_fire, flat_loo_nofire, flat, all_skipped_samples, hybrid, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft, fire_samples_fft, nonfire_samples_fft, hybrid_fire_fft, hybrid_nonfire_fft):
    # leave_one_out_X, leave_one_out_y, second_loo_X, leave_one_out_X_fft, second_loo_X_fft = [], [], [], [], []
    leave_one_out_X, leave_one_out_y, second_loo_X, leave_one_out_X_fft, second_loo_X_fft = {}, {}, {}, {}, {}

    if nonfire_dataset.size > 0:
        if hybrid:
            print("nonfire dataset is larger than 0 and hybrid")
            fire_dataset, hybrid_fire, fire_samples_fft, hybrid_fire_fft = shuffle(fire_dataset, hybrid_fire, fire_samples_fft, hybrid_fire_fft)
            nonfire_dataset, hybrid_nofire, nonfire_samples_fft, hybrid_nonfire_fft = shuffle(nonfire_dataset, hybrid_nofire, nonfire_samples_fft, hybrid_nonfire_fft)
        else:
            print("nonfire dataset is larger than 0 and NOT hybrid")
            fire_dataset, fire_samples_fft = shuffle(fire_dataset, fire_samples_fft)
            nonfire_dataset, nonfire_samples_fft = shuffle(nonfire_dataset, nonfire_samples_fft)

        firebins = {}
        actualfiresamples = {}
        actualhybridfire = {}
        actualfirefft = {}
        actualhybridfirefft = {}

        nonfirebins = {}
        flatfirebins = {}
        flatnonfirebins = {}
        hybridfirebins = {}
        hybridnofirebins = {}
        fftfirebins = {}
        fftnofirebins = {}
        hybridfftfirebins = {}
        hybridfftnofirebins = {}


        # NOTE: THIS DOES NOT WORK ANYMORE... TOOK THE FLAT OUT A COUPLE LINES UP
        if flat:
            for i, val in enumerate(fire_dataset):
                # rounded = round(val[10], 3)
                rounded = round(val[12], 3)
                if rounded not in firebins.keys():
                    firebins[rounded] = 0
            for i, val in enumerate(nonfire_dataset):
                # rounded = round(val[10], 3)
                rounded = round(val[12], 3)
                if rounded not in nonfirebins.keys():
                    nonfirebins[rounded] = []

            for i, val in enumerate(fire_dataset):
                # firebins[round(val[10], 3)]+=1 #appends the index in the dataset
                firebins[round(val[12], 3)]+=1

            for i, val in enumerate(nonfire_dataset):
                # nonfirebins[round(val[10], 3)].append(val)
                nonfirebins[round(val[12], 3)].append(val)
        else:
            # distance_idx = 21 # len 9
            distance_idx = 13 # len 5
            # distance_idx = 5 # len 2

            for i, val in enumerate(fire_dataset):
                rounded = round(val[0][distance_idx], 3)
                if rounded not in firebins.keys():
                    firebins[rounded] = 0
                    actualfiresamples[rounded] = []
                    actualhybridfire[rounded] = []
                    actualfirefft[rounded] = []
                    actualhybridfirefft[rounded] = []
                if rounded not in nonfirebins.keys():
                    nonfirebins[rounded] = []
                    flatnonfirebins[rounded] = []
                    hybridnofirebins[rounded] = []
                    fftnofirebins[rounded] = []
                    hybridfftnofirebins[rounded] = []
            for i, val in enumerate(nonfire_dataset):
                rounded = round(val[0][distance_idx], 3)
                if rounded not in nonfirebins.keys():
                    nonfirebins[rounded] = []
                    flatnonfirebins[rounded] = []
                    hybridnofirebins[rounded] = []
                    fftnofirebins[rounded] = []
                    hybridfftnofirebins[rounded] = []
            for i, val in enumerate(fire_dataset):
                # firebins[round(val[10], 3)]+=1 #appends the index in the dataset
                rounded = round(val[0][distance_idx], 3)
                firebins[rounded]+=1
                actualfiresamples[rounded].append(val)
                actualhybridfire[rounded].append(hybrid_fire[i])
                actualfirefft[rounded].append(fire_samples_fft[i])
                actualhybridfirefft[rounded].append(hybrid_fire_fft[i])
            for i, val in enumerate(nonfire_dataset):
                # nonfirebins[round(val[10], 3)].append(val)
                rounded = round(val[0][distance_idx], 3)
                nonfirebins[rounded].append(val)
                flatnonfirebins[rounded].append(flat_nonfire_samples[i])
                hybridnofirebins[rounded].append(hybrid_nofire[i])
                fftnofirebins[rounded].append(nonfire_samples_fft[i])
                hybridfftnofirebins[rounded].append(hybrid_nonfire_fft[i])

        for i, val in enumerate(firebins.keys()):
            print("count: ", i, " fire: ", firebins[val], " nonfire: ", len(nonfirebins[val]))

        total_longer = 0
        new_fire_dataset = []
        new_hybrid_fire = []
        new_fire_fft = []
        new_hybrid_fire_fft = []

        new_nonfire_dataset = []
        new_flatnonfire_dataset = []
        new_hybrid_nofire = []
        new_nonfire_fft = []
        new_hybrid_fft_nofire = []
        extras = np.array([])
        hybrid_extras = np.array([])
        fft_hybrid_extras = np.array([])
        fft_extras = np.array([])
        flat_extras = np.array([])

        for i, val in enumerate(firebins.keys()):
            if firebins[val] > 0:
                if len(new_nonfire_dataset) == 0:
                    if len(nonfirebins[val]) > firebins[val]: # adding all fire, only some nonfire
                        new_fire_dataset = np.array(actualfiresamples[val])
                        new_fire_fft = np.array(actualfirefft[val])

                        nonfire_toadd, nonfire_toadd_fft = shuffle(nonfirebins[val], fftnofirebins[val])
                        new_nonfire_dataset = np.array(nonfire_toadd[:firebins[val]])
                        extras = np.array(nonfire_toadd[firebins[val]:])
                        new_nonfire_fft = np.array(nonfire_toadd_fft[:firebins[val]])
                        fft_extras = np.array(nonfire_toadd_fft[firebins[val]:])
                        if hybrid:
                            new_hybrid_fire = np.array(actualhybridfire[val])
                            new_hybrid_fire_fft = np.array(actualhybridfirefft[val])

                            new_hybrid_fft_nofire = np.array(hybridfftnofirebins[val][:firebins[val]])
                            fft_hybrid_extras = np.array(hybridfftnofirebins[val][firebins[val]:])
                            new_hybrid_nofire = np.array(hybridnofirebins[val][:firebins[val]])
                            hybrid_extras = np.array(hybridnofirebins[val][firebins[val]:])
                        else:
                            new_flatnonfire_dataset = np.array(flatnonfirebins[val][:firebins[val]])
                            flat_extras = np.array(flatnonfirebins[val][firebins[val]:])
                        # print('here1')
                    else: # adding all nonfire, only some fire <- changed this back to all fire and extras
                        nonfires = len(nonfirebins[val])
                        new_nonfire_dataset = np.array(nonfirebins[val])
                        new_nonfire_fft = np.array(fftnofirebins[val])

                        fire_toadd, fire_toadd_fft = shuffle(actualfiresamples[val], actualfirefft[val])
                        new_fire_dataset = np.array(fire_toadd) #[:nonfires]
                        new_fire_fft = np.array(fire_toadd_fft) #[:nonfires]
                        if hybrid:
                            new_hybrid_nofire = np.array(hybridnofirebins[val])
                            new_hybrid_fft_nofire = np.array(hybridfftnofirebins[val])

                            new_hybrid_fire = np.array(actualhybridfire[val]) #[:nonfires]
                            new_hybrid_fire_fft = np.array(actualhybridfirefft[val]) #[:nonfires]
                else:
                    if len(nonfirebins[val]) > firebins[val]: #adding all fires, some nonfires
                        new_fire_dataset = np.concatenate((new_fire_dataset, np.array(actualfiresamples[val])), axis=0)
                        new_fire_fft = np.concatenate((new_fire_fft, np.array(actualfirefft[val])), axis=0)

                        new_nonfire_dataset = np.concatenate((new_nonfire_dataset, np.array(nonfirebins[val][:firebins[val]])), axis=0)
                        new_nonfire_fft = np.concatenate((new_nonfire_fft, np.array(fftnofirebins[val][:firebins[val]])), axis=0)
                        if hybrid:
                            new_hybrid_fire = np.concatenate((new_hybrid_fire, np.array(actualhybridfire[val])), axis=0)
                            new_hybrid_fire_fft = np.concatenate((new_hybrid_fire_fft, np.array(actualhybridfirefft[val])), axis=0)

                            new_hybrid_nofire = np.concatenate((new_hybrid_nofire, np.array(hybridnofirebins[val][:firebins[val]])), axis=0)
                            new_hybrid_fft_nofire = np.concatenate((new_hybrid_fft_nofire, np.array(hybridfftnofirebins[val][:firebins[val]])), axis=0)
                        else:
                            new_flatnonfire_dataset = np.concatenate((new_flatnonfire_dataset, np.array(flatnonfirebins[val][:firebins[val]])), axis=0)
                        # print('here3')
                        try:
                            extras = np.concatenate((extras, np.array(nonfirebins[val][firebins[val]:])), axis=0)
                            fft_extras = np.concatenate((fft_extras, np.array(fftnofirebins[val][firebins[val]:])), axis=0)
                            if hybrid:
                                hybrid_extras = np.concatenate((hybrid_extras, np.array(hybridnofirebins[val][firebins[val]:])), axis=0)
                                fft_hybrid_extras = np.concatenate((fft_hybrid_extras, np.array(hybridfftnofirebins[val][firebins[val]:])), axis=0)
                            else:
                                flat_extras = np.concatenate((flat_extras, np.array(flatnonfirebins[val][firebins[val]:])), axis=0)
                        except:
                            extras = np.array(nonfirebins[val][firebins[val]:])
                            fft_extras = np.array(fftnofirebins[val][firebins[val]:])
                            if hybrid:
                                hybrid_extras = np.array(hybridnofirebins[val][firebins[val]:])
                                fft_hybrid_extras = np.array(hybridfftnofirebins[val][firebins[val]:])
                            else:
                                flat_extras = np.array(flatnonfirebins[val][firebins[val]:])
                    # elif len(nonfirebins[val]) > 0:
                    #     total_longer += firebins[val] - len(nonfirebins[val])
                    #     new_nonfire_dataset = np.concatenate((new_nonfire_dataset, np.array(nonfirebins[val])), axis=0)
                    #     new_nonfire_fft = np.concatenate((new_nonfire_fft, np.array(fftnofirebins[val])), axis=0)
                    #     if hybrid:
                    #         new_hybrid_nofire = np.concatenate((new_hybrid_nofire, np.array(hybridnofirebins[val])), axis=0)
                    #         new_hybrid_fft_nofire = np.concatenate((new_hybrid_fft_nofire, np.array(hybridfftnofirebins[val])), axis=0)
                    #     else:
                    #         new_flatnonfire_dataset = np.concatenate((new_flatnonfire_dataset, np.array(flatnonfirebins[val])), axis=0)
                    else: #add all nonfires, only some fires <- changed this back
                        nonfires = len(nonfirebins[val])
                        if nonfires > 0:
                            new_nonfire_dataset = np.concatenate((new_nonfire_dataset, np.array(nonfirebins[val])), axis=0)
                            new_nonfire_fft = np.concatenate((new_nonfire_fft, np.array(fftnofirebins[val])), axis=0)

                            new_fire_dataset = np.concatenate((new_fire_dataset, np.array(actualfiresamples[val])), axis=0) #[:nonfires]
                            new_fire_fft = np.concatenate((new_fire_fft, np.array(actualfirefft[val])), axis=0) #[:nonfires]
                            if hybrid:
                                new_hybrid_nofire = np.concatenate((new_hybrid_nofire, np.array(hybridnofirebins[val])), axis=0)
                                new_hybrid_fft_nofire = np.concatenate((new_hybrid_fft_nofire, np.array(hybridfftnofirebins[val])), axis=0)

                                new_hybrid_fire = np.concatenate((new_hybrid_fire, np.array(actualhybridfire[val])), axis=0) #[:nonfires]
                                new_hybrid_fire_fft = np.concatenate((new_hybrid_fire_fft, np.array(actualhybridfirefft[val])), axis=0) #[:nonfires]
                            else:
                                new_flatnonfire_dataset = np.concatenate((new_flatnonfire_dataset, np.array(flatnonfirebins[val][:firebins[val]])), axis=0)
                            # print('here3')



# # NOTE: added this below
#             # fire bins is 0 - distance without fire
            # else:
            #     if len(new_nonfire_dataset) == 0:
            #         extras = np.array(nonfirebins[val])
            #         fft_extras = np.array(fftnofirebins[val])
            #         if hybrid:
            #             fft_hybrid_extras = np.array(hybridfftnofirebins[val])
            #             hybrid_extras = np.array(hybridnofirebins[val])
            #         else:
            #             flat_extras = np.array(flatnonfirebins[val])
            #
            #     else:
            #         try:
            #             extras = np.concatenate((extras, np.array(nonfirebins[val])), axis=0)
            #             fft_extras = np.concatenate((fft_extras, np.array(fftnofirebins[val])), axis=0)
            #             if hybrid:
            #                 hybrid_extras = np.concatenate((hybrid_extras, np.array(hybridnofirebins[val])), axis=0)
            #                 fft_hybrid_extras = np.concatenate((fft_hybrid_extras, np.array(hybridfftnofirebins[val])), axis=0)
            #             else:
            #                 flat_extras = np.concatenate((flat_extras, np.array(flatnonfirebins[val])), axis=0)
            #         except:
            #             extras = np.array(nonfirebins[val])
            #             fft_extras = np.array(fftnofirebins[val])
            #             if hybrid:
            #                 hybrid_extras = np.array(hybridnofirebins[val])
            #                 fft_hybrid_extras = np.array(hybridfftnofirebins[val])
            #             else:
            #                 flat_extras = np.array(flatnonfirebins[val])

        # fire_dataset = new_fire_dataset
        # hybrid_fire = new_hybrid_fire
        # fire_samples_fft = new_fire_fft
        # hybrid_fire_fft = new_hybrid_fire_fft
        print("Check if more fires:")
        total_longer = fire_dataset.shape[0] - new_nonfire_dataset.shape[0]
        print("Fires have ", total_longer, " more samples")
        print("Nonfires: ", new_nonfire_dataset.shape, " fft: ", new_nonfire_fft.shape)
        print("Fires:    ", fire_dataset.shape, " fft: ", fire_samples_fft.shape)
        # print("Extras:   ", extras.shape)
        print("HYBRID:")
        print("Non-Fires: ", new_hybrid_nofire.shape, " fft: ", new_hybrid_fft_nofire.shape)
        print("Fire: ", hybrid_fire.shape, " fft: ", hybrid_fire_fft.shape)
        # print("Fires:     ", flat_fire_dataset.shape)

        if total_longer > 0:
            if hybrid:
                extras, hybrid_extras, fft_extras, fft_hybrid_extras = shuffle(extras, hybrid_extras, fft_extras, fft_hybrid_extras)
                try:
                    new_nonfire_dataset = np.concatenate((new_nonfire_dataset, extras[:total_longer]), axis=0)
                    new_hybrid_nofire = np.concatenate((new_hybrid_nofire, hybrid_extras[:total_longer]), axis=0)
                    new_nonfire_fft = np.concatenate((new_nonfire_fft, fft_extras[:total_longer]), axis=0)
                    new_hybrid_fft_nofire = np.concatenate((new_hybrid_fft_nofire, fft_hybrid_extras[:total_longer]), axis=0)
                except:
                    print("Dims arent the same")
                    pass
            else:
                np.random.shuffle(extras)
                new_nonfire_dataset = np.concatenate((new_nonfire_dataset, extras[:total_longer]), axis=0) # TODO: uncomment for real data
            # new_flatnonfire_dataset = np.concatenate((new_flatnonfire_dataset, flat_extras[:total_longer]), axis=0)

        print("After balance:")
        print("Nonfires: ", new_nonfire_dataset.shape)
        print("Fires:    ", fire_dataset.shape)
        print("Hybrid:")
        print("Non-Fires: ", new_hybrid_nofire.shape)
        # print("Fires:     ", flat_fire_dataset.shape)

        if hybrid:
            X, y, X_second, X_fft, X_second_fft = shuffleFormatDataLabels(fire_dataset, new_nonfire_dataset, hybrid_fire, new_hybrid_nofire, fire_samples_fft=fire_samples_fft, hybrid_fire_fft=hybrid_fire_fft, new_nonfire_fft=new_nonfire_fft, new_hybrid_fft_nofire=new_hybrid_fft_nofire, flat=flat, cnn=cnn)
        else:
            X, y, X_second, X_fft, X_second_fft = shuffleFormatDataLabels(fire_dataset, new_nonfire_dataset, flat_fire_dataset, new_flatnonfire_dataset, flat=flat, cnn=cnn)
    else:
        X, y, X_second, X_fft, X_second_fft = np.array([]), np.array([]), np.array([]), np.array([]), np.array([])

    if len(leave_one_out_nofire.keys()) is not 0:
        for dist in leave_one_out_nofire.keys():
            if hybrid:
                test_x, test_y, second_x_test, test_x_fft, second_x_test_fft = shuffleFormatDataLabels(leave_one_out_fire[dist], leave_one_out_nofire[dist], hybrid_loo_fire[dist], hybrid_loo_nofire[dist], fire_samples_fft=fire_samples_loo_fft[dist], hybrid_fire_fft=hybrid_fire_loo_fft[dist], new_nonfire_fft=nonfire_samples_loo_fft[dist], new_hybrid_fft_nofire=hybrid_nonfire_loo_fft[dist], flat=flat, cnn=cnn, leave_one_out="leave one out")
            else:
                test_x, test_y, second_x_test, test_x_fft, second_x_test_fft = shuffleFormatDataLabels(leave_one_out_fire[dist], leave_one_out_nofire[dist], flat_loo_fire[dist], flat_loo_nofire[dist], fire_samples_fft=fire_samples_loo_fft[dist], hybrid_fire_fft=hybrid_fire_loo_fft[dist], new_nonfire_fft=nonfire_samples_loo_fft[dist], new_hybrid_fft_nofire=hybrid_nonfire_loo_fft[dist], flat=flat, cnn=cnn, leave_one_out="leave one out")
            # second_loo_X.append(second_x_test)
            # leave_one_out_X.append(test_x)
            # leave_one_out_y.append(test_y)
            # leave_one_out_X_fft.append(test_x_fft)
            # second_loo_X_fft.append(second_x_test_fft)
            second_loo_X[dist] = second_x_test
            leave_one_out_X[dist] = test_x
            leave_one_out_y[dist] = test_y
            leave_one_out_X_fft[dist] = test_x_fft
            second_loo_X_fft[dist] = second_x_test_fft

    return X, y, X_second, X_fft, X_second_fft, leave_one_out_X, leave_one_out_y, second_loo_X, leave_one_out_X_fft, second_loo_X_fft, all_skipped_samples

def nonParallelLeaveOneOutSampleCreation(obj, exp, flat, hybrid):
    start_time = 0
    end_time = obj.len_reflection
    leave_one_out_fire, leave_one_out_nofire, skipped_samples, hybrid_loo_fire, hybrid_loo_nofire = sample_factory.makeSamples(exp, obj, start_time, end_time, flat, leave_one_out_bool=True, hybrid=hybrid)

    return {"distance": obj.distance,
            "samples": [np.array(leave_one_out_fire), np.array(leave_one_out_nofire), skipped_samples, np.array(hybrid_loo_fire), np.array(hybrid_loo_nofire)]}


def parallelLeaveOneOutSampleCreation(pass_arr):
    obj = pass_arr[0]
    exp = pass_arr[1]
    start_time = pass_arr[2]
    end_time = pass_arr[3]
    flat = pass_arr[4]
    hybrid = pass_arr[5]

    leave_one_out_fire, leave_one_out_nofire, skipped_samples, hybrid_loo_fire, hybrid_loo_nofire = sample_factory.makeSamples(exp, obj, start_time, end_time, flat, leave_one_out_bool=True, hybrid=hybrid)

    return {"distance": obj.distance,
            "samples": [np.array(leave_one_out_fire), np.array(leave_one_out_nofire), skipped_samples, np.array(hybrid_loo_fire), np.array(hybrid_loo_nofire)]}

#could try to parallelize, but needs to omake this sequential so that the distances are all consistent
def makeLeaveOneOutSamples(exp, flat, hybrid):
    fire_samples, nonfire_samples, leave_one_out_samples_fire, leave_one_out_samples_nofire, fires, nonfires, leave_one_out_samples, leave_one_out_fire, leave_one_out_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire = [], [], [], [], [], [], [], [], [], [], [], [], []
    all_skipped_samples = []

    fire_samples_dict, nonfire_samples_dict, all_skipped_samples_dict, hybrid_fire_dict, hybrid_nofire_dict = {}, {}, {}, {}, {}

    cores = 40
    chunksize = 1

    pass_arr = []
    for o in exp.objects.keys():
        obj = exp.objects[o]

        #filters
        if o < (list(exp.objects.keys())[:1][0] + 2) or obj.distance > (list(exp.objects.keys())[-1:][0] - 2):
            continue
        if obj.distance > 170 and not exp.fire: #changed from 175
            continue
        if exp.fire and obj.distance > (exp.fire_bin_list[-1:][0] + 37): # THIS LIMITS THE TESTING SEARCH SPACE TO JUST BEHIND THE FIRE
            continue

        fire_samples_dict[obj.distance] = []
        nonfire_samples_dict[obj.distance] = []
        all_skipped_samples_dict[obj.distance] = []
        hybrid_fire_dict[obj.distance] = []
        hybrid_nofire_dict[obj.distance] = []

        # This is assuming that fire experiments will never be longer than 1 hour
        obj = exp.objects[o]
        start_time = 0
        end_time = 40000
        step = 40000
        loops_per_dist = 0
        while end_time <= obj.len_reflection:
            pass_arr.append([obj, exp, start_time, end_time, flat, hybrid])
            start_time+=step
            end_time+=step
            loops_per_dist+=1
            # print('samples: ', )
        if loops_per_dist != 1:
            pass_arr.append([obj, exp, start_time, obj.len_reflection, flat, hybrid])
            loops_per_dist+=1

    with Pool(processes=cores) as pool:
        sample_results = pool.map(parallelLeaveOneOutSampleCreation, pass_arr, chunksize)

    for i, r in enumerate(sample_results):
        # fire_samples.append(np.array(r["samples"][0]))
        # nonfire_samples.append(np.array(r["samples"][1]))
        # all_skipped_samples.append(r["samples"][2])
        # hybrid_fire.append(np.array(r["samples"][3]))
        # hybrid_nofire.append(np.array(r["samples"][4]))
        fire_samples_dict[r["distance"]].append(np.array(r["samples"][0]))
        nonfire_samples_dict[r["distance"]].append(np.array(r["samples"][1]))
        all_skipped_samples_dict[r["distance"]] = all_skipped_samples_dict[r["distance"]] + r["samples"][2]
        hybrid_fire_dict[r["distance"]].append(np.array(r["samples"][3]))
        hybrid_nofire_dict[r["distance"]].append(np.array(r["samples"][4]))
    all_skipped_samples = all_skipped_samples_dict

    for i, val in enumerate(fire_samples_dict.keys()):
        fire_size = False
        nonfire_size = False
        for x in fire_samples_dict[val]:
            if x.size > 0:
                fire_size = True
                break
        if fire_size:
            fire_samples_dict[val] = np.concatenate([x for x in fire_samples_dict[val] if x.size > 0])
            hybrid_fire_dict[val] = np.concatenate([x for x in hybrid_fire_dict[val] if x.size > 0])
        else:
            fire_samples_dict[val] = fire_samples_dict[val][0]
            hybrid_fire_dict[val] = hybrid_fire_dict[val][0]

        for x in nonfire_samples_dict[val]:
            if x.size > 0:
                nonfire_size = True
                break
        if nonfire_size:
            nonfire_samples_dict[val] = np.concatenate([x for x in nonfire_samples_dict[val] if x.size > 0])
            hybrid_nofire_dict[val] = np.concatenate([x for x in hybrid_nofire_dict[val] if x.size > 0])
        else:
            nonfire_samples_dict[val] = nonfire_samples_dict[val][0]
            hybrid_nofire_dict[val] = hybrid_nofire_dict[val][0]

    flat_fire_samples, fire_samples_fft, hybrid_fire_fft, fire_samples, hybrid_fire = sortSamples_dict(fire_samples_dict, hybrid_fire_dict, exp, loops_per_dist)
    flat_nonfire_samples, nonfire_samples_fft, hybrid_nonfire_fft, nonfire_samples, hybrid_nofire = sortSamples_dict(nonfire_samples_dict, hybrid_nofire_dict, exp, loops_per_dist)
    # flat_fire_samples, fire_samples_fft, hybrid_fire_fft, fire_samples, hybrid_fire = sortSamples(fire_samples, hybrid_fire, exp, loops_per_dist)
    # flat_nonfire_samples, nonfire_samples_fft, hybrid_nonfire_fft, nonfire_samples, hybrid_nofire = sortSamples(nonfire_samples, hybrid_nofire, exp, loops_per_dist)

    # for i, val in enumerate(fire_samples_dict.keys()):
    #     print("Distance: ", val, " samples: ", len(fire_samples_dict[val]))
    #
    # exit()

    skipped = []
    current_skipped = []
    # if loops_per_dist > 1:
    #     for i, val in enumerate(all_skipped_samples):
    #         # print(i, " ", i%loops_per_dist)
    #         if i > 0 and i%loops_per_dist == (loops_per_dist - 1):
    #             skipped.append(current_skipped)
    #             current_skipped = []
    #         current_skipped = current_skipped + val
    #     all_skipped_samples = skipped

    return fire_samples, nonfire_samples, flat_fire_samples, flat_nonfire_samples, all_skipped_samples, hybrid_fire, hybrid_nofire, fire_samples_fft, hybrid_fire_fft, nonfire_samples_fft, hybrid_nonfire_fft

def sortSamples_dict(main_samples, hybrid_samples, exp, loops_per_dist):
    flat_samples = dict.fromkeys(main_samples.keys(), np.array([]))
    samples_fft = dict.fromkeys(main_samples.keys(), np.array([]))
    hybrid_fft = dict.fromkeys(main_samples.keys(), np.array([]))
    samples3d = dict.fromkeys(main_samples.keys(), np.array([]))
    samples3d_hybrid = dict.fromkeys(main_samples.keys(), np.array([]))

    distance = 0
    for i, dist in enumerate(main_samples.keys()):
        flat_sample = sample_factory.makeFlatFrom3D(main_samples[dist], exp)
        flat_sample = normalizing.normalize1dScenario(flat_sample, exp)

        single_sample_fft = normalizing.normalizeFFT(main_samples[dist])
        single_hybrid_fft = normalizing.normalizeFFT(hybrid_samples[dist])

        single_sample = normalizing.normalize3dScenario(main_samples[dist], exp, globalmaxref, globalminref)
        single_hybrid = normalizing.normalize3dScenario(hybrid_samples[dist], exp, globalmaxref, globalminref)

        # print("dist: ", distance, " shape: ", single_sample.shape)

        # print("before if: ", flat_samples[distance])

            # print('flat_sample: ', flat_sample)
        flat_samples[dist] = flat_sample
        samples_fft[dist] = single_sample_fft
        hybrid_fft[dist] = single_hybrid_fft
        samples3d[dist] = single_sample
        samples3d_hybrid[dist] = single_hybrid

    return flat_samples, samples_fft, hybrid_fft, samples3d, samples3d_hybrid




def sortSamples(main_samples, hybrid_samples, exp, loops_per_dist):
    flat_samples = [None] * (len(main_samples)//loops_per_dist)
    samples_fft = [None] * (len(main_samples)//loops_per_dist)
    hybrid_fft = [None] * (len(main_samples)//loops_per_dist)
    samples3d = [None] * (len(main_samples)//loops_per_dist)
    samples3d_hybrid = [None] * (len(main_samples)//loops_per_dist)

    distance = 0
    for i, sample in enumerate(main_samples):
        if i > 0 and i%loops_per_dist == 0:
            distance+=1

        flat_sample = sample_factory.makeFlatFrom3D(sample, exp)
        flat_sample = normalizing.normalize1dScenario(flat_sample, exp)

        single_sample_fft = normalizing.normalizeFFT(sample)
        single_hybrid_fft = normalizing.normalizeFFT(hybrid_samples[i])

        single_sample = normalizing.normalize3dScenario(sample, exp, globalmaxref, globalminref)
        single_hybrid = normalizing.normalize3dScenario(hybrid_samples[i], exp, globalmaxref, globalminref)

        # print("dist: ", distance, " shape: ", single_sample.shape)

        # print("before if: ", flat_samples[distance])
        if flat_sample.size > 2:
            if flat_samples[distance] is None:
                # print('flat_sample: ', flat_sample)
                flat_samples[distance] = flat_sample
                samples_fft[distance] = single_sample_fft
                hybrid_fft[distance] = single_hybrid_fft
                samples3d[distance] = single_sample
                samples3d_hybrid[distance] = single_hybrid
            else:
                # print(flat_samples[distance].shape, " and ", flat_sample.shape)
                flat_samples[distance] = np.concatenate((flat_samples[distance], flat_sample), axis=0)
                samples_fft[distance] = np.concatenate((samples_fft[distance], single_sample_fft), axis=0)
                hybrid_fft[distance] = np.concatenate((hybrid_fft[distance], single_hybrid_fft), axis=0)
                samples3d[distance] = np.concatenate((samples3d[distance], single_sample), axis=0)
                samples3d_hybrid[distance] = np.concatenate((samples3d_hybrid[distance], single_hybrid), axis=0)
                # print(samples3d[distance].shape)
                # print('Concat')

        # if i > 0 and i%loops_per_dist == (loops_per_dist - 1):
    for i in range(len(flat_samples)):
        if flat_samples[i] is None:
            flat_samples[i] = np.array([])
            samples_fft[i] = np.array([])
            hybrid_fft[i] = np.array([])
            samples3d[i] = np.array([])
            samples3d_hybrid[i] = np.array([])

    return flat_samples, samples_fft, hybrid_fft, samples3d, samples3d_hybrid


def setupParallel(experiments, cnn, leave_one_out, svm, flat, model, hybrid):
    global bin_count

    fire_samples, nonfire_samples, leave_one_out_fire, leave_one_out_nofire, flat_loo_fire, flat_loo_nofire, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire = [], [], [], [], [], [], [], [], [], []
    all_flat_fire_samples, all_flat_nonfire_samples = [], []
    all_fire_fft, all_nonfire_fft, all_hybrid_fire_fft, all_hybrid_nonfire_fft = np.array([]), np.array([]), np.array([]), np.array([])
    all_fire_samples, all_nonfire_samples, all_hybrid_fire_samples, all_hybrid_nofire_samples = np.array([]), np.array([]), np.array([]), np.array([])
    max_fft_fire = 0
    max_fft_nofire = 0
    total_fft_fire = 0
    total_fft_nofire = 0
    num_fft_fire = 0
    num_fft_nofire = 0
    total_firemax_idx = 0
    total_nofiremax_idx = 0

    cores = 40
    chunksize = 1 #8

    if model is None:
        for exp in experiments:
            if flat:
                fire_samples = np.array([])
                nonfire_samples = np.array([])
            else:
                fire_samples, nonfire_samples = [], []
                hybrid_fire, hybrid_nofire = [], []
            if exp.name == leave_one_out:
                leave_one_out_exp = exp
                continue

            print("Creating Samples for: ", exp)
            try:
                obj_dists = list(exp.objects.keys())
                pass_arr = []
                for dist in obj_dists:
                    pass_arr.append([dist, cnn, leave_one_out, svm, flat, hybrid])
                with Pool(processes=cores) as pool:
                    exp_results = pool.map(exp.parallelObjectSampleCreation, pass_arr, chunksize)
            except:
                print("Experiment too big to use parallel")
                exp_results = []
                for dist in obj_dists:
                    exp_results.append(exp.nonParallelObjectSampleCreation(dist, cnn, leave_one_out, svm, flat, hybrid))


            for idx, r in enumerate(exp_results):
                if flat:
                    if len(r[0]) != 0 and fire_samples.size == 0:
                        fire_samples = r[0]
                    elif r[0].size != 0:
                        fire_samples = np.concatenate((fire_samples, r[0]), axis=0)
                    if len(r[1]) != 0 and nonfire_samples.size == 0:
                        nonfire_samples = r[1]
                    elif len(r[1]) != 0:
                        nonfire_samples = np.concatenate((nonfire_samples, r[1]), axis=0)
                else:
                    fire_samples = fire_samples + r[0]
                    nonfire_samples = nonfire_samples + r[1]
                    if hybrid:
                        hybrid_fire = hybrid_fire + r[4]
                        hybrid_nofire = hybrid_nofire + r[5]
            if flat:
                if exp.name == leave_one_out:
                    toNorm = [leave_one_out_fire, leave_one_out_nofire]
                else:
                    toNorm = [fire_samples, nonfire_samples]

                toReturn = []
                for i in toNorm:
                    toReturn.append(normalizing.normalizeFlatData(i, fire_samples, nonfire_samples, leave_one_out, exp))

                if exp.name == leave_one_out:
                    leave_one_out_fire = toReturn[0]
                    leave_one_out_nofire = toReturn[1]
                else:
                    if all_fire_samples.size == 0:
                        all_fire_samples = toReturn[0]
                    elif toReturn[0].size > 1:
                        all_fire_samples = np.concatenate((all_fire_samples, toReturn[0]), axis=0)
                    if all_nonfire_samples.size == 0:
                        all_nonfire_samples = toReturn[1]
                    elif toReturn[1].size > 1:
                        all_nonfire_samples = np.concatenate((all_nonfire_samples, toReturn[1]), axis=0)

            if not flat and exp.name != leave_one_out:
                fire_samples = np.array(fire_samples)
                nonfire_samples = np.array(nonfire_samples)
                if hybrid:
                    hybrid_fire = np.array(hybrid_fire)
                    hybrid_nofire = np.array(hybrid_nofire)

            # print("fire samples: ", fire_samples.shape)

            if nonfire_samples.size > 1 or fire_samples.size > 1:
                if nonfire_samples.size > 1:
                    flat_nonfire_samples = sample_factory.makeFlatFrom3D(nonfire_samples, exp)
                    flat_nonfire_samples = normalizing.normalize1dScenario(flat_nonfire_samples, exp)
                    nonfire_samples_fft = normalizing.normalizeFFT(nonfire_samples)
                    nonfire_samples = normalizing.normalize3dScenario(nonfire_samples, exp)
                    if hybrid:
                        hybrid_nonfire_samples_fft = normalizing.normalizeFFT(hybrid_nofire)
                        hybrid_nofire = normalizing.normalize3dScenario(hybrid_nofire, exp)
                        if all_hybrid_nofire_samples.size < 2 and all_nonfire_samples.size < 2:
                            all_nonfire_samples = nonfire_samples
                            all_hybrid_nofire_samples = hybrid_nofire
                            all_flat_nonfire_samples = flat_nonfire_samples
                            all_nonfire_fft = nonfire_samples_fft
                            all_hybrid_nonfire_fft = hybrid_nonfire_samples_fft
                        else:
                            all_nonfire_samples = np.concatenate((all_nonfire_samples, nonfire_samples), axis=0)
                            all_flat_nonfire_samples = np.concatenate((all_flat_nonfire_samples, flat_nonfire_samples), axis=0)
                            all_hybrid_nofire_samples = np.concatenate((all_hybrid_nofire_samples, hybrid_nofire), axis=0)
                            all_nonfire_fft = np.concatenate((all_nonfire_fft, nonfire_samples_fft), axis=0)
                            all_hybrid_nonfire_fft = np.concatenate((all_hybrid_nonfire_fft, hybrid_nonfire_samples_fft), axis=0)
                    else:
                        if all_nonfire_samples.size < 2:
                            all_nonfire_samples = nonfire_samples
                            all_flat_nonfire_samples = flat_nonfire_samples
                            all_nonfire_fft = nonfire_samples_fft
                        else:
                            all_nonfire_samples = np.concatenate((all_nonfire_samples, nonfire_samples), axis=0)
                            all_flat_nonfire_samples = np.concatenate((all_flat_nonfire_samples, flat_nonfire_samples), axis=0)
                            all_nonfire_fft = np.concatenate((all_nonfire_fft, nonfire_samples_fft), axis=0)
                if fire_samples.size > 1:
                    # for f in fire_samples:
                    #     dist = f[0][13]
                    #     bin_count[dist]+=1
                    #make flat samples
                    flat_fire_samples = sample_factory.makeFlatFrom3D(fire_samples, exp)
                    #normalize those 1d samples
                    flat_fire_samples = normalizing.normalize1dScenario(flat_fire_samples, exp)
                    #normalize the 3d version of those samples
                    fire_samples_fft = normalizing.normalizeFFT(fire_samples)
                    fire_samples = normalizing.normalize3dScenario(fire_samples, exp)
                    if hybrid:
                        hybrid_fire_samples_fft = normalizing.normalizeFFT(hybrid_fire)
                        hybrid_fire = normalizing.normalize3dScenario(hybrid_fire, exp)
                        if all_hybrid_fire_samples.size < 2 and all_fire_samples.size < 2:
                            all_fire_samples = fire_samples
                            all_hybrid_fire_samples = hybrid_fire
                            all_flat_fire_samples = flat_fire_samples
                            all_fire_fft = fire_samples_fft
                            all_hybrid_fire_fft = hybrid_fire_samples_fft
                        else:
                            if hybrid_fire.size > 2 and fire_samples.size > 2:
                                all_fire_samples = np.concatenate((all_fire_samples, fire_samples), axis=0)
                                all_flat_fire_samples = np.concatenate((all_flat_fire_samples, flat_fire_samples), axis=0)
                                all_hybrid_fire_samples = np.concatenate((all_hybrid_fire_samples, hybrid_fire), axis=0)
                                all_fire_fft = np.concatenate((all_fire_fft, fire_samples_fft), axis=0)
                                all_hybrid_fire_fft = np.concatenate((all_hybrid_fire_fft, hybrid_fire_samples_fft), axis=0)
                    else:
                        if all_fire_samples.size < 2:
                            all_fire_samples = fire_samples
                            all_flat_fire_samples = flat_fire_samples
                            all_fire_fft = fire_samples_fft
                        else:
                            all_fire_samples = np.concatenate((all_fire_samples, fire_samples), axis=0)
                            all_flat_fire_samples = np.concatenate((all_flat_fire_samples, flat_fire_samples), axis=0)
                            all_fire_fft = np.concatenate((all_fire_fft, fire_samples_fft), axis=0)
            else:
                continue

            #
            # # if no samples meet the training criteria
            # if nonfire_samples.size < 1 and fire_samples.size < 1:
            #     continue
            # # checking for hybrid
            # if hybrid and hybrid_nofire.size < 1 and hybrid_fire.size < 1:
            #     continue








    else:
        leave_one_out_exp = experiments[0]
        all_flat_fire_samples, all_flat_nonfire_samples = np.array([]), np.array([])

    if leave_one_out is not None:
        #doing leave one out last
        leave_one_out_fire, leave_one_out_nofire, flat_loo_fire, flat_loo_nofire, all_skipped_samples, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft = leaveOneOutSamplesMidstep(leave_one_out_exp, fire_samples, nonfire_samples, flat, hybrid)
    else:
        all_skipped_samples = []

    if flat:
        return all_fire_samples, all_nonfire_samples, leave_one_out_fire, leave_one_out_nofire, all_skipped_samples

    # for f in bin_count.keys():
    #     print("bin: ", f, " amt: ", bin_count[f])

    return all_fire_samples, all_nonfire_samples, all_flat_fire_samples, all_flat_nonfire_samples, leave_one_out_fire, leave_one_out_nofire, flat_loo_fire, flat_loo_nofire, all_skipped_samples, all_hybrid_fire_samples, all_hybrid_nofire_samples, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft, all_fire_fft, all_nonfire_fft, all_hybrid_fire_fft, all_hybrid_nonfire_fft

def leaveOneOutSamplesMidstep(leave_one_out_exp, fire_samples, nonfire_samples, flat, hybrid):
    if hybrid:
        print("HYBRID - Creating Leave One Out Samples for: ", leave_one_out_exp)
    else:
        print("Creating Leave One Out Samples for: ", leave_one_out_exp)
    leave_one_out_fire, leave_one_out_nofire, flat_loo_fire, flat_loo_nonfire, all_skipped_samples, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft = makeLeaveOneOutSamples(leave_one_out_exp, flat, hybrid)
    if flat:
        for key, val in enumerate(leave_one_out_fire):
            loo_fire = [] # normalizing.normalizeFlatData(val, fire_samples, nonfire_samples, leave_one_out, leave_one_out_exp)
            loo_nonfire = [] # normalizing.normalizeFlatData(leave_one_out_nofire[key], fire_samples, nonfire_samples, leave_one_out, leave_one_out_exp)
            leave_one_out_fire[key] = loo_fire
            leave_one_out_nofire[key] = loo_nonfire

    return leave_one_out_fire, leave_one_out_nofire, flat_loo_fire, flat_loo_nonfire, all_skipped_samples, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft

def makeDataset(experiments, cnn=False, leave_one_out=None, svm=False, flat=False, model=None, hybrid=False):
    fire_samples, nonfire_samples, leave_one_out_samples_fire, leave_one_out_samples_nofire, fires, nonfires, leave_one_out_samples, leave_one_out_fire, leave_one_out_nofire = [], [], [], [], [], [], [], [], []
    print("Experiments: ", len(experiments))
    fire_samples, nonfire_samples, flat_fire_samples, flat_nonfire_samples, leave_one_out_fire, leave_one_out_nofire, flat_loo_fire, flat_loo_nofire, all_skipped_samples, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft, fire_samples_fft, nonfire_samples_fft, hybrid_fire_fft, hybrid_nonfire_fft = setupParallel(experiments, cnn, leave_one_out, svm, flat, model, hybrid)

    print("Fire Samples:         ", fire_samples.shape)
    print("Nonfire Samples:      ", nonfire_samples.shape)
    # print("Flat Fire Samples:    ", flat_fire_samples.shape)
    # print("Flat Nonfire Samples: ", flat_nonfire_samples.shape)
    print("HYBRID Fire Samples:         ", hybrid_fire.shape)
    print("HYBRID Nonfire Samples:      ", hybrid_nofire.shape)


    if leave_one_out is not None:
        # leave_one_out_samples = np.concatenate((leave_one_out_samples_nofire, leave_one_out_samples_fire), axis=0)
        print("Leave one out no fire: ", len(leave_one_out_nofire[list(leave_one_out_nofire.keys())[0]]))
        print("Leave one out fire:    ", len(leave_one_out_fire[list(leave_one_out_fire.keys())[0]]))
        # print("Leave one out no fire samples: ", leave_one_out_nofire[0].shape)
        try:
            print("leave one out fire above")
            # print("Leave one out fire samples:    ", leave_one_out_fire[len(leave_one_out_fire) - 3].shape)
            # print("Leave one out fft samples:    ", nonfire_samples_loo_fft)
            # print("Flat LOO samples:              ", flat_loo_fire[len(flat_loo_fire) - 3].shape)
        except:
            split_scen = leave_one_out.split('_')
            if 'fire' not in split_scen:
                print("Leave one out nofire samples:    ", leave_one_out_nofire[len(leave_one_out_nofire) - 1].shape)
                # print("Flat LOO samples:                ", flat_loo_fire[len(flat_loo_nofire) - 1].shape)
            else:
                print("Leave one out fire samples:    ", leave_one_out_fire[len(leave_one_out_fire) - 1].shape)
                # print("Flat LOO samples:              ", flat_loo_fire[len(flat_loo_fire) - 1].shape)

    return formatData(fire_samples, nonfire_samples, flat_fire_samples, flat_nonfire_samples, cnn, leave_one_out_fire, leave_one_out_nofire, flat_loo_fire, flat_loo_nofire, flat, all_skipped_samples, hybrid, hybrid_fire, hybrid_nofire, hybrid_loo_fire, hybrid_loo_nofire, fire_samples_loo_fft, hybrid_fire_loo_fft, nonfire_samples_loo_fft, hybrid_nonfire_loo_fft, fire_samples_fft, nonfire_samples_fft, hybrid_fire_fft, hybrid_nonfire_fft)
