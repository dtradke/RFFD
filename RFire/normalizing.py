# RFire
# 12/15/19
#
# Works with the range profile code starting with process_dat.py

import pandas as pd
import numpy as np
import sys
from matplotlib import pyplot as plt
import multiprocessing as mp
from multiprocessing import Pool
from os import listdir
import object_factory
import sample_factory
# import util

#
# isglobalslopemax = 0
# isglobalslopemin = 1000
# globalmaxdiff = 0
# globalmindiff = 1000000


def normalizeFFT(arr):
    if arr.size < 2:
        return arr

    # reflections_idx = 18 # len 9
    reflections_idx = 10 # len 5
    # reflections_idx = 4 # len 2

    arr = arr[:,:,reflections_idx]
    to_return = []

    for a in arr:
        a = np.absolute(np.fft.fft(a))
        a = np.divide(a[1:], a[0]) #NOTE: make sure the fft is consistent... a fire might increase on the end where baseline doesnt

        maximums = np.ones(a.shape)
        maximums = np.divide(maximums, 10) # maybe change this to see how it effects predictions
        minimums = np.zeros(a.shape)

        numerator = np.subtract(a, minimums)
        denom = np.subtract(maximums, minimums)
        to_return.append(np.divide(numerator, denom))
    return np.array(to_return)

def normalize1dScenario(X, exp, globalmaxref=0, globalminref=10000000):
    if X.size < 2:
        return X

    max_pos = 255
    min_pos = 0
    maxs = [exp.max_perc_fluc, exp.max_diff, exp.max_degree, max_pos, exp.max_reflection]
    mins = [exp.min_perc_fluc, exp.min_diff, exp.min_degree, min_pos, exp.min_reflection]
    maximums, minimums = [], []
    for i in range(5):
        maximums = maximums + maxs
        minimums = minimums + mins
    maximums = np.array(maximums)
    minimums = np.array(minimums)
    full_max, full_min = [], []

    for i in range(X.shape[0]):
        full_max.append(maximums)
        full_min.append(minimums)

    full_max = np.array(full_max)
    full_min = np.array(full_min)

    numerator = X.__sub__(full_min)
    denom = full_max.__sub__(full_min)
    return np.divide(numerator, denom)


def normalize3dScenario(X, exp, globalmaxref=0, globalminref=10000000):
    # global globalmaxref
    # global globalminref

    if X.size < 2:
        return X

    # max_fluc = max([np.amax(X[:,:,2]), np.amax(X[:,:,7]), np.amax(X[:,:,12]), np.amax(X[:,:,17]), np.amax(X[:,:,22])])
    # print('max_fluc: ', max_fluc)

    max_pos = 255
    min_pos = 0

    space = 1 #exp.max_reflection - exp.min_reflection
    max_fluc = (exp.max_reflection - exp.min_reflection) // 2

    # maxs = [exp.max_reflection, space, max_fluc, max_pos, exp.max_reflection, exp.max_reflection, 10, exp.max_reflection, 10]
    # mins = [exp.min_reflection, 0, 0, min_pos, exp.min_reflection, exp.min_reflection, 0, exp.min_reflection, 0]
    maxs = [exp.max_reflection, space, max_fluc, max_pos, exp.max_reflection]
    mins = [exp.min_reflection, 0, 0, min_pos, exp.min_reflection]
    # maxs = [space, max_pos]
    # mins = [0, min_pos]
    maximums = []
    minimums = []
    for a in range(5):
        maximums = maximums + maxs
        minimums = minimums + mins

    maximums = np.array(maximums)
    minimums = np.array(minimums)

    for i, val in enumerate(X):

        sample_max = []
        sample_min = []
        for x, frame in enumerate(val):
            sample_max.append(maximums)
            sample_min.append(minimums)
        sample_max = np.array(sample_max)
        sample_min = np.array(sample_min)

        numerator = val.__sub__(sample_min)
        denom = sample_max.__sub__(sample_min)
        X[i] = np.divide(numerator, denom)

    return X

# Normalizes all 3d data at once
def normalizeData(X, flat, leave_one_out=False):
    global isglobalmax
    global isglobalmin

    if not leave_one_out:
        ismax = np.amax(X, axis=0)
        isglobalmax = np.amax(ismax, axis=0)
        ismin = np.amin(X, axis=0)
        isglobalmin = np.amin(ismin, axis=0)

    for i, val in enumerate(X):
        # print(val.shape)
        # print(val)
        calc_max = np.amax(val, axis=0)
        calc_min = np.amin(val, axis=0)
        # print(calc_max)
        # print(calc_min)
        max_ref = max([calc_max[0], calc_max[2], calc_max[3], calc_max[5], calc_max[6], calc_max[8], calc_max[9], calc_max[11], calc_max[12], calc_max[14]])
        min_ref = min([calc_min[0], calc_min[2], calc_min[3], calc_min[5], calc_min[6], calc_min[8], calc_min[9], calc_min[11], calc_min[12], calc_min[14]])
        # max_pos = max([calc_max[1], calc_max[4], calc_max[7], calc_max[10], calc_max[13]])
        # min_pos = min([calc_min[1], calc_min[4], calc_min[7], calc_min[10], calc_min[13]])
        max_pos = 255
        min_pos = 0

        maximums = np.array([max_ref, max_pos, max_ref, max_ref, max_pos, max_ref, max_ref, max_pos, max_ref, max_ref, max_pos, max_ref, max_ref, max_pos, max_ref]) #deleted a zero from all
        minimums = np.array([min_ref, min_pos, min_ref, min_ref, min_pos, min_ref, min_ref, min_pos, min_ref, min_ref, min_pos, min_ref, min_ref, min_pos, min_ref])
        # print(maximums)
        # print(minimums)

        sample_max = []
        sample_min = []
        for x in range(0, val.shape[0]):
            sample_max.append(maximums)
            sample_min.append(minimums)
        sample_max = np.array(sample_max)
        sample_min = np.array(sample_min)

        numerator = val.__sub__(sample_min)
        denom = sample_max.__sub__(sample_min)
        X[i] = np.divide(numerator, denom)


    # [obj.reflection[j], obj.distance, obj.normal_reflection]
    pos_inner = [calc_max[0], calc_max[1], calc_max[2], calc_max[3], calc_max[4], calc_max[5], calc_max[6], calc_max[7], calc_max[8], calc_max[9], calc_max[10], calc_max[11], calc_max[12], calc_max[13], calc_max[14]]
    pos_arr = np.array([np.array(pos_inner)])
    posmaximums = np.array([isglobalmax[0], 255, isglobalmax[0], isglobalmax[0], 255, isglobalmax[0], isglobalmax[0], 255, isglobalmax[0], isglobalmax[0], 255, isglobalmax[0], isglobalmax[0], 255, isglobalmax[0]]) #deleted a zero from all
    posminimums = np.array([isglobalmin[0], 0, isglobalmin[0], isglobalmin[0], 0, isglobalmin[0], isglobalmin[0], 0, isglobalmin[0], isglobalmin[0], 0, isglobalmin[0], isglobalmin[0], 0, isglobalmin[0]])

    posnumerator = pos_arr.__sub__(posminimums)
    posdenom = posmaximums.__sub__(posminimums)
    pos_arr = np.divide(posnumerator, posdenom)

    X[i][-1:] = pos_arr
    # X[i] = np.concatenate((X[i], pos_arr), axis=0)


    # maximums = np.array([6000000000, 256, 6000000000, 6000000000, 256, 6000000000, 6000000000, 256, 6000000000, 6000000000, 256, 6000000000, 6000000000, 256, 6000000000]) #deleted a zero from all
    # minimums = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    # maxs = []
    # mins = []
    # big_max = []
    # big_min = []
    #
    # for i in range(0, X[0].shape[0]):
    #     maxs.append(maximums)
    #     mins.append(minimums)
    # for i in range(0, X.shape[0]):
    #     big_max.append(np.array(maxs))
    #     big_min.append(np.array(mins))
    # big_max = np.array(big_max)
    # big_min = np.array(big_min)
    #
    # numerator = X.__sub__(big_min)
    # denom = big_max.__sub__(big_min)
    # X = np.divide(numerator, denom)
    return X


# Normalizes the flat samples to all other samples in that experiment
# def normalizeFlatData(fire_samples, nonfire_samples, leave_one_out_fire, leave_one_out_nofire, leave_one_out, exp):
def normalizeFlatData(X, fire_samples, nonfire_samples, leave_one_out, exp):
    isexpmax, isexpmin = [], []
    global isglobalmax
    global isglobalmin
    global isglobalslopemax
    global isglobalslopemin
    global globalmindiff
    global globalmaxdiff

    if X.size == 0:
        return X

    if fire_samples.size != 0 and leave_one_out is None:
        full_samples = np.concatenate((fire_samples, nonfire_samples), axis=0)
    else:
        full_samples = nonfire_samples

    if leave_one_out != exp.name:
        ismax = np.amax(full_samples, axis=0)
        ismin = np.amin(full_samples, axis=0)
        # print(ismax)
        # print(ismin)
        # exit()
        # if fire_samples.size != 0:
        #     X = np.concatenate((fire_samples, nonfire_samples), axis=0)

        # 25
        min_slope = min([ismin[2], ismin[7], ismin[12], ismin[17], ismin[22]])
        max_slope = max([ismax[2], ismax[7], ismax[12], ismax[17], ismax[22]])


        min_diff = min([ismin[1], ismin[6], ismin[11], ismin[16], ismin[21]])
        max_diff = max([ismax[1], ismax[6], ismax[11], ismax[16], ismax[21]])



        # 20
        # min_slope = min([ismin[1], ismin[5], ismin[9], ismin[13], ismin[17]])
        # max_slope = max([ismax[1], ismax[5], ismax[9], ismax[13], ismax[17]])
        max_norm = np.array([exp.max_reflection, max_diff, max_slope, 255, exp.max_reflection, exp.max_reflection, max_diff, max_slope, 255, exp.max_reflection, exp.max_reflection,
                            max_diff, max_slope, 255, exp.max_reflection, exp.max_reflection, max_diff, max_slope, 255, exp.max_reflection, exp.max_reflection, max_diff, max_slope, 255, exp.max_reflection])
        min_norm = np.array([exp.min_reflection, min_diff, min_slope, 0, exp.min_reflection, exp.min_reflection, min_diff, min_slope, 0, exp.min_reflection, exp.min_reflection,
                            min_diff, min_slope, 0, exp.min_reflection, exp.min_reflection, min_diff, min_slope, 0, exp.min_reflection, exp.min_reflection, min_diff, min_slope, 0, exp.min_reflection])


        if exp.max_reflection > isglobalmax:
            isglobalmax = exp.max_reflection
        if exp.min_reflection < isglobalmin:
            isglobalmin = exp.min_reflection
        if max_slope > isglobalslopemax:
            isglobalslopemax = max_slope
        if min_slope < isglobalslopemin:
            isglobalslopemin = min_slope

        if min_diff < globalmindiff:
            globalmindiff = min_diff
        if max_diff > globalmaxdiff:
            globalmaxdiff = max_diff
    else:
        # if fire_samples.size != 0:
        #     X = np.concatenate((fire_samples, nonfire_samples), axis=0)
        # else:
        #     X = nonfire_samples
        max_diff = globalmaxdiff
        min_diff = globalmindiff
        max_slope = isglobalslopemax
        min_slope = isglobalslopemin
        max_norm = np.array([isglobalmax, max_diff, max_slope, 255, isglobalmax, isglobalmax, max_diff, max_slope, 255, isglobalmax, isglobalmax, max_diff, max_slope, 255, isglobalmax, isglobalmax, max_diff, max_slope, 255, isglobalmax, isglobalmax, max_diff, max_slope, 255, isglobalmax])
        min_norm = np.array([isglobalmin, min_diff, min_slope, 0, isglobalmin, isglobalmin, min_diff, min_slope, 0, isglobalmin, isglobalmin, min_diff, min_slope, 0, isglobalmin, isglobalmin, min_diff, min_slope, 0, isglobalmin, isglobalmin, min_diff, min_slope, 0, isglobalmin])

    for i, val in enumerate(X):
        numerator = val.__sub__(min_norm)
        denom = max_norm.__sub__(min_norm)
        X[i] = np.divide(numerator, denom)

    return X
