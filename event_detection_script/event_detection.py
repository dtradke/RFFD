import pandas as pd
import numpy as np
from numpy import genfromtxt
from matplotlib import pyplot as plt
import collections
import math
from scipy.signal import argrelmin, argrelmax

FLUCT_RANGE = .25

class SigObject(object):

    def __init__(self, x=None, y=None, id=None, power=0):
        self.x = x
        self.y = y
        self.id = id
        self.power = collections.deque(40*[power], 40)
        self.distance = math.sqrt((x ** 2) + (y ** 2))
        self.flag = False
        self.avgrelmax = power
        self.avgrelmin = power
        self.returning_obj = True
        self.fluc_flag = 0
        self.fluc_zone = []
        self.timesteps = []
        self.fire_flag = False
        self.normal_reflection = sum(self.power) / len(self.power)
        try:
            self.direction = math.atan2(y,x)/math.pi*180
        except:
            self.direction = 90

        self.range = self.normal_reflection * FLUCT_RANGE

    def updateMetrics(self, cur_x, cur_y, cur_power, timestep):
        self.x = cur_x
        self.y = cur_y
        self.power.popleft()
        self.power.append(cur_power)
        self.distance = math.sqrt((self.x ** 2) + (self.y ** 2))
        self.timesteps.append(timestep)
        try:
            self.direction = math.atan2(cur_y,cur_x)/math.pi*180
        except:
            self.direction = 90

    def getAvgFluctuation(self):
        np_power = np.array(self.power)
        self.avgrelmax = ((self.avgrelmax + len(argrelmax(np_power)[0])) / 2) / len(self.power)
        self.avgrelmin = ((self.avgrelmin + len(argrelmin(np_power)[0])) / 2) / len(self.power)
        return

    def checkFluctuation(self, timestep):
        # np_power = np.array(self.power)
        np_power = np.array(self.fluc_zone)
        np_recent = np.array(self.power)

        if len(np_power) % 30 == 0 and len(np_power) > 30:
            np_power2 = []
            s = 0
            e = 30
            for x in range(1, (int((len(np_power) / 30)) + 1)):
                new_arr = np_power[s:e]
                np_power2.append(min(new_arr))
                s = s + 30
                e = e + 30
            np_power = np.array(np_power2)
        else:
            return

        cur_rel_max = argrelmax(np_power)
        cur_rel_min = argrelmin(np_power)
        rel_max_amt = argrelmax(np_recent)
        rel_min_amt = argrelmin(np_recent)

        if len(cur_rel_max[0]) > 5 and len(cur_rel_min[0]) > 5:
            distances = self.getFluctuationDistances(cur_rel_max[0], cur_rel_min[0], np_power)
            avg_fluc_dist = sum(distances) / len(distances)
            max_fluc_dist = max(distances)
            rel_max_metric = len(cur_rel_max[0]) / len(np_power)
            rel_min_metric = len(cur_rel_min[0]) / len(np_power)

            # if avg_fluc_dist > self.range or len(rel_max_amt[0]) > self.avgrelmax and len(rel_min_amt[0]) > self.avgrelmin:
            if max_fluc_dist > self.range and rel_max_metric> self.avgrelmax and rel_min_metric > self.avgrelmin and self.fire_flag == False:
                self.fire_flag = True
                print('CONCLUSION: OBJECT {} IS ON FIRE! Occuring at time {} \U0001f525 \U0001f525 \U0001f525'.format(self, timestep))
        return

    def getFluctuationDistances(self, cur_rel_max, cur_rel_min, arr):
        idx = 0
        important_idx = []
        for x in cur_rel_max:
            important_idx.append(x)
        for x in cur_rel_min:
            important_idx.append(x)
        important_idx = sorted(important_idx)
        fluctuation_diff = []
        for i in range(0, (len(important_idx) - 1)):
            diff = arr[important_idx[i]] - arr[important_idx[i+1]]
            fluctuation_diff.append(abs(diff))
        return fluctuation_diff

    def getCurAvgPower(self):
        numerator = list(self.power)[:-10]
        return sum(numerator) / len(numerator)

    def __repr__(self):
        return "SigObject({},{} meters,x: {}, y: {}, {} degrees, {})".format(self.id, self.distance, self.x, self.y, self.direction, self.range)


def makeInitialObj(data_df):
    cols = data_df.columns
    first = data_df.loc[data_df[cols[1]] == 1]
    id = 0
    objs = []
    old_obj_amt = 0

    for index, row in first.iterrows():
        old_obj_amt = row[cols[2]]
        objs.append(SigObject(row[cols[4]], row[cols[5]], id, row[cols[8]]))
        id = id + 1
    return objs, old_obj_amt, data_df




def checkForFire(sig_objects, new_sig_objects, timestep):
    for obj in sig_objects:
        # current_state = obj.power[0] - obj.power[len(obj.power) - 1]
        comp_to_avg = obj.normal_reflection - obj.power[len(obj.power) - 1]
        current_max_min = max(obj.power) - min(obj.power)
        cur_avg_power = obj.getCurAvgPower()

        #still decreasing
        if abs((obj.power[len(obj.power) - 1] - obj.power[len(obj.power) - 2])) < obj.range and comp_to_avg > obj.range: #current_state
            obj.fluc_flag = True

        #back to normal level
        if obj.flag == True and cur_avg_power <= (obj.normal_reflection + obj.range) and cur_avg_power >= (obj.normal_reflection - obj.range):
            obj.flag = False
            obj.fluc_flag = False

        if obj.fluc_flag:
            obj.fluc_zone.append(obj.power[len(obj.power) - 1])

        #dropped and not returned and stabilized yet
        if obj.flag:
            obj.checkFluctuation(timestep)

        #drops
        if comp_to_avg > obj.range:
            # print('Drop detected for object: ', obj, ' at ', timestep)
            obj.flag = True

            for a in sig_objects:
                if a.id != obj.id:
                    if a.direction <= (obj.direction + 10) and a.direction >= (obj.direction - 10):
                        if a.distance < obj.distance:
                            if timestep in a.timesteps:
                                # print('There is an object in the way, no fire event.')
                                break
            obj.checkFluctuation(timestep)

    return



def getBaselineRange(sig_objects):
    for obj in sig_objects:
        avg_power = (sum(obj.power) / len(obj.power))
        if avg_power > 50:
            cur_range = avg_power * FLUCT_RANGE
        else:
            cur_range = 15
        obj.range = cur_range



def runSim(sig_objects, data_df, old_obj_amt):
    cols = data_df.columns
    idx = 2
    length = data_df['Frame_NO.'][data_df.shape[0] - 1]

    for z in range(0, int(length)):
        new_sig_objects = []
        cur_df = data_df.loc[data_df[cols[1]] == idx]

        for index, row in cur_df.iterrows():
            cur_x = row[cols[4]]
            cur_y = row[cols[5]]
            cur_power = row[cols[8]]
            cur_obj_amt = row[cols[2]]
            obj_amt = 0

            for a in sig_objects:
                obj_amt+=1
                #updates
                if cur_x >= (a.x - 0.5) and cur_x <= (a.x + 0.5):
                    if  cur_y >= (a.y - 0.2) and cur_y <= (a.y + 0.2):
                        a.updateMetrics(cur_x, cur_y, cur_power, (idx+1))
                        a.returning_obj = True
                        break
            else:
                #new object
                new_id = (len(sig_objects) + len(new_sig_objects))
                new_obj = SigObject(cur_x, cur_y, new_id, cur_power)
                new_obj.power.popleft()
                new_obj.power.append(cur_power)
                new_obj.returning_obj = False
                new_obj.timesteps.append(idx+1)
                new_sig_objects.append(new_obj)

        sig_objects = sig_objects + new_sig_objects
        idx+=1

        if idx < 2000:
            getBaselineRange(sig_objects)
            for obj in sig_objects:
                obj.getAvgFluctuation()
        elif idx >= 2000:
            if idx == 2000:
                print('Going into test phase now...')
            checkForFire(sig_objects, new_sig_objects, idx)

    flags = 0
    for a in sig_objects:
        if a.fire_flag == True:
            flags = flags + 1

    if flags == 0:
        print('CONCLUSION: There was no fire present')

if __name__ == '__main__':
    directory = 'fire_2m.csv'
    print('================ ', directory, '================')
    data_df = pd.read_csv(directory)

    initial_objects, old_obj_amt, data_df = makeInitialObj(data_df)
    runSim(initial_objects, data_df, old_obj_amt)
