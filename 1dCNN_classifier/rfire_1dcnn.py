# RFire
# 11/16/19
#
# 1DCNN model for the second RFire algorithm, using a CNN. Good results. Models have been trained already using temporal samples.


# cnn model
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
from matplotlib import pyplot
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.utils import to_categorical
from keras.models import load_model
import time
import preprocess
import sys

# load a single file as a numpy array
# def load_file(filepath):
# 	dataframe = read_csv(filepath, header=None, delim_whitespace=True)
# 	return dataframe.values

# load a list of files and return as a 3d numpy array
# def load_group(filenames, prefix=''):
# 	loaded = list()
# 	for name in filenames:
# 		data = load_file(prefix + name)
# 		loaded.append(data)
# 	# stack group so that features are the 3rd dimension
# 	loaded = dstack(loaded)
# 	return loaded

# load a dataset group, such as train or test
# def load_dataset_group(group, prefix=''):
# 	filepath = prefix + group + '/InertialSignals/'
# 	# load all 9 files as a single array
# 	filenames = list()
# 	# total acceleration
# 	filenames += ['total_acc_x_'+group+'.txt', 'total_acc_y_'+group+'.txt', 'total_acc_z_'+group+'.txt']
# 	# body acceleration
# 	filenames += ['body_acc_x_'+group+'.txt', 'body_acc_y_'+group+'.txt', 'body_acc_z_'+group+'.txt']
# 	# body gyroscope
# 	filenames += ['body_gyro_x_'+group+'.txt', 'body_gyro_y_'+group+'.txt', 'body_gyro_z_'+group+'.txt']
# 	# load input data
# 	X = load_group(filenames, filepath)
# 	# load class output
# 	y = load_file(prefix + group + '/y_'+group+'.txt')
# 	return X, y

# load the dataset, returns train and test X and y elements
# def load_dataset(prefix=''):
# 	# load all train
# 	trainX, trainy = load_dataset_group('train', prefix + 'HARDataset/')
# 	print(trainX.shape, trainy.shape)
# 	# load all test
# 	testX, testy = load_dataset_group('test', prefix + 'HARDataset/')
# 	print(testX.shape, testy.shape)
# 	# zero-offset class values
# 	trainy = trainy - 1
# 	testy = testy - 1
# 	# one hot encode y
# 	trainy = to_categorical(trainy)
# 	testy = to_categorical(testy)
# 	print(trainX.shape, trainy.shape, testX.shape, testy.shape)
# 	return trainX, trainy, testX, testy

def train_model(trainX, trainy, epochs=1, batch_size=500):
    verbose, epochs, batch_size = 0, epochs, 500#32
    n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]
    model = Sequential()
    model.add(Conv1D(filters=64, kernel_size=2, activation='relu', input_shape=(n_timesteps,n_features)))
    model.add(Conv1D(filters=64, kernel_size=2, activation='relu'))
    model.add(Conv1D(filters=128, kernel_size=2, activation='relu'))
    model.add(Dropout(0.5))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(100, activation='relu'))
    model.add(Dense(n_outputs, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=1) #verbose
    return model

# fit and evaluate a model
# def evaluate_model(trainX, trainy, testX, testy):
#     verbose, epochs, batch_size = 0, 10, 32
#     n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]
#     model = Sequential()
#     model.add(Conv1D(filters=64, kernel_size=3, activation='relu', input_shape=(n_timesteps,n_features)))
#     model.add(Conv1D(filters=64, kernel_size=3, activation='relu'))
#     model.add(Dropout(0.5))
#     model.add(MaxPooling1D(pool_size=2))
#     model.add(Flatten())
#     model.add(Dense(100, activation='relu'))
#     model.add(Dense(n_outputs, activation='softmax'))
#     model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
#     # fit network
#     model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=verbose)
#     # evaluate model
#     _, accuracy = model.evaluate(testX, testy, batch_size=batch_size, verbose=0)
#     return accuracy

# summarize scores
def summarize_results(scores):
	print(scores)
	m, s = mean(scores), std(scores)
	print('Accuracy: %.3f%% (+/-%.3f)' % (m, s))

def timeSeriesTest(testY, y, leave_one_out, ignition_sample):
    correct = 0
    incorrect = 0
    FN = 0
    FP = 0
    TP = 0
    TN = 0
    FN_before_fire = 0
    fire_flag = False
    early_classification = 0
    false_alarm = 0
    for i, val in enumerate(testY):
        if y[i][0] > y[i][1]: #predicted no fire
            if val[0] > val[1]: #actual no fire
                correct += 1
                TN += 1
            else: #actual fire
                incorrect += 1
                FN += 1
                if not fire_flag:
                    FN_before_fire += 1
        else: # predicted fire
            if leave_one_out and not fire_flag:
                if val[0] < val[1]:
                    fire_flag = True
                    print(" \U0001f525 \U0001f525 \U0001f525 Detected fire at time ", i, " \U0001f525 \U0001f525 \U0001f525")
                    latency = i - ignition_sample
                    print("Latency: ", latency, " second(s)")
                else:
                    future = testY[i:i+5]
                    #checking if a fire is close due to the labeling format
                    for f in future:
                        if f[0] < f[1]:
                            early_classification += 1
                            break
                    else:
                        print("\U0000274C \U0000274C \U0000274C FALSE ALARM at time ", i, " \U0000274C \U0000274C \U0000274C")
                        false_alarm += 1
            if val[0] < val[1]: #actual fire
                correct += 1
                TP += 1
            else: # actual no fire
                incorrect += 1
                FP += 1

    print("Correct: ", correct)
    print("incorrect: ", incorrect)
    print("correct perc: ", correct/len(testY))
    print("TP: ", TP)
    print("TN: ", TN)
    print("FP (annoying) - no fire but predicted fire:     ", FP)
    print("FN (bad) - actual fire but not predicted fire:  ", FN)
    print("Predicted no fire before time when fire starts: ", FN_before_fire)
    print('Early Classification: ', early_classification)
    print('False Alarms: ', false_alarm)

# run an experiment
def run_experiment(model=None, repeats=10):

    leave_one_out = True
	# load data
    trainX, trainY, testX, testY, ignition_sample = preprocess.loadDataset(leave_one_out)

    print("Training Shape: ", trainX.shape, "   ", trainY.shape)
    print("Testing Shape:  ", testX.shape, "    ", testY.shape)

    if model is None:
        epochs = 100
        model = train_model(trainX, trainY, epochs=epochs)
        time_string = time.strftime("%Y%m%d-%H%M%S")
        if leave_one_out:
            fname = 'models/' + time_string + '_' + str(epochs) + 'epochs_leaveoneout-carpet1mleftout.h5'
        else:
            fname = 'models/' + time_string + '_' + str(epochs) + 'epochs.h5'
        model.save(fname)

    y = model.predict(testX)

    timeSeriesTest(testY, y, leave_one_out, ignition_sample)


    # print(len(y))
    # print(len(testY))
    # exit()


    # _, accuracy = model.evaluate(testX, testY, batch_size=500, verbose=0)
    # print("accuracy: ", accuracy)

	# repeat experiment
    # scores = list()
    # for r in range(repeats):
    #     score = evaluate_model(trainX, trainy, testX, testy)
    #     score = score * 100.0
    #     print('>#%d: %.3f' % (r+1, score))
    #     scores.append(score)
	# # summarize results
    # summarize_results(scores)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        mod_str = sys.argv[len(sys.argv) - 1]
        mod = load_model('models/' + mod_str)
        run_experiment(mod)
    else:
        run_experiment()

# run the experiment
# run_experiment()
