# RFire
# 11/16/19
#
# Formats data for the original RFire algorithm

import pandas as pd
import numpy as np
from numpy import genfromtxt
from matplotlib import pyplot as plt
import collections
import math

FLUCT_RANGE = .11
SEARCH_SPACE_MAX = 4
SEARCH_SPACE_MIN = 0.5
ENDBASELINE = 600
POINTTHRESH = 0.8
RECENT_TIME_BUFFER = 30



def getLabel(window, fire, flag):
    if window[50][0] < (window[50][5] * (1 - FLUCT_RANGE)) and fire:
        flag = True
    if flag: #fire
        window.append(np.array([1, 1, 1, 1, 1, 1]))
    else: #no fire
        window.append(np.array([0, 0, 0, 0, 0, 0]))
    return window, flag


def makeSamples(dataset_obj, fire):
    samples = []
    ySamples = []
    fires = []

    for i in dataset_obj:
        if fire:
            fires.append(i)
        window_start = 0
        window_end = 100
        step = 10
        idx = 0
        flag = False
        while window_end < len(i.ram):
            window = []
            for j in range(window_start, window_end):
                l = [i.ram[j], i.distance[j], i.direction[j], i.occlusions[j], i.movement[j], i.normal_reflection]
                window.append(np.array(l))
            window, flag = getLabel(window, fire, flag)
            window = np.array(window)
            samples.append(window)
            window_start += step
            window_end += step
            idx+=1
    samples = np.array(samples)
    return samples, fires


def balanceData(data, leave_one_out=False):
    if not leave_one_out:
        fire_dataset = []
        nofire_dataset = []
        for i in range(0, data.shape[0]):
            if data[i][100][0] == 1:
                fire_dataset.append(data[i])
            else:
                nofire_dataset.append(data[i])

        len_fires = len(fire_dataset)
        len_nofires = len(nofire_dataset)

        fire_dataset_np = np.array(fire_dataset)
        print("Fire samples:    ", fire_dataset_np.shape)
        nofire_dataset_np = np.array(nofire_dataset)
        print("No Fire samples: ", nofire_dataset_np.shape)
        np.random.shuffle(nofire_dataset_np)
        nofire_dataset_np = nofire_dataset_np[:fire_dataset_np.shape[0]]

        dataset = np.concatenate((fire_dataset_np, nofire_dataset_np), axis=0)
        np.random.shuffle(dataset)
    else:
        dataset = data

    X = []
    y = []
    no_fire = 0
    fire = 0
    for i in range(0, dataset.shape[0]):
        if dataset[i][100][0] == 1: #fire
            y.append(np.array([0.0, 1.0]))
            fire += 1
        else: #no fire
            y.append(np.array([1.0, 0.0]))
            no_fire += 1
        X.append(dataset[i][:-1])

    X = np.array(X)
    y = np.array(y)

    print("Total samples: ", X.shape)
    if leave_one_out:
        print("Fire Starts at: ", no_fire)
    X = normalizeData(X)

    return X, y, no_fire

def normalizeEquation(num, min, max):
    return (num - min) / (max - min)

def normalizeData(X):
    # [i.ram[j], i.distance[j], i.direction[j], i.occlusions[j], i.movement[j], i.normal_reflection]
    maximums = [13000, SEARCH_SPACE_MAX, 170, 5, 1, 13000]
    minimums = [0, SEARCH_SPACE_MIN, 10, 0, 0, 0]
    for i in X:
        for j in i:
            for a in range(j.shape[0]):
                j[a] = normalizeEquation(j[a], minimums[a], maximums[a])
    return X
